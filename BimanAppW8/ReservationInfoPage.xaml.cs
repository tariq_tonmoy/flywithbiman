﻿using BimanAppW8.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using BimanPortableClassLibrary.DataModel;
using BimanPortableClassLibrary.Stories;
using Windows.UI.Popups;
using Windows.UI.StartScreen;


// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace BimanAppW8
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class ReservationInfoPage : Page
    {

        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();

        /// <summary>
        /// This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        /// <summary>
        /// NavigationHelper is used on each page to aid in navigation and 
        /// process lifetime management
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }


        public ReservationInfoPage()
        {
            this.InitializeComponent();
            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += navigationHelper_LoadState;
            this.navigationHelper.SaveState += navigationHelper_SaveState;
        }

        /// <summary>
        /// Populates the page with content passed during navigation. Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session. The state will be null the first time a page is visited.</param>
        /// 

        int NoOfDepartFlights, NoOfReturnFlights;
        int deptHitNext = 0, deptHitPrev = 0, retHitNext = 0, retHitPrev = 0;
        private void navigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {

            if (e.PageState != null && e.PageState.ContainsKey("ReservationInfoDataModel"))
            {
                StoryCollection.StoryCollectionInstance.ViewReservation.SetReservationInfoDataModel((ReservationInfoDataModel)e.PageState["ReservationInfoDataModel"]);
                InitializeBooking.IsBookingInitialized = true;
            }
            if (StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoDataModel().BookingID == null || StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoDataModel().BookingID == "")
            {
                StoryCollection.StoryCollectionInstance.ViewReservation.SetReservationInfoDataModel(StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoColletor().ReservationInfoCollection.FirstOrDefault(x => x.BookingID == App.TileID));
            }


            DefaultViewModel["ReservationInfo"] = StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoDataModel();
            var df = from v in StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoDataModel().FlightCollection
                     where v.FlightType == FlightTypeEnum.DepartureFlight
                     select v;
            NoOfDepartFlights = df.Count();

            var rf = from v in StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoDataModel().FlightCollection
                     where v.FlightType == FlightTypeEnum.ReturnFlight
                     select v;
            NoOfReturnFlights = rf.Count();
            hitReset();



            if (!StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoDataModel().WillReturn)
            {
                ReturnFlightGrid.Visibility = Visibility.Collapsed;
            }
            else
            {
                ReturnFlightGrid.Visibility = Visibility.Visible;
            }
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void navigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
            if (StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoDataModel() != null)
            {
                e.PageState["ReservationInfoDataModel"] = StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoDataModel();
            }
            if (StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel() != null)
            {
                e.PageState["BookFlightDataModel"] = StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel();
            }
        }

        #region NavigationHelper registration

        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// 
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="GridCS.Common.NavigationHelper.LoadState"/>
        /// and <see cref="GridCS.Common.NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {

            DepartureMapControl.Transaction = true;
            ReturnMapControl.Transaction = true;
            navigationHelper.OnNavigatedFrom(e);

        }

        #endregion

        private async void HyperlinkButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            Windows.UI.Popups.MessageDialog dialogue = new Windows.UI.Popups.MessageDialog("Do you want your default browser to open this link?", "Open Link");
            dialogue.Commands.Add(new Windows.UI.Popups.UICommand("Yes", this.CommandInvokeHandler));
            dialogue.Commands.Add(new Windows.UI.Popups.UICommand("No", this.CommandInvokeHandler));


            await dialogue.ShowAsync();

        }

        private async void CommandInvokeHandler(Windows.UI.Popups.IUICommand command)
        {
            if (command.Label == "Yes")
                await Windows.System.Launcher.LaunchUriAsync(new Uri("https://" + StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoDataModel().Website, UriKind.Absolute));

        }


        private MapInfoControl DepartureMapControl, ReturnMapControl;
        private void DepartureMapInfoControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (DepartureMapControl == null)
            {
                DepartureMapControl = (MapInfoControl)sender;
                DepartureMapControl.nextFlightClicked += DepartureMapControl_nextFlightClicked;
            }
            DepartureMapControl.DataContext = StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoDataModel().FlightCollection[0];
            List<ReservationInfoDataModel.Flight> FlightCollection = new List<ReservationInfoDataModel.Flight>();
            var v = from f in StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoDataModel().FlightCollection
                    where f.FlightType == FlightTypeEnum.DepartureFlight
                    select f;

            FlightCollection = v.ToList();

            ((MapInfoControl)sender).InitializeMap(FlightCollection);
        }

        void DepartureMapControl_nextFlightClicked(object sender, RoutedEventArgs e)
        {
            int index = 0;
            if (deptHitNext == 0 && deptHitPrev == 0 || deptHitNext < NoOfDepartFlights - 1)
            {
                deptHitNext++;
                index = deptHitNext;
                DepartureMapControl.Direction = true;
                if (deptHitNext >= NoOfDepartFlights - 1)
                {
                    DepartureMapControl.Direction = false;
                }
            }
            else if (deptHitNext >= NoOfDepartFlights - 1 && deptHitPrev >= 0)
            {
                deptHitPrev = NoOfDepartFlights - 1;
                deptHitPrev--;
                DepartureMapControl.Direction = false;

                if (deptHitPrev <= 0)
                {
                    deptHitNext = 0;
                    deptHitPrev = 0;
                    DepartureMapControl.Direction = true;
                }
                index = deptHitPrev;
            }
            DepartureMapControl.DataContext = StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoDataModel().FlightCollection[index];
        }

        private void ReturnMapInfoControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (ReturnMapControl == null)
            {
                ReturnMapControl = (MapInfoControl)sender;
                ReturnMapControl.nextFlightClicked += ReturnMapControl_nextFlightClicked;
            }
            if (StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoDataModel().WillReturn)
            {

                ReturnMapControl.DataContext = StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoDataModel().FlightCollection[NoOfDepartFlights];
                //if (ReturnMapControl == null)


                List<ReservationInfoDataModel.Flight> FlightCollection = new List<ReservationInfoDataModel.Flight>();
                var v = from f in StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoDataModel().FlightCollection
                        where f.FlightType == FlightTypeEnum.ReturnFlight
                        select f;

                FlightCollection = v.ToList();

                ((MapInfoControl)sender).InitializeMap(FlightCollection);

            }
        }

        void ReturnMapControl_nextFlightClicked(object sender, RoutedEventArgs e)
        {
            int index = 0;
            if (retHitNext == 0 && retHitPrev == 0 || retHitNext < NoOfReturnFlights - 1)
            {
                retHitNext++;
                index = retHitNext;
                ReturnMapControl.Direction = true;
                if (retHitNext >= NoOfReturnFlights - 1)
                {
                    ReturnMapControl.Direction = false;
                }
            }
            else if (retHitNext >= NoOfReturnFlights - 1 && retHitPrev >= 0)
            {
                retHitPrev = NoOfReturnFlights - 1;
                retHitPrev--;
                ReturnMapControl.Direction = false;

                if (retHitPrev <= 0)
                {
                    retHitNext = 0;
                    retHitPrev = 0;
                    ReturnMapControl.Direction = true;
                }
                index = retHitPrev;
            }
            ReturnMapControl.DataContext = StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoDataModel().FlightCollection[index+NoOfDepartFlights];

        }

        private void HomeButton_Click(object sender, RoutedEventArgs e)
        {
            this.Frame.Navigate(typeof(HubPage));
        }

        private void AppBar_Closed(object sender, object e)
        {
            ReservationInfoPageAppBar.ResetAppBar();
        }

        FindAppBarControl ReservationInfoPageAppBar = null;
        private void FindAppBarControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (ReservationInfoPageAppBar == null)
            {
                ReservationInfoPageAppBar = (FindAppBarControl)sender;
                ReservationInfoPageAppBar.FindButtonTapped += ReservationInfoPageAppBar_FindButtonTapped;
                ReservationInfoPageAppBar.SaveButtonTapped += ReservationInfoPageAppBar_SaveButtonTapped;
                ReservationInfoPageAppBar.DeleteButtonTapped += ReservationInfoPageAppBar_DeleteButtonTapped;
                ReservationInfoPageAppBar.PinButtonTapped += ReservationInfoPageAppBar_PinButtonTapped;

            }
            SetAppBar();


        }

        async void ReservationInfoPageAppBar_PinButtonTapped(object sender, TappedRoutedEventArgs e)
        {
            this.AppBarForReservation.IsSticky = true;
            this.TopAppBar.IsSticky = true;
            ReservationInfoPageAppBar.ToggleAppBarButton(!SecondaryTile.Exists(StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoDataModel().BookingID));

            if (SecondaryTile.Exists(StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoDataModel().BookingID))
            {
                //Unpin
                SecondaryTile secondaryTile = new SecondaryTile(StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoDataModel().BookingID);
                Windows.Foundation.Rect rect = GetElementRect((FrameworkElement)sender);
                Windows.UI.Popups.Placement plc = Windows.UI.Popups.Placement.Above;

                bool b = await secondaryTile.RequestDeleteForSelectionAsync(rect, plc);

                ReservationInfoPageAppBar.ToggleAppBarButton(b);

            }
            else
            {
                //pin

                Uri secondaryTileLogo310x150 = new Uri(new Windows.ApplicationModel.Resources.ResourceLoader().GetString("secondaryTileLogo310x150"));
                Uri secondaryTileLogo150x150 = new Uri(new Windows.ApplicationModel.Resources.ResourceLoader().GetString("secondaryTileLogo150x150"));
                Uri secondaryTileLogo30x30 = new Uri(new Windows.ApplicationModel.Resources.ResourceLoader().GetString("secondaryTileLogo30x30"));

                string tileActivationArguments = StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoDataModel().BookingID + " was pinned at " + DateTime.Now.ToString("dd-MMM-yyyy");
                string displayName = StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoDataModel().BookingID + "  (" + StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoDataModel().From + "->" + StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoDataModel().To + ")";
                TileSize tileSize = TileSize.Wide310x150;

                SecondaryTile secondaryTile = new SecondaryTile(StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoDataModel().BookingID, displayName, tileActivationArguments, secondaryTileLogo310x150, tileSize);
                secondaryTile.VisualElements.Square150x150Logo = secondaryTileLogo150x150;
                secondaryTile.VisualElements.Square30x30Logo = secondaryTileLogo30x30;

                secondaryTile.VisualElements.ShowNameOnWide310x150Logo = true;
                secondaryTile.VisualElements.ShowNameOnSquare150x150Logo = true;



                secondaryTile.VisualElements.ForegroundText = ForegroundText.Dark;
                secondaryTile.WideLogo = secondaryTileLogo310x150;


                Windows.Foundation.Rect rect = GetElementRect((FrameworkElement)sender);
                Windows.UI.Popups.Placement plc = Windows.UI.Popups.Placement.Left;

                bool b = await secondaryTile.RequestCreateForSelectionAsync(rect, plc);
                ReservationInfoPageAppBar.ToggleAppBarButton(!b);

            }

            this.AppBarForReservation.IsSticky = false;
            this.TopAppBar.IsSticky = false;

        }

        private Windows.Foundation.Rect GetElementRect(FrameworkElement element)
        {
            GeneralTransform transform = element.TransformToVisual(null);
            Point point = transform.TransformPoint(new Point());
            return new Rect(point, new Size(element.ActualWidth, element.ActualHeight));
        }

        void ReservationInfoPageAppBar_DeleteButtonTapped(object sender, TappedRoutedEventArgs e)
        {
            StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoColletor().ReservationInfoCollection.Remove(StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoColletor().ReservationInfoCollection.FirstOrDefault(x => x.BookingID == StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoDataModel().BookingID));
            ShowEmptyListString();
            SetAppBar();
            SortReservationList();
        }

        void ReservationInfoPageAppBar_SaveButtonTapped(object sender, TappedRoutedEventArgs e)
        {
            StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoColletor().ReservationInfoCollection.Add(StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoDataModel());
            ShowEmptyListString();
            SetAppBar();
            SortReservationList();
        }

        private void SetAppBar()
        {
            if (StoryCollection.StoryCollectionInstance.ViewReservation.IsBookingPresentInDatabase(StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoDataModel().BookingID))
            {
                ReservationInfoPageAppBar.SetAppBar(2);

            }
            else
            {
                ReservationInfoPageAppBar.SetAppBar(1);
            }

        }

        async void ReservationInfoPageAppBar_FindButtonTapped(object sender, TappedRoutedEventArgs e)
        {
            LoadingGrid.Visibility = Visibility.Visible;
            LoadingShowStoryboard.Stop();
            LoadingShowStoryboard.Begin();
            HomeButton.IsEnabled = false;
            LoadingRing.IsActive = true;
            ReservationInfoDataModel ridm = StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoDataModel();
            try
            {

                StoryCollection.StoryCollectionInstance.ViewReservation.SetReservationInfoDataModel(await StoryCollection.StoryCollectionInstance.ViewReservation.GetBookingSearchResult(ReservationInfoPageAppBar.BookingText, ReservationInfoPageAppBar.NameText));
            }
            catch (Exception ex)
            {
                StoryCollection.StoryCollectionInstance.ViewReservation.SetReservationInfoDataModel(ridm);
                MessageDialog message = new MessageDialog(ex.Message);
                message.ShowAsync();
                LoadingShowStoryboard.Stop();
                LoadingHideStoryboard.Begin();
                return;
            }

            LoadingHideStoryboard.Begin();
            LoadingShowStoryboard.Stop();

            if (!StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoDataModel().HasErrors)
                this.Frame.Navigate(typeof(ReservationInfoPage));
            else
            {
                MessageDialog msg = new MessageDialog(StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoDataModel().ErrorMessage);
                await msg.ShowAsync();
                StoryCollection.StoryCollectionInstance.ViewReservation.SetReservationInfoDataModel(ridm);
            }
        }

        private void LoadingHideStoryBoard_Completed(object sender, object e)
        {
            LoadingRing.IsActive = false;
            LoadingGrid.Visibility = Visibility.Collapsed;
            HomeButton.IsEnabled = true;
        }

        public ReservationListControl _ReservationListColtrol;
        private async void ReservationListControl_Loaded(object sender, RoutedEventArgs e)
        {
            _ReservationListColtrol = ((ReservationListControl)sender);
            ((ReservationListControl)sender).DataContext = StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoColletor();

            ((ReservationListControl)sender).ReservationListItemCick += ReservationInfoPage_ReservationListItemCick;

            DispatcherTimer ClosestFlightTimer = new DispatcherTimer();
            ClosestFlightTimer.Interval = new TimeSpan(0, 0, 1, 0);
            ClosestFlightTimer.Tick += ClosestFlightTimer_Tick;
            ClosestFlightTimer.Start();
            ClosestFlightTimer_Tick(null, null);

            ShowEmptyListString();



        }


        private void ShowEmptyListString()
        {
            if (StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoColletor().ReservationInfoCollection.Count == 0)
            {
                _ReservationListColtrol.Visibility = Visibility.Collapsed;
                EmptyTextBlock.Visibility = Visibility.Visible;

            }
            else
            {
                _ReservationListColtrol.Visibility = Visibility.Visible;
                EmptyTextBlock.Visibility = Visibility.Collapsed;
            }
        }

        private void ReservationInfoPage_ReservationListItemCick(object sender, ItemClickEventArgs e)
        {
            if (((ReservationInfoDataModel)e.ClickedItem).BookingID == StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoDataModel().BookingID) return;


            StoryCollection.StoryCollectionInstance.ViewReservation.SetReservationInfoDataModel((ReservationInfoDataModel)e.ClickedItem);


            DefaultViewModel["ReservationInfo"] = StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoDataModel();
            var df = from v in StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoDataModel().FlightCollection
                     where v.FlightType == FlightTypeEnum.DepartureFlight
                     select v;
            NoOfDepartFlights = df.Count();

            var rf = from v in StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoDataModel().FlightCollection
                     where v.FlightType == FlightTypeEnum.ReturnFlight
                     select v;
            NoOfReturnFlights = rf.Count();
            hitReset();



            if (!StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoDataModel().WillReturn)
            {
                ReturnFlightGrid.Visibility = Visibility.Collapsed;
            }
            else
            {
                ReturnFlightGrid.Visibility = Visibility.Visible;
            }



            DepartureMapInfoControl_Loaded(DepartureMapControl, null);
            if (StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoDataModel().WillReturn)
                ReturnMapInfoControl_Loaded(ReturnMapControl, null);


            ContentGridShowStoryboard.Begin();

        }

        void hitReset()
        {
            deptHitNext = 0;
            deptHitPrev = 0;
            retHitNext = 0;
            retHitPrev = 0;

        }

        void ClosestFlightTimer_Tick(object sender, object e)
        {
            SortReservationList();
        }

        void SortReservationList()
        {
            foreach (var v in StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoColletor().ReservationInfoCollection)
            {
                v.UpdateClosestFlightString();
                v.updateReservationStatus();
            }

            List<ReservationInfoDataModel> TempList = new List<ReservationInfoDataModel>(StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoColletor().ReservationInfoCollection.OrderBy(x => x.MinTimeToFlight.TotalMinutes)).ToList();

            StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoColletor().ReservationInfoCollection.Clear();

            foreach (var v in TempList)
            {
                StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoColletor().ReservationInfoCollection.Add(v);

            }
        }
    }
}
