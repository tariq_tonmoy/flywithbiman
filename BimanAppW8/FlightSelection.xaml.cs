﻿using BimanAppW8.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using BimanPortableClassLibrary.DataModel;
using BimanPortableClassLibrary.Stories;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using BimanAppW8.DataModel.HtmlScrapingImplementation;
using Windows.UI.Popups;
using System.Collections.ObjectModel;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace BimanAppW8
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class FlightSelection : Page
    {

        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();

        /// <summary>
        /// This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        /// <summary>
        /// NavigationHelper is used on each page to aid in navigation and 
        /// process lifetime management
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }


        public FlightSelection()
        {
            this.InitializeComponent();
            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += navigationHelper_LoadState;
            this.navigationHelper.SaveState += navigationHelper_SaveState;
        }






        private async void navigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {

            if (e.PageState != null && e.PageState.ContainsKey("BookFlightDataModel"))
            {
                StoryCollection.StoryCollectionInstance.BookFlight.SetBookFlightDataModel((BookFlightDataModel)e.PageState["BookFlightDataModel"]);

            }


            InitializeFlightSelection();


        }



        private void navigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
            try
            {
                if (StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel() != null)
                    e.PageState["BookFlightDataModel"] = StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel();


            }
            catch (Exception)
            {

            }


            fsuc_departure.ResetBorderAndRadio(false);
            fsuc_departure.HideCabinClassGrid();

            fsuc_return.ResetBorderAndRadio(false);
            fsuc_return.HideCabinClassGrid();
        }



        private delegate Task<FlightSelectionDataModel> DelegateFlightSelection();
        IFlightBookingImplementation implementation = new IFlightBookingImplementation();

        private FlightSelectionDataModel FlightSelectionTrip_1 = new FlightSelectionDataModel(), FlightSelectionTrip_2 = new FlightSelectionDataModel();


        private DateTime DatePrev, DateNext;
        private DateTime DeptDate, RetDate;
        private static bool _IsInitialized = false;

        public static bool IsInitialized
        {
            get { return FlightSelection._IsInitialized; }
            set { FlightSelection._IsInitialized = value; }
        }
        private static ObservableCollection<FlightSelectionDataModel.FlightDetails> FlightDetailsForRevisit = new System.Collections.ObjectModel.ObservableCollection<FlightSelectionDataModel.FlightDetails>();

        private async void InitializeFlightSelection()
        {
            try
            {
                DeptDate = DatePrev = DateNext = new DateTime(Convert.ToInt32(StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().DepartDate.Year), Convert.ToInt32(CommonVariables.MonthDictionary.FirstOrDefault(x => x.Value == StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().DepartDate.Month).Key), Convert.ToInt32(StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().DepartDate.Day));
                RetDate = DatePrevRet = DateNextRet = new DateTime(Convert.ToInt32(StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().ReturnDate.Year), Convert.ToInt32(CommonVariables.MonthDictionary.FirstOrDefault(x => x.Value == StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().ReturnDate.Month).Key) , Convert.ToInt32(StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().ReturnDate.Day));

                if (!IsInitialized)
                {
                    FlightSelectionDataModel f = new FlightSelectionDataModel();
                    DelegateFlightSelection del = new DelegateFlightSelection(StoryCollection.StoryCollectionInstance.BookFlight.GetFlightSelectionFromBimanAsync);
                    IAsyncResult ar = del.BeginInvoke(null, null);
                    ar.AsyncWaitHandle.WaitOne();
                    var v = del.EndInvoke(ar);

                    LoadingGrid.Visibility = Visibility.Visible;
                    LoadingRing.IsActive = true;

                    f = await v;
                    if (f.FlightCollection == null || f.FlightCollection.Count == 0)
                    {
                        MessageDialog msg = new MessageDialog("No Flights Found", "Search Failed");
                        await msg.ShowAsync();

                        LoadingShowStoryBoard.Stop();
                        LoadingHideStoryBoard.Begin();

                        this.Frame.Navigate(typeof(HubPage));
                        return;

                    }
                    if (f != null) StoryCollection.StoryCollectionInstance.BookFlight.SetFlightSelectionDataModel(f);
                    else throw new Exception();
                    IsInitialized = true;


                }
                else
                {
                    StoryCollection.StoryCollectionInstance.BookFlight.GetFlightSelectionDataModel().FlightCollection = FlightDetailsForRevisit;
                }

                FlightDetailsForRevisit = new System.Collections.ObjectModel.ObservableCollection<FlightSelectionDataModel.FlightDetails>(StoryCollection.StoryCollectionInstance.BookFlight.GetFlightSelectionDataModel().FlightCollection);

                if (StoryCollection.StoryCollectionInstance.BookFlight.GetFlightSelectionDataModel().OneWayFlag)
                {
                    FlightSelectionTrip_1 = new FlightSelectionDataModel();
                    FlightSelectionTrip_1.FlightCollection = new ObservableCollection<FlightSelectionDataModel.FlightDetails>();
                    FlightSelectionTrip_1.CalendarDateCollection = new ObservableCollection<FlightSelectionDataModel.CalendarDate>();
                    FlightSelectionTrip_1.CalendarDateCollectionForShow = new ObservableCollection<FlightSelectionDataModel.CalendarDate>();
                    FlightSelectionTrip_1.SelectedCabinFamily = new FlightSelectionDataModel.FlightDetails.CabinFamily();
                    FlightSelectionTrip_1.SelectedBaggageAllowance = new FlightSelectionDataModel.BaggageAllowance();
                    FlightSelectionTrip_1.Header = new FlightSelectionDataModel.HeaderData();

                    FlightSelectionTrip_1.Header.From = StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().CityIDFrom;
                    FlightSelectionTrip_1.Header.To = StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().CityIDTo;
                    FlightSelectionTrip_1.Header.FromImage = "Assets/City/" + StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().CityIDFrom.Split('(')[1].Split(')')[0] + "1.jpg";
                    FlightSelectionTrip_1.Header.ToImage = "Assets/City/" + StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().CityIDTo.Split('(')[1].Split(')')[0] + "1.jpg";
                    FlightSelectionTrip_1.Header.TripHeader = "Departure";




                    this.PopulateDateData("trip_1");

                    StoryCollection.StoryCollectionInstance.BookFlight.GetFlightSelectionDataModel().FlightDetailsForShowCollection = new ObservableCollection<FlightSelectionDataModel.FlightDetailForShow>();
                    FlightSelectionTrip_1.FlightDetailsForShowCollection = StoryCollection.StoryCollectionInstance.BookFlight.GetFlightSelectionDataModel().FlightDetailsForShowCollection;


                    this.DefaultViewModel["FlightSelectionHeader1"] = FlightSelectionTrip_1;
                }


                if (StoryCollection.StoryCollectionInstance.BookFlight.GetFlightSelectionDataModel().RoundTripFlag)
                {
                    FlightSelectionTrip_2 = new FlightSelectionDataModel();
                    FlightSelectionTrip_2.FlightCollection = new ObservableCollection<FlightSelectionDataModel.FlightDetails>();
                    FlightSelectionTrip_2.CalendarDateCollection = new ObservableCollection<FlightSelectionDataModel.CalendarDate>();
                    FlightSelectionTrip_2.CalendarDateCollectionForShow = new ObservableCollection<FlightSelectionDataModel.CalendarDate>();
                    FlightSelectionTrip_2.SelectedCabinFamily = new FlightSelectionDataModel.FlightDetails.CabinFamily();
                    FlightSelectionTrip_2.SelectedBaggageAllowance = new FlightSelectionDataModel.BaggageAllowance();
                    FlightSelectionTrip_2.Header = new FlightSelectionDataModel.HeaderData();

                    FlightSelectionTrip_2.Header.To = StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().CityIDFrom;
                    FlightSelectionTrip_2.Header.From = StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().CityIDTo;
                    FlightSelectionTrip_2.Header.ToImage = "Assets/City/" + StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().CityIDFrom.Split('(')[1].Split(')')[0] + "2.jpg";
                    FlightSelectionTrip_2.Header.FromImage = "Assets/City/" + StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().CityIDTo.Split('(')[1].Split(')')[0] + "2.jpg";
                    FlightSelectionTrip_2.Header.TripHeader = "Return";

                    this.PopulateDateData("trip_2");
                    StoryCollection.StoryCollectionInstance.BookFlight.GetFlightSelectionDataModel().FlightDetailsForShowCollection = new ObservableCollection<FlightSelectionDataModel.FlightDetailForShow>();
                    FlightSelectionTrip_2.FlightDetailsForShowCollection = StoryCollection.StoryCollectionInstance.BookFlight.GetFlightSelectionDataModel().FlightDetailsForShowCollection;

                    this.DefaultViewModel["FlightSelectionHeader2"] = FlightSelectionTrip_2;

                }
                else
                {
                    fsuc_return.Visibility = Visibility.Collapsed;
                }

                LoadingShowStoryBoard.Stop();
                LoadingHideStoryBoard.Begin();

                StoryCollection.StoryCollectionInstance.BookFlight.GetFlightSelectionDataModel().FlightCollection.Clear();

            }
            catch (Exception ex)
            {
                LoadingShowStoryBoard.Stop();
                LoadingHideStoryBoard.Begin();
                MessageDialog msg = new MessageDialog(ex.Message);
                msg.ShowAsync();
                this.Frame.Navigate(typeof(HubPage));

            }

        }



        private void PopulateDateData(string TripID)
        {
            if (TripID == "trip_1")
            {
                FlightSelectionTrip_1.FlightCollection.Clear();
                FlightSelectionTrip_1.CalendarDateCollection.Clear();
                FlightSelectionTrip_1.CalendarDateCollectionForShow.Clear();
            }
            else
            {
                if (StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().RoundTripTicket)
                {
                    FlightSelectionTrip_2.FlightCollection.Clear();
                    FlightSelectionTrip_2.CalendarDateCollection.Clear();
                    FlightSelectionTrip_2.CalendarDateCollectionForShow.Clear();
                }
            }
            if (TripID == "trip_1")
            {
                foreach (var v in StoryCollection.StoryCollectionInstance.BookFlight.GetFlightSelectionDataModel().FlightCollection)
                {
                    if (v.TripID == "trip_1")
                    {
                        FlightSelectionTrip_1.FlightCollection.Add(v);
                        if (FlightSelectionTrip_1.CalendarDateCollection.FirstOrDefault(x => x.Day == v.FlightDay && x.Month == v.FlightMonth && x.Year == v.FlightYear) == null)
                        {
                            FlightSelectionTrip_1.CalendarDateCollection.Add(new FlightSelectionDataModel.CalendarDate() { Day = v.FlightDay, Month = v.FlightMonth, Year = v.FlightYear });
                            FlightSelectionTrip_1.CalendarDateCollectionForShow.Add(new FlightSelectionDataModel.CalendarDate() { Day = v.FlightDay, Month = CommonVariables.MonthDictionary[v.FlightMonth], Year = v.FlightYear });
                        }
                    }
                }
            }
            else if (TripID == "trip_2")
            {


                foreach (var v in StoryCollection.StoryCollectionInstance.BookFlight.GetFlightSelectionDataModel().FlightCollection)
                {
                    if (v.TripID == "trip_2")
                    {
                        FlightSelectionTrip_2.FlightCollection.Add(v);
                        if (FlightSelectionTrip_2.CalendarDateCollection.FirstOrDefault(x => x.Day == v.FlightDay && x.Month == v.FlightMonth && x.Year == v.FlightYear) == null)
                        {
                            FlightSelectionTrip_2.CalendarDateCollection.Add(new FlightSelectionDataModel.CalendarDate() { Day = v.FlightDay, Month = v.FlightMonth, Year = v.FlightYear });
                            FlightSelectionTrip_2.CalendarDateCollectionForShow.Add(new FlightSelectionDataModel.CalendarDate() { Day = v.FlightDay, Month = CommonVariables.MonthDictionary[v.FlightMonth], Year = v.FlightYear });

                        }

                    }
                }

            }


        }


        #region NavigationHelper registration

        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// 
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="GridCS.Common.NavigationHelper.LoadState"/>
        /// and <see cref="GridCS.Common.NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedFrom(e);
        }

        #endregion


        
        private FlightSelectionUserControl fsuc_departure, fsuc_return;
        private void DepartureFlightSelectionUserControl_Loaded(object sender, RoutedEventArgs e)
        {
            fsuc_departure = (FlightSelectionUserControl)sender;
            fsuc_departure.PrevButtonClickedEvent += fsuc_departure_PrevButtonClickedEvent;
            fsuc_departure.NextButtonClickedEvent += fsuc_departure_NextButtonClickedEvent;
            fsuc_departure.ItemClickedEvent += fsuc_departure_ItemClickedEvent;
            fsuc_departure.FlightDetailsClickedEvent += fsuc_departure_FlightDetailsClickedEvent;
            fsuc_departure.RadioClickedEvent += fsuc_departure_RadioClickedEvent;

        }

        void fsuc_departure_RadioClickedEvent(object sender, RoutedEventArgs e)
        {
            RadioButton radioButton = (RadioButton)sender;
            this.SetSelectedFlight("trip_1", radioButton);


        }

        private void SetSelectedFlight(string TripID, RadioButton radioButton)
        {
            if (radioButton.IsEnabled && (bool)radioButton.IsChecked)
            {
                IEnumerable<FlightSelectionDataModel.FlightDetails> v = null;
                if (TripID == "trip_1")
                {
                    v = FlightSelectionTrip_1.FlightCollection.Where(x => x.FlightID == FlightDetailForShow_Trip_1.FlightID);
                }
                else
                {
                    v = FlightSelectionTrip_2.FlightCollection.Where(x => x.FlightID == FlightDetailForShow_Trip_2.FlightID);
                }

                foreach (var f in v)
                {
                    bool CabinFlag = false;
                    foreach (var c in f.CabinFamilyCollection)
                    {
                        if (c.EncodedValue == null) CabinFlag = false;
                        else
                        {
                            CabinFlag = true;
                            break;
                        }

                    }


                    if (CabinFlag)
                    {
                        string str = "";
                        if (radioButton.Name == "SuperSaverRadio")
                            str = "Super Saver";

                        else if (radioButton.Name == "EconomySaverRadio")
                            str = "Economy Saver";

                        else if (radioButton.Name == "EconomyFlexibleRadio")
                            str = "Economy Flexible";

                        else if (radioButton.Name == "BusinessSaverRadio")
                            str = "Business Saver";

                        else if (radioButton.Name == "BusinessFlexibleRadio")
                            str = "Business Flexible";

                        var cabin = f.CabinFamilyCollection.FirstOrDefault(x => x.CabinFamilyName == str);
                        var baggage = StoryCollection.StoryCollectionInstance.BookFlight.GetFlightSelectionDataModel().BaggageAllowanceCollection.FirstOrDefault(x => x.CabinFamilyName == str);

                        if (TripID == "trip_1")
                        {
                            FlightSelectionTrip_1.SelectedCabinFamily.CabinFamilyName = cabin.CabinFamilyName;
                            FlightSelectionTrip_1.SelectedCabinFamily.EncodedValue = cabin.EncodedValue;
                            FlightSelectionTrip_1.SelectedCabinFamily.Price = cabin.Price + "/Person";
                            FlightSelectionTrip_1.SelectedCabinFamily.TotalPrice = cabin.TotalPrice;


                            FlightSelectionTrip_1.SelectedBaggageAllowance.CabinFamilyName = baggage.CabinFamilyName;
                            FlightSelectionTrip_1.SelectedBaggageAllowance.ChangeOrRefund = baggage.ChangeOrRefund;
                            FlightSelectionTrip_1.SelectedBaggageAllowance.Domestic = baggage.Domestic;
                            FlightSelectionTrip_1.SelectedBaggageAllowance.FromBangladesh = baggage.FromBangladesh;
                            FlightSelectionTrip_1.SelectedBaggageAllowance.OutsideBangladesh = baggage.OutsideBangladesh;


                        }
                        else
                        {
                            FlightSelectionTrip_2.SelectedCabinFamily.CabinFamilyName = cabin.CabinFamilyName;
                            FlightSelectionTrip_2.SelectedCabinFamily.EncodedValue = cabin.EncodedValue;
                            FlightSelectionTrip_2.SelectedCabinFamily.Price = cabin.Price + "/Person";
                            FlightSelectionTrip_2.SelectedCabinFamily.TotalPrice = cabin.TotalPrice;

                            FlightSelectionTrip_2.SelectedBaggageAllowance.CabinFamilyName = baggage.CabinFamilyName;
                            FlightSelectionTrip_2.SelectedBaggageAllowance.ChangeOrRefund = baggage.ChangeOrRefund;
                            FlightSelectionTrip_2.SelectedBaggageAllowance.Domestic = baggage.Domestic;
                            FlightSelectionTrip_2.SelectedBaggageAllowance.FromBangladesh = baggage.FromBangladesh;
                            FlightSelectionTrip_2.SelectedBaggageAllowance.OutsideBangladesh = baggage.OutsideBangladesh;

                        }





                    }
                }
            }
        }
        FlightSelectionDataModel.FlightDetailForShow FlightDetailForShow_Trip_1 = new FlightSelectionDataModel.FlightDetailForShow();
        void fsuc_departure_FlightDetailsClickedEvent(object sender, ItemClickEventArgs e)
        {
            FlightDetailForShow_Trip_1 = (FlightSelectionDataModel.FlightDetailForShow)e.ClickedItem;

            var v = FlightSelectionTrip_1.FlightCollection.Where(x => x.FlightID == FlightDetailForShow_Trip_1.FlightID);
            foreach (var f in v)
            {
                bool CabinFlag = false;
                foreach (var c in f.CabinFamilyCollection)
                {
                    if (c.EncodedValue == null) CabinFlag = false;
                    else
                    {
                        CabinFlag = true;
                        break;
                    }

                }

                if (CabinFlag)
                {
                    if (f.CabinFamilyCollection.FirstOrDefault(x => x.EncodedValue != null && x.CabinFamilyName == "Super Saver") != null)
                        FlightSelectionTrip_1.IsSuperSaverAvailable = true;
                    else FlightSelectionTrip_1.IsSuperSaverAvailable = false;


                    if (f.CabinFamilyCollection.FirstOrDefault(x => x.EncodedValue != null && x.CabinFamilyName == "Economy Saver") != null)
                        FlightSelectionTrip_1.IsEconomySaverAvailable = true;
                    else FlightSelectionTrip_1.IsEconomySaverAvailable = false;


                    if (f.CabinFamilyCollection.FirstOrDefault(x => x.EncodedValue != null && x.CabinFamilyName == "Economy Flexible") != null)
                        FlightSelectionTrip_1.IsEconomyFlexibleAvailable = true;
                    else FlightSelectionTrip_1.IsEconomyFlexibleAvailable = false;


                    if (f.CabinFamilyCollection.FirstOrDefault(x => x.EncodedValue != null && x.CabinFamilyName == "Business Saver") != null)
                        FlightSelectionTrip_1.IsBusinessSaverAvailable = true;
                    else FlightSelectionTrip_1.IsBusinessSaverAvailable = false;


                    if (f.CabinFamilyCollection.FirstOrDefault(x => x.EncodedValue != null && x.CabinFamilyName == "Business Flexible") != null)
                        FlightSelectionTrip_1.IsBusinessFlexibleAvailable = true;
                    else FlightSelectionTrip_1.IsBusinessFlexibleAvailable = false;

                }
                else if (!CabinFlag && f.CabinFamilyCollection != null && f.CabinFamilyCollection.Count != 0)
                {
                    FlightSelectionTrip_1.IsSuperSaverAvailable = false;
                    FlightSelectionTrip_1.IsEconomySaverAvailable = false;
                    FlightSelectionTrip_1.IsEconomyFlexibleAvailable = false;
                    FlightSelectionTrip_1.IsBusinessSaverAvailable = false;
                    FlightSelectionTrip_1.IsBusinessFlexibleAvailable = false;
                }
            }
        }

        private FlightSelectionDataModel.CalendarDate ClickedDateDept = null;
        void fsuc_departure_ItemClickedEvent(object sender, ItemClickEventArgs e)
        {
            FlightSelectionDataModel.CalendarDate cd = new FlightSelectionDataModel.CalendarDate();
            cd = (FlightSelectionDataModel.CalendarDate)e.ClickedItem;
            string MonthStr = CommonVariables.MonthDictionary.FirstOrDefault(x => x.Value == cd.Month).Key;

            FlightSelectionDataModel.CalendarDate TempCalenderDate = FlightSelectionTrip_1.CalendarDateCollection.FirstOrDefault(x => (x.Day == cd.Day) && (x.Month == (MonthStr.Length < 2 ? "0" + MonthStr : MonthStr)) && (x.Year == cd.Year));

            if (ClickedDateDept == null || TempCalenderDate != ClickedDateDept)
            {
                fsuc_departure.ResetBorderAndRadio(true);
                fsuc_departure.HideCabinClassGrid();


                SetFlightDetailsForShow(TempCalenderDate, "trip_1");
                ClickedDateDept = TempCalenderDate;
            }
        }


        private void SetFlightDetailsForShow(FlightSelectionDataModel.CalendarDate _CalendarDetails, string TripID)
        {
            if (TripID == "trip_1") FlightSelectionTrip_1.FlightDetailsForShowCollection.Clear();
            else FlightSelectionTrip_2.FlightDetailsForShowCollection.Clear();
            FlightSelectionDataModel fsdm = null;
            if (TripID == "trip_1") fsdm = FlightSelectionTrip_1;
            else fsdm = FlightSelectionTrip_2;

            var v = fsdm.FlightCollection.Where(x => x.FlightDay == _CalendarDetails.Day && x.FlightMonth == _CalendarDetails.Month && x.FlightYear == _CalendarDetails.Year && x.TripID == TripID);
            DateTime dt = new DateTime(Convert.ToInt32(_CalendarDetails.Year), Convert.ToInt32(_CalendarDetails.Month), Convert.ToInt32(_CalendarDetails.Day));
            DateTime TakeOffDate, LandingDate;

            foreach (var f in v)
            {
                FlightSelectionDataModel.FlightDetailForShow fdfs;
                if (TripID == "trip_1")
                {

                    if (FlightSelectionTrip_1.FlightDetailsForShowCollection.FirstOrDefault(x => x.FlightID == f.FlightID) != null)
                    {
                        fdfs = FlightSelectionTrip_1.FlightDetailsForShowCollection.FirstOrDefault(x => x.FlightID == f.FlightID);
                        fdfs.FlightName += "\n";
                        fdfs.Landing += "\n";
                        fdfs.LandingDate += "\n";
                        fdfs.Route += "\n";
                        fdfs.TakeOff += "\n";
                        fdfs.TakeOffDate += "\n";
                    }
                    else
                    {
                        fdfs = new FlightSelectionDataModel.FlightDetailForShow();
                        fdfs.FlightName = "";
                        fdfs.Landing = "";
                        fdfs.LandingDate = "";
                        fdfs.Route = "";
                        fdfs.TakeOff = "";
                        fdfs.TakeOffDate = "";

                    }

                }
                else
                {

                    if (FlightSelectionTrip_2.FlightDetailsForShowCollection.FirstOrDefault(x => x.FlightID == f.FlightID) != null)
                    {
                        fdfs = FlightSelectionTrip_2.FlightDetailsForShowCollection.FirstOrDefault(x => x.FlightID == f.FlightID);
                        fdfs.FlightName += "\n";
                        fdfs.Landing += "\n";
                        fdfs.LandingDate += "\n";
                        fdfs.Route += "\n";
                        fdfs.TakeOff += "\n";
                        fdfs.TakeOffDate += "\n";
                    }
                    else
                    {
                        fdfs = new FlightSelectionDataModel.FlightDetailForShow();
                        fdfs.FlightName = "";
                        fdfs.Landing = "";
                        fdfs.LandingDate = "";
                        fdfs.Route = "";
                        fdfs.TakeOff = "";
                        fdfs.TakeOffDate = "";

                    }
                }


                fdfs.FlightID = f.FlightID;
                fdfs.FlightName += f.FlightName;
                fdfs.Landing += f.Landing;
                fdfs.TakeOff += f.TakeOff;
                fdfs.Route += f.Route;
                if (f.TakeOffTimeShift != null)
                {
                    if (f.TakeOffTimeShift[0] == '+')
                        TakeOffDate = (dt + new TimeSpan(Convert.ToInt32(f.TakeOffTimeShift[1].ToString()), 0, 0, 0));
                    else
                        TakeOffDate = (dt - new TimeSpan(Convert.ToInt32(f.TakeOffTimeShift[1].ToString()), 0, 0, 0));

                }
                else TakeOffDate = dt;
                if (f.LandingTimeShift != null)
                {
                    if (f.LandingTimeShift[0] == '+')
                        LandingDate = (TakeOffDate + new TimeSpan(Convert.ToInt32(f.LandingTimeShift[1].ToString()), 0, 0, 0));
                    else
                        LandingDate = (TakeOffDate - new TimeSpan(Convert.ToInt32(f.LandingTimeShift[1].ToString()), 0, 0, 0));

                }
                else LandingDate = TakeOffDate;

                fdfs.TakeOffDate += TakeOffDate.Day + "-" + TakeOffDate.Month + "-" + TakeOffDate.Year;

                fdfs.LandingDate += LandingDate.Day + "-" + LandingDate.Month + "-" + LandingDate.Year;

                if (TripID == "trip_1")
                {
                    if (FlightSelectionTrip_1.FlightDetailsForShowCollection.FirstOrDefault(x => x.FlightID == f.FlightID) != null)
                        FlightSelectionTrip_1.FlightDetailsForShowCollection.Remove(FlightSelectionTrip_1.FlightDetailsForShowCollection.FirstOrDefault(x => x.FlightID == f.FlightID));
                    FlightSelectionTrip_1.FlightDetailsForShowCollection.Add(fdfs);
                }
                else
                {
                    if (FlightSelectionTrip_2.FlightDetailsForShowCollection.FirstOrDefault(x => x.FlightID == f.FlightID) != null)
                        FlightSelectionTrip_2.FlightDetailsForShowCollection.Remove(FlightSelectionTrip_2.FlightDetailsForShowCollection.FirstOrDefault(x => x.FlightID == f.FlightID));
                    FlightSelectionTrip_2.FlightDetailsForShowCollection.Add(fdfs);

                }

            }
        }

        void fsuc_departure_NextButtonClickedEvent(object sender, RoutedEventArgs e)
        {
            DeptDate += new TimeSpan(7, 0, 0, 0);
            IsInitialized = false;


            AddDataToDateList(DeptDate, false, "trip_1");

        }


        void fsuc_departure_PrevButtonClickedEvent(object sender, RoutedEventArgs e)
        {
            DeptDate -= new TimeSpan(7, 0, 0, 0);
            IsInitialized = false;

            AddDataToDateList(DeptDate, true, "trip_1");

        }

        private async void AddDataToDateList(DateTime dt, bool ins, string TripID)
        {
            if (TripID == "trip_1")
            {


                string tempYear = StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().DepartDate.Year, tempMonth = StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().DepartDate.Month, tempDay = StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().DepartDate.Day;

                StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().DepartDate.Year = dt.Year.ToString();
                StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().DepartDate.Month = CommonVariables.MonthDictionary[dt.Month.ToString()];
                StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().DepartDate.Day = dt.Day.ToString();



                var v = StoryCollection.StoryCollectionInstance.BookFlight.GetFlightSelectionFromBimanAsync();

                LoadingHideStoryBoard.Stop();
                LoadingShowStoryBoard.Begin();
                LoadingRing.IsActive = true;
                LoadingGrid.Visibility = Visibility.Visible;

                fsuc_departure.ResetTickMark();
                fsuc_departure.ResetBorderAndRadio(true);
                fsuc_departure.HideCabinClassGrid();
                if (FlightSelectionTrip_1.FlightDetailsForShowCollection != null)
                    FlightSelectionTrip_1.FlightDetailsForShowCollection.Clear();

                if (StoryCollection.StoryCollectionInstance.BookFlight.GetFlightSelectionDataModel().RoundTripFlag)
                {
                    fsuc_return.ResetTickMark();
                    fsuc_return.ResetBorderAndRadio(true);
                    fsuc_return.HideCabinClassGrid();
                    if (FlightSelectionTrip_2.FlightDetailsForShowCollection != null)
                        FlightSelectionTrip_2.FlightDetailsForShowCollection.Clear();
                }
                try
                {
                    var f = await v;

                }
                catch (Exception ex)
                {
                    MessageDialog msg = new MessageDialog(ex.Message, "Flight Search Error");
                    msg.ShowAsync();
                    IsInitialized = true;
                    StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().DepartDate.Year = tempYear;
                    StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().DepartDate.Month = tempMonth;
                    StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().DepartDate.Day = tempDay;

                    InitializeFlightSelection();
                    return;
                }



                if (StoryCollection.StoryCollectionInstance.BookFlight.GetFlightSelectionDataModel().FlightCollection == null || StoryCollection.StoryCollectionInstance.BookFlight.GetFlightSelectionDataModel().FlightCollection.Count == 0)
                {
                    MessageDialog msg = new MessageDialog("No Flights Found on " + DeptDate.ToString("dd-MMM-yyyy"), "Search Result");
                    await msg.ShowAsync();

                    DeptDate = new DateTime(Convert.ToInt32(tempYear), Convert.ToInt32(CommonVariables.MonthDictionary.FirstOrDefault(x => x.Value == tempMonth).Key), Convert.ToInt32(tempDay));
                    AddDataToDateList(DeptDate, ins, TripID);
                    return;
                }

                this.PopulateDateData("trip_1");
                this.PopulateDateData("trip_2");


            }
            else if (TripID == "trip_2")
            {
                string tempYear = StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().DepartDate.Year, tempMonth = StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().DepartDate.Month, tempDay = StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().DepartDate.Day;

                StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().ReturnDate.Year = dt.Year.ToString();
                StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().ReturnDate.Month = CommonVariables.MonthDictionary[dt.Month.ToString()];
                StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().ReturnDate.Day = dt.Day.ToString();


                var v = StoryCollection.StoryCollectionInstance.BookFlight.GetFlightSelectionFromBimanAsync();

                LoadingHideStoryBoard.Stop();
                LoadingShowStoryBoard.Begin();
                LoadingRing.IsActive = true;
                LoadingGrid.Visibility = Visibility.Visible;

                fsuc_return.ResetTickMark();
                fsuc_return.ResetBorderAndRadio(true);
                fsuc_return.HideCabinClassGrid();
                FlightSelectionTrip_2.FlightDetailsForShowCollection.Clear();


                fsuc_departure.ResetTickMark();
                fsuc_departure.ResetBorderAndRadio(true);
                fsuc_departure.HideCabinClassGrid();
                FlightSelectionTrip_1.FlightDetailsForShowCollection.Clear();

                try
                {
                    var f = await v;
                }
                catch (Exception ex)
                {
                    MessageDialog msg = new MessageDialog(ex.Message, "Flight Search Error");
                    msg.ShowAsync();
                    IsInitialized = true;
                    StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().DepartDate.Year = tempYear;
                    StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().DepartDate.Month = tempMonth;
                    StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().DepartDate.Day = tempDay;

                    InitializeFlightSelection();
                    return;
                }

                if (StoryCollection.StoryCollectionInstance.BookFlight.GetFlightSelectionDataModel().FlightCollection == null || StoryCollection.StoryCollectionInstance.BookFlight.GetFlightSelectionDataModel().FlightCollection.Count == 0)
                {
                    MessageDialog msg = new MessageDialog("No Flights Found on " + RetDate.ToString("dd-MMM-yyyy"), "Search Result");
                    await msg.ShowAsync();

                    RetDate = new DateTime(Convert.ToInt32(tempYear), Convert.ToInt32(CommonVariables.MonthDictionary.FirstOrDefault(x => x.Value == tempMonth).Key), Convert.ToInt32(tempDay));
                    AddDataToDateList(RetDate, ins, TripID);
                    return;
                }

                this.PopulateDateData("trip_1");
                this.PopulateDateData("trip_2");


            }

            LoadingShowStoryBoard.Stop();
            LoadingHideStoryBoard.Begin();

            StoryCollection.StoryCollectionInstance.BookFlight.GetFlightSelectionDataModel().FlightCollection.Clear();
        }



        private void LoadingHideStoryBoard_Completed(object sender, object e)
        {

            LoadingGrid.Visibility = Visibility.Collapsed;
            LoadingRing.IsActive = false;
        }

        private void ReturnFlightSelectionUserControl_Loaded(object sender, RoutedEventArgs e)
        {
            fsuc_return = (FlightSelectionUserControl)sender;
            fsuc_return.PrevButtonClickedEvent += fsuc_return_PrevButtonClickedEvent;
            fsuc_return.NextButtonClickedEvent += fsuc_return_NextButtonClickedEvent;
            fsuc_return.ItemClickedEvent += fsuc_return_ItemClickedEvent;
            fsuc_return.FlightDetailsClickedEvent += fsuc_return_FlightDetailsClickedEvent;
            fsuc_return.RadioClickedEvent += fsuc_return_RadioClickedEvent;
        }

        void fsuc_return_RadioClickedEvent(object sender, RoutedEventArgs e)
        {
            RadioButton radioButton = (RadioButton)sender;
            this.SetSelectedFlight("trip_2", radioButton);
        }

        private FlightSelectionDataModel.FlightDetailForShow FlightDetailForShow_Trip_2 = new FlightSelectionDataModel.FlightDetailForShow();
        void fsuc_return_FlightDetailsClickedEvent(object sender, ItemClickEventArgs e)
        {
            FlightDetailForShow_Trip_2 = (FlightSelectionDataModel.FlightDetailForShow)e.ClickedItem;

            var v = FlightSelectionTrip_2.FlightCollection.Where(x => x.FlightID == FlightDetailForShow_Trip_2.FlightID);
            foreach (var f in v)
            {
                bool CabinFlag = false;
                foreach (var c in f.CabinFamilyCollection)
                {
                    if (c.EncodedValue == null) CabinFlag = false;
                    else
                    {
                        CabinFlag = true;
                        break;
                    }

                }

                if (CabinFlag)
                {
                    if (f.CabinFamilyCollection.FirstOrDefault(x => x.EncodedValue != null && x.CabinFamilyName == "Super Saver") != null)
                        FlightSelectionTrip_2.IsSuperSaverAvailable = true;
                    else FlightSelectionTrip_2.IsSuperSaverAvailable = false;


                    if (f.CabinFamilyCollection.FirstOrDefault(x => x.EncodedValue != null && x.CabinFamilyName == "Economy Saver") != null)
                        FlightSelectionTrip_2.IsEconomySaverAvailable = true;
                    else FlightSelectionTrip_2.IsEconomySaverAvailable = false;


                    if (f.CabinFamilyCollection.FirstOrDefault(x => x.EncodedValue != null && x.CabinFamilyName == "Economy Flexible") != null)
                        FlightSelectionTrip_2.IsEconomyFlexibleAvailable = true;
                    else FlightSelectionTrip_2.IsEconomyFlexibleAvailable = false;


                    if (f.CabinFamilyCollection.FirstOrDefault(x => x.EncodedValue != null && x.CabinFamilyName == "Business Saver") != null)
                        FlightSelectionTrip_2.IsBusinessSaverAvailable = true;
                    else FlightSelectionTrip_2.IsBusinessSaverAvailable = false;


                    if (f.CabinFamilyCollection.FirstOrDefault(x => x.EncodedValue != null && x.CabinFamilyName == "Business Flexible") != null)
                        FlightSelectionTrip_2.IsBusinessFlexibleAvailable = true;
                    else FlightSelectionTrip_2.IsBusinessFlexibleAvailable = false;

                }
                else if (!CabinFlag && f.CabinFamilyCollection != null && f.CabinFamilyCollection.Count != 0)
                {
                    FlightSelectionTrip_2.IsSuperSaverAvailable = false;
                    FlightSelectionTrip_2.IsEconomySaverAvailable = false;
                    FlightSelectionTrip_2.IsEconomyFlexibleAvailable = false;
                    FlightSelectionTrip_2.IsBusinessSaverAvailable = false;
                    FlightSelectionTrip_2.IsBusinessFlexibleAvailable = false;
                }
            }
        }

        FlightSelectionDataModel.CalendarDate ClickedDateRet = null;
        void fsuc_return_ItemClickedEvent(object sender, ItemClickEventArgs e)
        {
            FlightSelectionDataModel.CalendarDate cd = new FlightSelectionDataModel.CalendarDate();
            cd = (FlightSelectionDataModel.CalendarDate)e.ClickedItem;
           
            FlightSelectionDataModel.CalendarDate TempCalenderDate = FlightSelectionTrip_2.CalendarDateCollection.FirstOrDefault(x => (x.Day == cd.Day) && (x.Month == CommonVariables.MonthDictionary.FirstOrDefault(y => y.Value == cd.Month && y.Key.Length >= 2).Key) && (x.Year == cd.Year));

            if (ClickedDateRet == null || ClickedDateRet != TempCalenderDate)
            {
                fsuc_return.ResetBorderAndRadio(true);
                fsuc_return.HideCabinClassGrid();

                SetFlightDetailsForShow(TempCalenderDate, "trip_2");
                ClickedDateRet = TempCalenderDate;
            }
        }

        void fsuc_return_NextButtonClickedEvent(object sender, RoutedEventArgs e)
        {
            RetDate += new TimeSpan(7, 0, 0, 0);
            IsInitialized = false;

            AddDataToDateList(RetDate, true, "trip_2");
        }

        DateTime DatePrevRet, DateNextRet;
        void fsuc_return_PrevButtonClickedEvent(object sender, RoutedEventArgs e)
        {
            RetDate -= new TimeSpan(7, 0, 0, 0);
            IsInitialized = false;


            AddDataToDateList(RetDate, true, "trip_2");

        }

        private async void Button_Click(object sender, RoutedEventArgs e)
        {

            if (!StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().RoundTripTicket && (FlightSelectionTrip_1.SelectedCabinFamily != null && fsuc_departure.IsCabinSelected()))
            {
                StoryCollection.StoryCollectionInstance.BookFlight.GetFlightSelectionDataModel().SelectedCabinFamilyDeparture = FlightSelectionTrip_1.SelectedCabinFamily;

                TravelInitiaryDataModel t = null;

                LoadingRing.IsActive = true;
                LoadingGrid.Visibility = Visibility.Visible;
                LoadingHideStoryBoard.Stop();
                LoadingShowStoryBoard.Begin();

                bool tempFlag = false;

                try
                {
                    var v = StoryCollection.StoryCollectionInstance.BookFlight.SetTravelInitiaryFromBimanAsync();


                    t = await v;

                }
                catch (Exception ex)
                {
                    MessageDialog message = new MessageDialog(ex.Message, "Flight Search Error");
                    message.ShowAsync();
                    LoadingShowStoryBoard.Stop();
                    LoadingHideStoryBoard.Begin();
                    return;
                }



                if (t.HasErrors)
                {
                    fsuc_departure.ResetBorderAndRadio(false);
                    fsuc_departure.HideCabinClassGrid();


                    if (t.ErrorDescription != null)
                    {
                        MessageDialog message = new MessageDialog("Unable to process Request:\n" + t.ErrorDescription, "Flight Search Error");
                        await message.ShowAsync();
                    }
                    else
                    {
                        MessageDialog message = new MessageDialog("Unable to process Request", "Flight Search Error");
                        await message.ShowAsync();
                        this.Frame.Navigate(typeof(HubPage));

                    }


                }
                else
                {

                    var n = StoryCollection.StoryCollectionInstance.BookFlight.GetFlightSelectionDataModel();
                    this.Frame.Navigate(typeof(TermsPage));

                }

                LoadingShowStoryBoard.Stop();
                LoadingHideStoryBoard.Begin();
            }
            else if (FlightSelectionTrip_1.SelectedCabinFamily != null && FlightSelectionTrip_2.SelectedCabinFamily != null && fsuc_departure.IsCabinSelected() && fsuc_return.IsCabinSelected())
            {



                StoryCollection.StoryCollectionInstance.BookFlight.GetFlightSelectionDataModel().SelectedCabinFamilyDeparture = FlightSelectionTrip_1.SelectedCabinFamily;
                StoryCollection.StoryCollectionInstance.BookFlight.GetFlightSelectionDataModel().SelectedCabinFamilyReturn = FlightSelectionTrip_2.SelectedCabinFamily;

                TravelInitiaryDataModel t = null;

                LoadingRing.IsActive = true;
                LoadingGrid.Visibility = Visibility.Visible;
                LoadingHideStoryBoard.Stop();
                LoadingShowStoryBoard.Begin();

                bool tempFlag = false;

                try
                {
                    var v = StoryCollection.StoryCollectionInstance.BookFlight.SetTravelInitiaryFromBimanAsync();


                    t = await v;

                }
                catch (Exception ex)
                {
                    MessageDialog message = new MessageDialog(ex.Message, "Flight Search Error");
                    message.ShowAsync();
                    LoadingShowStoryBoard.Stop();
                    LoadingHideStoryBoard.Begin();
                    return;
                }


                if (t.HasErrors)
                {
                    fsuc_departure.ResetBorderAndRadio(false);
                    fsuc_departure.HideCabinClassGrid();

                    fsuc_return.ResetBorderAndRadio(false);
                    fsuc_return.HideCabinClassGrid();

                    if (t.ErrorDescription != null)
                    {
                        MessageDialog message = new MessageDialog("Unable to process Request:\n" + t.ErrorDescription, "Flight Search Error");
                        await message.ShowAsync();
                    }
                    else
                    {
                        MessageDialog message = new MessageDialog("Unable to process Request", "Flight Search Error");
                        await message.ShowAsync();
                        this.Frame.Navigate(typeof(HubPage));
                    }


                }
                else
                {

                    var n = StoryCollection.StoryCollectionInstance.BookFlight.GetFlightSelectionDataModel();
                    this.Frame.Navigate(typeof(TermsPage));

                }

                LoadingShowStoryBoard.Stop();
                LoadingHideStoryBoard.Begin();
            }
            else
            {
                MessageDialog message = new MessageDialog("Please choose flight(s) properly.", "Flight Search Error");
                await message.ShowAsync();
            }
        }
    }
}
