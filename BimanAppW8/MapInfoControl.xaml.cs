﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Bing.Maps;
using Bing.Maps.Bing_Maps_XamlTypeInfo;
using Bing.Maps.Search;
using Bing.Maps.Directions;
using System.Diagnostics;
using System.Windows;
using BimanPortableClassLibrary.DataModel;
using Windows.UI.Text;
using System.Threading.Tasks;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace BimanAppW8
{
    public sealed partial class MapInfoControl : UserControl
    {
        public MapInfoControl()
        {
            this.InitializeComponent();
        }
        private bool _Direction;
        private bool _Transaction = false;

        public bool Transaction
        {
            get { return _Transaction; }
            set
            {
                _Transaction = value;
                if (_Transaction && timer.IsEnabled) timer.Stop();
            }
        }

        public bool Direction
        {
            set { _Direction = value; ToggleDirection(); }
        }
        private List<ReservationInfoDataModel.Flight> FlightCollection = new List<ReservationInfoDataModel.Flight>();
        private class PlaceLocationPair
        {
            public string place { get; set; }
            public Location location { get; set; }
        }

        private List<PlaceLocationPair> _PlaceLocationPairList;
        DispatcherTimer timer;
        private int c = 0;
        public async void InitializeMap(List<ReservationInfoDataModel.Flight> PassedFlightCollection)
        {
            FlightCollection = PassedFlightCollection;
            if (FlightCollection.Count <= 1) NextFlightStackPanel.Visibility = Visibility.Collapsed;
            else NextFlightStackPanel.Visibility = Visibility.Visible;

            _PlaceLocationPairList = new List<PlaceLocationPair>();
            foreach (var v in FlightCollection)
            {
                try
                {
                    var temp = _PlaceLocationPairList.First(x => x.place.ToUpper() == v.DepartPlace.ToUpper());
                }
                catch (Exception)
                {

                    _PlaceLocationPairList.Add(new PlaceLocationPair() { place = v.DepartPlace, location = null });
                }
                try
                {
                    var temp = _PlaceLocationPairList.First(x => x.place.ToUpper() == v.LandPlace.ToUpper());
                }
                catch (Exception)
                {
                    _PlaceLocationPairList.Add(new PlaceLocationPair() { place = v.LandPlace, location = null });
                }
            }


            HideFacadeButton.Visibility = Visibility.Collapsed;
            MapProgressRing.IsActive = true;
            MapProgressRing.Visibility = Visibility.Visible;
            FacadeHideStoryboard.Stop();
            FacadeShowStoryboard.Begin();

            FlightMap.ShapeLayers.Clear();
            MapLayer.SetPosition(DepartPushpin, null);
            MapLayer.SetPosition(LandingPushpin, null);
            MapLayer.SetPosition(IntermediatePushpin, null);
            c = 0;
            ShowMapTextBlock.Text = "Flight Map";
            ShowMapTextBlock.Foreground = new SolidColorBrush(Windows.UI.Colors.LightGreen);


            timer = new DispatcherTimer();
            timer.Interval = new TimeSpan(0, 0, 15);
            timer.Tick += timer_Tick;
            timer_Tick(timer, null);
            timer.Start();
        }

        async void timer_Tick(object sender, object e)
        {
            c++;
            try
            {
                await LoadMap(_PlaceLocationPairList);
                ((DispatcherTimer)sender).Stop();

            }
            catch (Exception)
            {

            }

            if (c > 1000)
            {
                MapProgressRing.IsActive = false;
                MapProgressRing.Visibility = Visibility.Collapsed;
                ShowMapTextBlock.Text = "Map Loading Error";
                ShowMapTextBlock.Foreground = new SolidColorBrush(Windows.UI.Colors.Yellow);
                if (((DispatcherTimer)sender).IsEnabled)
                    ((DispatcherTimer)sender).Stop();

            }
        }


        private async Task LoadMap(List<PlaceLocationPair> _PlaceLocationPairList)
        {
            double maxLatitude = -1000, minLatitude = 1000, maxLongditude = -1000, minLongditude = 10000;
            foreach (var v in _PlaceLocationPairList)
            {
                GeocodeRequestOptions requestOption = new GeocodeRequestOptions(v.place.Split('(')[0]);
                LocationDataResponse response = await FlightMap.SearchManager.GeocodeAsync(requestOption);

                int tempIndex = _PlaceLocationPairList.FindIndex(x => x.place == v.place);
                if (tempIndex != -1)
                    _PlaceLocationPairList[tempIndex].location = new Location(response.LocationData[0].Location);

                maxLatitude = v.location.Latitude > maxLatitude ? v.location.Latitude : maxLatitude;
                maxLongditude = v.location.Longitude > maxLongditude ? v.location.Longitude : maxLongditude;

                minLatitude = v.location.Latitude < minLatitude ? v.location.Latitude : minLatitude;
                minLongditude = v.location.Longitude < minLongditude ? v.location.Longitude : minLongditude;

            }

            Location maxLocation = new Location(maxLatitude, maxLongditude), minLocation = new Location(minLatitude, minLongditude);


            LocationRect locationRect = new LocationRect(new LocationCollection() { maxLocation, minLocation });
            locationRect.Width += locationRect.Width * .4;
            locationRect.Height += locationRect.Height * .4;


            FlightMap.SetView(locationRect);
            FlightMap.MapType = MapType.Road;

            MapLayer.SetPosition(DepartPushpin, _PlaceLocationPairList[0].location);
            MapLayer.SetPosition(LandingPushpin, _PlaceLocationPairList[_PlaceLocationPairList.Count - 1].location);
            for (int i = 1; i < _PlaceLocationPairList.Count - 1; i++)
                MapLayer.SetPosition(IntermediatePushpin, _PlaceLocationPairList[i].location);


            MapShapeLayer shapeLayer = new MapShapeLayer();
            for (int i = 0; i < _PlaceLocationPairList.Count - 1; i++)
            {
                MapPolyline polyLine = new MapPolyline();
                polyLine.Locations = new LocationCollection() { _PlaceLocationPairList[i].location, _PlaceLocationPairList[i + 1].location };
                polyLine.Color = Windows.UI.Colors.Green;
                polyLine.Width = 5;
                shapeLayer.Shapes.Add(polyLine);
            }
            FlightMap.ShapeLayers.Add(shapeLayer);

            FlightMap.ShowNavigationBar = false;


            HideFacadeButton.Visibility = Visibility.Visible;
            MapProgressRing.IsActive = false;
            MapProgressRing.Visibility = Visibility.Collapsed;
        }
        private void FlightMap_DoubleTapped(object sender, DoubleTappedRoutedEventArgs e)
        {

            e.Handled = true;
        }

      
        public delegate void NextFlightClicked(object sender, RoutedEventArgs e);
        public event NextFlightClicked nextFlightClicked;
        private void NextFlight_Click(object sender, RoutedEventArgs e)
        {
            if (nextFlightClicked != null)
                nextFlightClicked(sender, e);
        }

        private void ToggleDirection()
        {
            if (!_Direction)
            {
                NextFlightText.Text = "Previous Flight";

                NextFlightButton.Visibility = Visibility.Collapsed;
                PrevFlightButton.Visibility = Visibility.Visible;

            }
            else
            {
                NextFlightText.Text = "Next Flight";

                NextFlightButton.Visibility = Visibility.Visible;
                PrevFlightButton.Visibility = Visibility.Collapsed;
            }
        }

    }
}
