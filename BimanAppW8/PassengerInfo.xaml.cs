﻿using BimanAppW8.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using BimanAppW8.DataModel;
using BimanPortableClassLibrary.Stories;
using BimanPortableClassLibrary.DataModel;
using BimanAppW8.Data;
using System.Collections.ObjectModel;
using System.Diagnostics;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234237

namespace BimanAppW8
{
    /// <summary>
    /// A basic page that provides characteristics common to most applications.
    /// </summary>
    public sealed partial class PassengerInfo : Page
    {

        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();

        /// <summary>
        /// This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        /// <summary>
        /// NavigationHelper is used on each page to aid in navigation and 
        /// process lifetime management
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }


        public PassengerInfo()
        {
            this.InitializeComponent();
            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += navigationHelper_LoadState;
            this.navigationHelper.SaveState += navigationHelper_SaveState;
        }

        /// <summary>
        /// Populates the page with content passed during navigation. Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session. The state will be null the first time a page is visited.</param>
        /// 

        private List<PassengerInfoUserControl> _PassengerInfoUserControlList = new List<PassengerInfoUserControl>();
        private void navigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            if (e.PageState != null && e.PageState.ContainsKey("IsTerminated"))
            {

                if (e.PageState != null && e.PageState.ContainsKey("BookFlightDataModel"))
                {
                    StoryCollection.StoryCollectionInstance.BookFlight.SetBookFlightDataModel((BookFlightDataModel)e.PageState["BookFlightDataModel"]);
                    InitializeBooking.IsBookingInitialized = true;
                }
                this.Frame.Navigate(typeof(HubPage));
                return;
            }

            int adults = StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().NoOfAdults;
            int children = StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().NoOfChildren; 
            int infants = StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().NoOfInfants;

            for (int i = 0; i < adults; i++)
            {
                PassengerInfoUserControl _PassengerInfoUserControl = new PassengerInfoUserControl();
                _PassengerInfoUserControl.Margin = new Thickness(0, 24, 48, 24);
                PassengerInfoFormStack.Children.Add(_PassengerInfoUserControl);
                if (i == 0)
                {
                    _PassengerInfoUserControl.SetupPassangerInfoForm(i, i+1);
                }
                else
                {
                    _PassengerInfoUserControl.SetupPassangerInfoForm(1, i + 1);
                }
                _PassengerInfoUserControlList.Add(_PassengerInfoUserControl);
            }

            for (int i = 0; i < children; i++)
            {
                PassengerInfoUserControl _PassengerInfoUserControl = new PassengerInfoUserControl();
                _PassengerInfoUserControl.Margin = new Thickness(0, 24, 48, 24);
                PassengerInfoFormStack.Children.Add(_PassengerInfoUserControl);
                _PassengerInfoUserControl.SetupPassangerInfoForm(2, i + 1);

                _PassengerInfoUserControlList.Add(_PassengerInfoUserControl);
            }

            for (int i = 0; i < infants; i++)
            {
                PassengerInfoUserControl _PassengerInfoUserControl = new PassengerInfoUserControl();
                _PassengerInfoUserControl.Margin = new Thickness(0, 24, 48, 24);
                PassengerInfoFormStack.Children.Add(_PassengerInfoUserControl);
                _PassengerInfoUserControl.SetupPassangerInfoForm(3, i + 1);

                _PassengerInfoUserControlList.Add(_PassengerInfoUserControl);
            }

        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void navigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
            e.PageState["IsTerminated"] = true;
            if (StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel() != null && InitializeBooking.IsBookingInitialized)
                e.PageState["BookFlightDataModel"] = StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel();
        }

        #region NavigationHelper registration

        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// 
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="GridCS.Common.NavigationHelper.LoadState"/>
        /// and <see cref="GridCS.Common.NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        

        private async void SubmitButton_Click(object sender, RoutedEventArgs e)
        {
            LoadingRing.IsActive = true;
            LoadingGrid.Visibility = Visibility.Visible;
            LoadingHideStoryboard.Stop();
            LoadingShowStoryboard.Begin();
            

            StoryCollection.StoryCollectionInstance.BookFlight.GetPassengerInfoDataModel().AdultInfoCollection = new List<PassengerInfoDataModel.AdultInfo>();
            StoryCollection.StoryCollectionInstance.BookFlight.GetPassengerInfoDataModel().ChildInfoCollection = new List<PassengerInfoDataModel.ChildInfo>();
            StoryCollection.StoryCollectionInstance.BookFlight.GetPassengerInfoDataModel().InfantInfoCollection = new List<PassengerInfoDataModel.InfantInfo>();


            foreach (var v in _PassengerInfoUserControlList)
            {
                if (v.Category == 0 || v.Category == 1)
                {
                    StoryCollection.StoryCollectionInstance.BookFlight.GetPassengerInfoDataModel().AdultInfoCollection.Add(v.PassangerInfoDataModel.SpecificAdultInfo);
                    if (v.Category == 0)
                    {
                        StoryCollection.StoryCollectionInstance.BookFlight.GetPassengerInfoDataModel().AlternatePhone = v.PassangerInfoDataModel.AlternatePhone;
                        StoryCollection.StoryCollectionInstance.BookFlight.GetPassengerInfoDataModel().AltPhoneCountryCode = v.PassangerInfoDataModel.AltPhoneCountryCode;
                        StoryCollection.StoryCollectionInstance.BookFlight.GetPassengerInfoDataModel().Email = v.PassangerInfoDataModel.Email;

                    }
                }
                else if (v.Category == 2)
                {
                    StoryCollection.StoryCollectionInstance.BookFlight.GetPassengerInfoDataModel().ChildInfoCollection.Add(v.PassangerInfoDataModel.SpecificChildInfo);
                }
                else if (v.Category == 3)
                {
                    StoryCollection.StoryCollectionInstance.BookFlight.GetPassengerInfoDataModel().InfantInfoCollection.Add(v.PassangerInfoDataModel.SpecificInfantInfo);
                }
            }

            try
            {

                ReservationInfoDataModel ridm = await StoryCollection.StoryCollectionInstance.BookFlight.CreateReservationToBimanAsync();
                if (ridm.HasErrors)
                {
                    Windows.UI.Popups.MessageDialog msg = new Windows.UI.Popups.MessageDialog(ridm.ErrorMessage);
                    msg.ShowAsync();
                }
                else
                {
                    StoryCollection.StoryCollectionInstance.ViewReservation.SetReservationInfoDataModel(ridm);

                    this.Frame.Navigate(typeof(ReservationInfoPage));
                }

            }
            catch (Exception ex)
            {
                Windows.UI.Popups.MessageDialog msg = new Windows.UI.Popups.MessageDialog(ex.Message);
                msg.ShowAsync();
            }

            LoadingHideStoryboard.Begin();
            LoadingShowStoryboard.Stop();
        }

        private void LoadingHideStoryBoard_Completed(object sender, object e)
        {
            LoadingRing.IsActive = false;
            LoadingGrid.Visibility = Visibility.Collapsed;
        }

    }
}
