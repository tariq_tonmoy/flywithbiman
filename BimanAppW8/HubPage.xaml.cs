﻿using BimanAppW8.Common;
using BimanAppW8.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System.Diagnostics;
using BimanPortableClassLibrary.DataModel;
using BimanPortableClassLibrary.Stories;
using BimanAppW8.DataModel;
using System.Net;
using Windows.UI.Xaml.Media.Animation;
using System.Net.Http;
using System.Threading.Tasks;
using BimanAppW8.DataModel.HtmlScrapingImplementation;
using System.Collections.ObjectModel;
using System.Threading;
using HtmlAgilityPack;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Popups;
using Windows.UI.ApplicationSettings;
using Windows.ApplicationModel.Resources.Core;

// The Hub Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=321224

namespace BimanAppW8
{
    /// <summary>
    /// A page that displays a grouped collection of items.
    /// </summary>
    public sealed partial class HubPage : Page
    {

        #region variables

        #region public_variables

        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        public static InfoGridControl departureInfoGridControl, returnInfoGridControl;
        public static bool departureInfoFlag = true, returnInfoFlag = true;
        public ReservationListControl _ReservationListColtrol;


        #endregion public_variables



        #region private_variables

        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();
        private static InitializeBooking _InitializeBooking = null;
        private ImageUserControl DepartureImageUserControl, ReturnImageUserControl;
        private DispatcherTimer ClosestFlightTimer;
        private FindAppBarControl HubPageAppBar = null;

        #endregion private_variables


        #endregion variables



        /// <summary>
        /// NavigationHelper is used on each page to aid in navigation and 
        /// process lifetime management
        /// </summary>


        /// <summary>
        /// This can be changed to a strongly typed view model.
        /// </summary>

        public HubPage()
        {

            this.InitializeComponent();
            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += navigationHelper_LoadState;
            this.navigationHelper.SaveState += navigationHelper_SaveState;

            SettingsPane.GetForCurrentView().CommandsRequested += App_CommandsRequested;

        }


        private void navigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {

            // TODO: Create an appropriate data model for your problem domain to replace the sample data

            if (e.PageState != null && e.PageState.ContainsKey("BookFlightDataModel"))
            {
                StoryCollection.StoryCollectionInstance.BookFlight.SetBookFlightDataModel((BookFlightDataModel)e.PageState["BookFlightDataModel"]);
                InitializeBooking.IsBookingInitialized = true;
            }

            _InitializeBooking = new InitializeBooking(DefaultViewModel);


        }


        void navigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
            if (StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel() != null && InitializeBooking.IsBookingInitialized)
                e.PageState["BookFlightDataModel"] = StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel();
        }

        void App_CommandsRequested(SettingsPane sender, SettingsPaneCommandsRequestedEventArgs args)
        {
            args.Request.ApplicationCommands.Add(new SettingsCommand("privacypolicy", "Privacy Policy", GetWebPageAsync));
        }
        private async void GetWebPageAsync(IUICommand command)
        {
            Uri uri = new Uri(new Windows.ApplicationModel.Resources.ResourceLoader().GetString("PrivacyPolicy"), UriKind.Absolute);
            await Windows.System.Launcher.LaunchUriAsync(uri);

        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session.  The state will be null the first time a page is visited.</param>


        #region BookFlightInitialization



        #region ImageControlEventHandlers

        private void Departure_ImageUserControl_Loaded(object sender, RoutedEventArgs e)
        {
            ImageUserControl imageUserControl = (ImageUserControl)sender;

            imageUserControl.ItemClicked += Departure_ImageUserControl_ItemClicked;
            DepartureImageUserControl = imageUserControl;

        }

        void Departure_ImageUserControl_ItemClicked(object sender, ItemClickEventArgs e)
        {
            StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().CityIDFrom = ((ImageUserControlData)e.ClickedItem).ImageName + " (" + ((ImageUserControlData)e.ClickedItem).ImageID + ")";

            if (StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().CityIDFrom != StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().CityIDTo)
            {
                SliderUserControlData.CheckButtonVisibility.FromStatus = true;
                if (!String.IsNullOrEmpty(StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().CityIDTo))
                    SliderUserControlData.CheckButtonVisibility.ToStatus = true;
                SliderUserControlData.CheckButtonVisibility.SetOverallStatus();
            }
            if (StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().CityIDFrom == StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().CityIDTo)
            {
                SliderUserControlData.CheckButtonVisibility.FromStatus = false;
                SliderUserControlData.CheckButtonVisibility.SetOverallStatus();
            }
        }




        private void Return_ImageUserControl_Loaded(object sender, RoutedEventArgs e)
        {
            ImageUserControl imageUserControl = (ImageUserControl)sender;

            imageUserControl.ItemClicked += Return_ImageUserControl_ItemClicked;
            ReturnImageUserControl = imageUserControl;
        }

        void Return_ImageUserControl_ItemClicked(object sender, ItemClickEventArgs e)
        {
            StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().CityIDTo = ((ImageUserControlData)e.ClickedItem).ImageName + " (" + ((ImageUserControlData)e.ClickedItem).ImageID + ")";

            if (StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().CityIDFrom != StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().CityIDTo)
            {
                SliderUserControlData.CheckButtonVisibility.ToStatus = true;
                if (!String.IsNullOrEmpty(StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().CityIDFrom))
                    SliderUserControlData.CheckButtonVisibility.FromStatus = true;
                SliderUserControlData.CheckButtonVisibility.SetOverallStatus();
            }
            if (StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().CityIDFrom == StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().CityIDTo)
            {
                SliderUserControlData.CheckButtonVisibility.ToStatus = false;
                SliderUserControlData.CheckButtonVisibility.SetOverallStatus();
            }


        }
        #endregion ImageControlEventHandlers





        #region SliderEventHandlers
        private void DepartureYear_Loaded(object sender, RoutedEventArgs e)
        {
            YearSliderControl departureYearSliderControl = (YearSliderControl)sender;
            departureYearSliderControl.PointerEnterEvent += departureYearSliderControl_PointerEnterEvent;
        }

        void departureYearSliderControl_PointerEnterEvent(object sender, PointerRoutedEventArgs e)
        {
            YearSliderControl.YearFlag = 1;

        }

        private void DepartureMonth_Loaded(object sender, RoutedEventArgs e)
        {
            MonthSliderControl departureMonthSliderControl = (MonthSliderControl)sender;
            departureMonthSliderControl.PointerEnterEvent += departureMonthSliderControl_PointerEnterEvent;


        }

        void departureMonthSliderControl_PointerEnterEvent(object sender, PointerRoutedEventArgs e)
        {
            MonthSliderControl.MonthFlag = 1;
        }

        private void DepartureDate_Loaded(object sender, RoutedEventArgs e)
        {
            DateSliderControl departureDateSliderControl = (DateSliderControl)sender;
            departureDateSliderControl.PointerEnterEvent += departureDateSliderControl_PointerEnterEvent;


        }

        void departureDateSliderControl_PointerEnterEvent(object sender, PointerRoutedEventArgs e)
        {
            DateSliderControl.DateFlag = 1;
        }

        private void ReturnYear_Loaded(object sender, RoutedEventArgs e)
        {
            YearSliderControl returnYearSliderControl = (YearSliderControl)sender;
            returnYearSliderControl.PointerEnterEvent += returnYearSliderControl_PointerEnterEvent;


        }

        void returnYearSliderControl_PointerEnterEvent(object sender, PointerRoutedEventArgs e)
        {
            YearSliderControl.YearFlag = 2;
        }

        private void ReturnMonth_Loaded(object sender, RoutedEventArgs e)
        {
            MonthSliderControl returnMonthSliderControl = (MonthSliderControl)sender;
            returnMonthSliderControl.PointerEnterEvent += returnMonthSliderControl_PointerEnterEvent;



        }

        void returnMonthSliderControl_PointerEnterEvent(object sender, PointerRoutedEventArgs e)
        {
            MonthSliderControl.MonthFlag = 2;
        }

        private void ReturnDate_Loaded(object sender, RoutedEventArgs e)
        {
            DateSliderControl returnDateSliderControl = (DateSliderControl)sender;
            returnDateSliderControl.PointerEnterEvent += returnDateSliderControl_PointerEnterEvent;
        }

        void returnDateSliderControl_PointerEnterEvent(object sender, PointerRoutedEventArgs e)
        {
            DateSliderControl.DateFlag = 2;
        }






        #region SliderInfoGridEventHandlers


        private void DepartureInfoGridControl_Loaded(object sender, RoutedEventArgs e)
        {
            departureInfoGridControl = (InfoGridControl)sender;
        }

        private void DepartureButton_Loaded(object sender, RoutedEventArgs e)
        {
            InfoButton departureInfoButton = (InfoButton)sender;
            departureInfoButton.ButtonClickedEvent += departureInfoButton_ButtonClickedEvent;

        }




        void departureInfoButton_ButtonClickedEvent(object sender, RoutedEventArgs e)
        {

            if (departureInfoFlag)
            {

                departureInfoGridControl.ShowInfoGrid(InitializeBooking.sliderUserControlData.DepartureInfo);
            }
            else
            {
                departureInfoGridControl.HideInfoGrid();

            }

            departureInfoFlag = !departureInfoFlag;
        }

        private void ReturnButton_Loaded(object sender, RoutedEventArgs e)
        {
            InfoButton returnInfoButton = (InfoButton)sender;
            returnInfoButton.ButtonClickedEvent += returnInfoButton_ButtonClickedEvent;
        }

        void returnInfoButton_ButtonClickedEvent(object sender, RoutedEventArgs e)
        {
            if (returnInfoFlag)
            {

                returnInfoGridControl.ShowInfoGrid(InitializeBooking.sliderUserControlData.ReturnInfo);
            }
            else
            {
                returnInfoGridControl.HideInfoGrid();

            }

            returnInfoFlag = !returnInfoFlag;
        }

        private void ReturnInfoGridControl_Loaded(object sender, RoutedEventArgs e)
        {
            returnInfoGridControl = (InfoGridControl)sender;
        }

        #endregion SliderInfoGridEventHandlers

        #endregion SliderEventHandlers





        #region ComboBoxEventHandlers
        private void AdultsComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            _InitializeBooking.comboDataSource.ChildrenComboData.Clear();
            _InitializeBooking.comboDataSource.InfantsComboData.Clear();
            try
            {


                for (int i = 0; i <= (StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().BookingLimit - Convert.ToInt32((string)e.AddedItems[0])); i++)
                {
                    _InitializeBooking.comboDataSource.ChildrenComboData.Add(i.ToString());
                }
                for (int i = 0; i <= Convert.ToInt32(_InitializeBooking.comboDataSource.AdultsComboData[_InitializeBooking.comboDataSource.AdultsSelectedIndex]); i++)
                {
                    _InitializeBooking.comboDataSource.InfantsComboData.Add(i.ToString());
                }

                _InitializeBooking.comboDataSource.ChildrenSelectedIndex = _InitializeBooking.comboDataSource.ChildrenComboData.Count - 1;
                _InitializeBooking.comboDataSource.InfantsSelectedIndex = _InitializeBooking.comboDataSource.InfantsComboData.Count - 1;

                _InitializeBooking.comboDataSource.ChildrenSelectedIndex = 0;
                _InitializeBooking.comboDataSource.InfantsSelectedIndex = 0;
            }
            catch (Exception)
            {
                _InitializeBooking.comboDataSource.ChildrenComboData.Clear();
                _InitializeBooking.comboDataSource.InfantsComboData.Clear();

                _InitializeBooking.comboDataSource.ChildrenComboData.Add("0");
                _InitializeBooking.comboDataSource.InfantsComboData.Add("0");


            }
        }
        #endregion ComboBoxEventHandlers





        #region CheckButtonEvents

        private Button checkButton;
        private void CheckButton_Loaded(object sender, RoutedEventArgs e)
        {
            checkButton = (Button)sender;
        }

        private void CheckButton_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (checkButton.IsEnabled)
                VisualStateManager.GoToState(checkButton, "CustomState", true);
        }
        #endregion CheckButtonEvents

        #endregion BookFlightInitialization

        /// <summary>
        /// Invoked when a HubSection header is clicked.
        /// </summary>
        /// <param name="sender">The Hub that contains the HubSection whose header was clicked.</param>
        /// <param name="e">Event data that describes how the click was initiated.</param>

        /// <summary>
        /// Invoked when an item within a section is clicked.
        /// </summary>
        /// <param name="sender">The GridView or ListView
        /// displaying the item clicked.</param>
        /// <param name="e">Event data that describes the item clicked.</param>


        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        #region NavigationHelper registration

        private void StopTimers()
        {
            _InitializeBooking.StopTimer();
            if (DepartureImageUserControl != null)
                DepartureImageUserControl.StopTimers();
            if (ReturnImageUserControl != null)
                ReturnImageUserControl.StopTimers();
            if (ClosestFlightTimer != null && ClosestFlightTimer.IsEnabled)
                ClosestFlightTimer.Stop();
        }
        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// 
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="GridCS.Common.NavigationHelper.LoadState"/>
        /// and <see cref="GridCS.Common.NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {

            navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {

            navigationHelper.OnNavigatedFrom(e);
        }

        #endregion



        private void Check_Button_Click(object sender, RoutedEventArgs e)
        {

            StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().DepartDate = new BookFlightDataModel.FlightDate();
            StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().DepartDate.Year = SliderUserControlData.DepartureData.YearData;
            StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().DepartDate.Month = SliderUserControlData.DepartureData.MonthData;
            StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().DepartDate.Day = SliderUserControlData.DepartureData.DayData;

            StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().ReturnDate = new BookFlightDataModel.FlightDate();
            StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().ReturnDate.Year = SliderUserControlData.ReturnData.YearData;
            StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().ReturnDate.Month = SliderUserControlData.ReturnData.MonthData;
            StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().ReturnDate.Day = SliderUserControlData.ReturnData.DayData;

            StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().PromoCode = _InitializeBooking.comboDataSource.PromoComboData[_InitializeBooking.comboDataSource.PromoSelectedIndex];
            StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().CabinClass = _InitializeBooking.comboDataSource.CabinComboData[_InitializeBooking.comboDataSource.CabinSelectedIndex];

            StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().NoOfAdults = Convert.ToInt32(_InitializeBooking.comboDataSource.AdultsComboData[_InitializeBooking.comboDataSource.AdultsSelectedIndex]);
            StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().NoOfInfants = Convert.ToInt32(_InitializeBooking.comboDataSource.InfantsComboData[_InitializeBooking.comboDataSource.InfantsSelectedIndex]);
            StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().NoOfChildren = Convert.ToInt32(_InitializeBooking.comboDataSource.ChildrenComboData[_InitializeBooking.comboDataSource.ChildrenSelectedIndex]);

            StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().OneWayTicket = InitializeBooking.radioData.OneWayTicket;
            StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().RoundTripTicket = InitializeBooking.radioData.RoundTripTicket;
            StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().FlexibleDate = InitializeBooking.radioData.FlexibleDate;
            StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().FixedDate = InitializeBooking.radioData.FixedDate;

            FlightSelection.IsInitialized = false;

            this.StopTimers();
            this.Frame.Navigate(typeof(FlightSelection));
        }

        private void ReservationListControl_Loaded(object sender, RoutedEventArgs e)
        {

            _ReservationListColtrol = ((ReservationListControl)sender);
            ((ReservationListControl)sender).DataContext = StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoColletor();



            ((ReservationListControl)sender).ReservationListItemCick += HubPage_ReservationListItemCick;

            ClosestFlightTimer = new DispatcherTimer();
            ClosestFlightTimer.Interval = new TimeSpan(0, 0, 1, 0);
            ClosestFlightTimer.Tick += ClosestFlightTimer_Tick;
            ClosestFlightTimer.Start();
            ClosestFlightTimer_Tick(null, null);


            if (StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoColletor().ReservationInfoCollection.Count == 0)
            {
                _ReservationListColtrol.Visibility = Visibility.Collapsed;
                DefaultViewModel["EmptyText"] = "No Reservation Information Available";
                DefaultViewModel["EmptyTextVisibility"] = Visibility.Visible;

            }
            else
            {
                _ReservationListColtrol.Visibility = Visibility.Visible;
                DefaultViewModel["EmptyText"] = "";
                DefaultViewModel["EmptyTextVisibility"] = Visibility.Collapsed;
            }

        }


        void ClosestFlightTimer_Tick(object sender, object e)
        {
            foreach (var v in StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoColletor().ReservationInfoCollection)
            {
                v.UpdateClosestFlightString();
            }

            List<ReservationInfoDataModel> TempList = new List<ReservationInfoDataModel>(StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoColletor().ReservationInfoCollection.OrderBy(x => x.MinTimeToFlight.TotalMinutes)).ToList();

            StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoColletor().ReservationInfoCollection.Clear();

            foreach (var v in TempList)
            {
                StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoColletor().ReservationInfoCollection.Add(v);

            }
        }


        void HubPage_ReservationListItemCick(object sender, ItemClickEventArgs e)
        {
            StoryCollection.StoryCollectionInstance.ViewReservation.SetReservationInfoDataModel((ReservationInfoDataModel)e.ClickedItem);
            this.StopTimers();
            this.Frame.Navigate(typeof(ReservationInfoPage));
        }

        private void AppBar_Closed(object sender, object e)
        {
            HubPageAppBar.ResetAppBar();
        }


        private void FindAppBarControl_Loaded(object sender, RoutedEventArgs e)
        {
            if (HubPageAppBar == null)
            {
                HubPageAppBar = (FindAppBarControl)sender;
                HubPageAppBar.SetAppBar(0);
                HubPageAppBar.FindButtonTapped += HubPageAppBar_FindButtonTapped;
            }
        }

        async void HubPageAppBar_FindButtonTapped(object sender, TappedRoutedEventArgs e)
        {

            LoadingGrid.Visibility = Visibility.Visible;
            LoadingHideStoryboard.Stop();
            LoadingShowStoryboard.Begin();
            LoadingRing.IsActive = true;


            try
            {
                StoryCollection.StoryCollectionInstance.ViewReservation.SetReservationInfoDataModel(await StoryCollection.StoryCollectionInstance.ViewReservation.GetBookingSearchResult(HubPageAppBar.BookingText, HubPageAppBar.NameText));
                LoadingHideStoryboard.Begin();
                LoadingShowStoryboard.Stop();



                if (!StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoDataModel().HasErrors)
                {
                    this.StopTimers();
                    this.Frame.Navigate(typeof(ReservationInfoPage));
                    return;
                }
                else
                {
                    MessageDialog msg = new MessageDialog(StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoDataModel().ErrorMessage);
                    await msg.ShowAsync();
                }
            }
            catch (Exception ex)
            {
                MessageDialog msg = new MessageDialog(ex.Message);
                msg.ShowAsync();
                LoadingHideStoryboard.Begin();
                LoadingShowStoryboard.Stop();


            }
        }

        private void LoadingHideStoryBoard_Completed(object sender, object e)
        {
            LoadingRing.IsActive = false;
            LoadingGrid.Visibility = Visibility.Collapsed;
        }

    }
}
