# Universal App for booking tickets for Biman Bangladesh Airlines #
* HTML Scraping to gather data from Biman Web pages
* Bing Map INtegration
* MVVM Architecture
* Animation in XAML
* Unit testing
* Windows store app link:
[https://www.microsoft.com/en-us/store/apps/fly-with-biman/9wzdncrdf50h](Link URL) [Copy and paste the link in browser's address bar to go to the app page]
* Windows Phone App development ongoing