﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BimanPortableClassLibrary.DataModel;
using BimanPortableClassLibrary.Stories;
using System.Net;
using System.IO;
using System.Text;
using HtmlAgilityPack;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;


namespace HtmlScrapingTest
{
    [TestClass]
    public class FlightBookingTest
    {
        #region TestFlightSelection
        private FlightSelectionDataModel.FlightDetails flightDetails = null;
        private FlightSelectionDataModel flightSelectionDataModel = new FlightSelectionDataModel();
        private int FlightIDFlag = -1;
        private bool flag = false;
        private bool ErrorFlag = false;

        // [TestMethod]
        public void TestFlightSelection()
        {
            #region For_Getting_Detailed_Price
            string url1 = "http://www.biman-airlines.com/bookings/flight_selection.aspx";

            string url2 = "http://www.biman-airlines.com/bookings/flight_selection.aspx?TT=RT&FL=on&DC=DAC&AC=RGN&AM=2013-12&AD=26&DC=&AC=&AM=&AD=&DC=&AC=&AM=&AD=&DC=&AC=&AM=&AD=&RM=2014-01&RD=31&PA=2&PC=1&PI=2&CC=C&NS=&CD=HELLO-YANGON";
            url2 = "http://www.biman-airlines.com/bookings/flight_selection.aspx?TT=OW&FL=on&DC=CGP&AC=FCO&AM=2013-12&AD=26&DC=&AC=&AM=&AD=&DC=&AC=&AM=&AD=&DC=&AC=&AM=&AD=&RM=2013-12&RD=28&PA=1&PC=&PI=&CC=&NS=&CD=";

            //url2 = "http://www.biman-airlines.com/bookings/flight_selection.aspx?TT=RT&FL=on&DC=FCO&AC=DAC&AM=2014-01&AD=01&DC=&AC=&AM=&AD=&DC=&AC=&AM=&AD=&DC=&AC=&AM=&AD=&RM=2014-01&RD=31&PA=1&PC=&PI=&CC=&NS=&CD=";

            //url2 = "http://www.biman-airlines.com/bookings/flight_selection.aspx?TT=RT&FL=on&DC=DAC&AC=CGP&AM=2014-01&AD=31&DC=&AC=&AM=&AD=&DC=&AC=&AM=&AD=&DC=&AC=&AM=&AD=&RM=2014-02&RD=08&PA=1&PC=&PI=&CC=&NS=&CD=";

            url2 = "http://www.biman-airlines.com/bookings/flight_selection.aspx?TT=RT&FL=on&DC=AUH&AC=HKG&AM=2013-12&AD=12&DC=&AC=&AM=&AD=&DC=&AC=&AM=&AD=&DC=&AC=&AM=&AD=&RM=2013-12&RD=21&PA=1&PC=&PI=&CC=&NS=&CD=";

            var cookies = new CookieContainer();
            ServicePointManager.Expect100Continue = false;


            var request = (HttpWebRequest)WebRequest.Create(url2);
            request.CookieContainer = cookies;

            request.Method = "GET";
            request.ContentType = "application/x-www-form-urlencoded";
            request.UserAgent = "Mozilla/5.0 (Windows NT 6.3; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0";
            request.KeepAlive = true;




            var WebGet = new HtmlDocument();
            using (var responseStream = request.GetResponse().GetResponseStream())
            using (var reader = new StreamReader(responseStream))
            {

                WebGet.Load(reader.BaseStream);

                string result = WebGet.DocumentNode.InnerHtml;
                FileStream file = File.Create("TestFileGet.htm");

                file.Write(new ASCIIEncoding().GetBytes(result), 0, result.Count() - 1);
                file.Close();
            }




            var nodes = WebGet.DocumentNode.SelectNodes("//div[@class='errors' or @class='warnings']");
            ErrorFlag = HasErrors(nodes);

            if (ErrorFlag) return;


            string strFsc = null, strTrip_1 = null, strTrip_2 = null;

            flightSelectionDataModel.FlightCollection = new ObservableCollection<FlightSelectionDataModel.FlightDetails>();

            nodes = WebGet.DocumentNode.SelectNodes("//div[@id='trip_1']/table");
            if (nodes != null)
                GetFlightData(nodes);

            nodes = WebGet.DocumentNode.SelectNodes("//div[@id='trip_2']/table");
            if (nodes != null)
                GetFlightData(nodes);

            nodes = WebGet.DocumentNode.SelectNodes("//div[@id='trip_1']/table");
            GetBaggageAllowanceDetails(nodes);

            //foreach (FlightSelectionDataModel.FlightDetails v in flightSelectionDataModel.FlightCollection)
            //{

            //}

            //foreach (var v in flightSelectionDataModel.BaggageAllowanceCollection)
            //{

            //}

        }


        public bool HasErrors(HtmlNodeCollection nodes)
        {
            if (nodes.Count == 0) return false;

            flightSelectionDataModel.ErrorCollection = new ObservableCollection<string>();

            foreach (var v in nodes)
            {
                GetErrorStrings(v);
            }

            return true;
        }


        public void GetErrorStrings(HtmlNode node)
        {
            if (!node.HasChildNodes)
            {
                if (node.ParentNode.Name == "h2" && new Regex(@"\S.*").Match(node.InnerText).Value != "")
                {
                    flightSelectionDataModel.ErrorHeader = new Regex(@"\S.*").Match(node.InnerText).Value;
                    Debug.WriteLine(new Regex(@"\S.*").Match(node.InnerText).Value);
                }
                else if (new Regex(@"\S.*").Match(node.InnerText).Value != "")
                {
                    flightSelectionDataModel.ErrorCollection.Add(new Regex(@"\S.*").Match(node.InnerText).Value);
                    Debug.WriteLine(new Regex(@"\S.*").Match(node.InnerText).Value);
                }
                return;
            }

            foreach (var v in node.ChildNodes)
            {
                GetErrorStrings(v);
            }
        }


        public void GetBaggageAllowanceDetails(HtmlNodeCollection tableNodes)
        {
            flightSelectionDataModel.BaggageAllowanceCollection = new ObservableCollection<FlightSelectionDataModel.BaggageAllowance>();
            foreach (HtmlNode tableNode in tableNodes)
            {
                try
                {
                    HtmlNode thNodes = tableNode.ChildNodes.First(x => x.Name == "thead").ChildNodes.First(x => x.Name == "tr");

                    foreach (var th in thNodes.ChildNodes)
                    {


                        if (th.Attributes["class"] != null && th.Attributes["class"].Value.Split(' ')[0] == "family")
                        {


                            FlightSelectionDataModel.BaggageAllowance baggageAllowance = new FlightSelectionDataModel.BaggageAllowance();

                            if (th.Attributes["class"].Value.Split(' ')[1] == "family-SS")
                            {
                                if (th.ChildNodes.First(x => x.Name == "span").Attributes["title"] != null)
                                {
                                    baggageAllowance.CabinFamilyName = "Super Saver";


                                }


                            }
                            else if (th.Attributes["class"].Value.Split(' ')[1] == "family-ES")
                            {
                                if (th.ChildNodes.First(x => x.Name == "span").Attributes["title"] != null)
                                {
                                    baggageAllowance.CabinFamilyName = "Economy Saver";


                                }
                            }
                            else if (th.Attributes["class"].Value.Split(' ')[1] == "family-EF")
                            {
                                if (th.ChildNodes.First(x => x.Name == "span").Attributes["title"] != null)
                                {
                                    baggageAllowance.CabinFamilyName = "Economy Flexible";


                                }
                            }
                            else if (th.Attributes["class"].Value.Split(' ')[1] == "family-BS")
                            {
                                if (th.ChildNodes.First(x => x.Name == "span").Attributes["title"] != null)
                                {
                                    baggageAllowance.CabinFamilyName = "Business Saver";


                                }
                            }
                            else if (th.Attributes["class"].Value.Split(' ')[1] == "family-BF")
                            {
                                if (th.ChildNodes.First(x => x.Name == "span").Attributes["title"] != null)
                                {
                                    baggageAllowance.CabinFamilyName = "Business Flexible";


                                }
                            }

                            string[] strArr = GetBaggageClassification(th.ChildNodes.First(x => x.Name == "span").Attributes["title"].Value);
                            baggageAllowance.ChangeOrRefund = strArr[0];
                            baggageAllowance.FromBangladesh = strArr[1];
                            baggageAllowance.OutsideBangladesh = strArr[2];
                            baggageAllowance.Domestic = strArr[3];

                            flightSelectionDataModel.BaggageAllowanceCollection.Add(baggageAllowance);


                        }

                    }
                    if (flightSelectionDataModel.BaggageAllowanceCollection != null) break;

                }
                catch (Exception)
                {
                    continue;

                }
            }
        }




        public string[] GetBaggageClassification(string baggage)
        {
            string[] strFinal = new string[4];
            string[] strArr = baggage.Split('\r');

            strFinal[0] = new Regex(@"(\S).*").Match(WebUtility.HtmlDecode(strArr[1])).Value;
            Regex rgx2 = new Regex(@"\d\d\w\w");

            strFinal[1] = rgx2.Match(strArr[3]).Value;
            strFinal[2] = rgx2.Match(strArr[4]).Value;
            strFinal[3] = rgx2.Match(strArr[5]).Value;

            return strFinal;
        }

        public void GetFlightData(HtmlNodeCollection nodes)
        {






            // nodes.count=7;
            foreach (var tag in nodes)
            {



                var tableRows = from tbodies in tag.ChildNodes
                                where tbodies.Name == "tbody"
                                from trs in tbodies.ChildNodes
                                where trs.Name == "tr"
                                select trs;


                foreach (var row in tableRows)
                {
                    flightDetails = new FlightSelectionDataModel.FlightDetails();
                    flightDetails.CabinFamilyCollection = new ObservableCollection<FlightSelectionDataModel.FlightDetails.CabinFamily>();

                    if (tag.Attributes["id"] != null)
                    {
                        string[] strArr = tag.Attributes["id"].Value.Split('_');
                        if (strArr.Count() == 6)
                        {
                            flightDetails.TripID = strArr[0] + "_" + strArr[1];
                            flightDetails.FlightYear = strArr[3];
                            flightDetails.FlightMonth = strArr[4];
                            flightDetails.FlightDay = strArr[5];

                        }



                    }

                    if (row.ChildNodes.Count <= 3) continue;
                    foreach (var childNode in row.ChildNodes)
                    {


                        tdClass = null;

                        GetNodeText(childNode);



                    }

                    if (flag)
                    {

                        FlightIDFlag++;
                        flightDetails.FlightID = FlightIDFlag.ToString();
                    }
                    else
                    {
                        flightDetails.FlightID = FlightIDFlag.ToString();
                    }
                    flag = false;

                    flightSelectionDataModel.FlightCollection.Add(flightDetails);

                }


            }



            #endregion For_Getting_Detailed_Price
        }

        string tdClass = null;
        Regex rgx = new Regex(@"\S+");
        public void GetNodeText(HtmlNode tempNode)
        {

            if (tempNode.Name == "td" && tempNode.Attributes["class"] != null)
            {
                tdClass = tempNode.Attributes["class"].Value;
            }

            if (!tempNode.HasChildNodes)
            {


                if (rgx.Match(tempNode.InnerText).Value.Length == 0)
                {

                    return;
                }
                else
                {


                    if (tdClass == "flight")
                    {
                        flightDetails.FlightName = tempNode.InnerText;

                    }
                    else if (tdClass == "time leaving")
                    {
                        if (flightDetails.TakeOff == null || flightDetails.TakeOff == "")
                            flightDetails.TakeOff = tempNode.InnerText;
                        else
                        {
                            flightDetails.TakeOffTimeShift = tempNode.InnerText;
                        }
                    }
                    else if (tdClass == "route")
                    {
                        if (flightDetails.Route == null || flightDetails.Route == "")
                        {
                            flightDetails.Route = tempNode.InnerText;
                        }
                        else
                        {
                            flightDetails.Route += " " + tempNode.InnerText;
                        }
                    }
                    else if (tdClass == "time landing")
                    {

                        if (flightDetails.Landing == null || flightDetails.Landing == "")
                        {
                            flightDetails.Landing = tempNode.InnerText;
                        }
                        else
                        {
                            flightDetails.LandingTimeShift = tempNode.InnerText;
                        }
                    }
                    else if (tdClass.Split(' ')[0] == "family")
                    {

                        //Flights can be segmented with stos at airports other than the destination.
                        //This flag is required to mark those flights.
                        flag = true;

                        FlightSelectionDataModel.FlightDetails.CabinFamily cabinFamily = new FlightSelectionDataModel.FlightDetails.CabinFamily();
                        if (tdClass.Split(' ')[1] == "family-SS")
                        {
                            cabinFamily.CabinFamilyName = "Super Saver";
                            cabinFamily.Price = tempNode.InnerText;

                        }
                        else if (tdClass.Split(' ')[1] == "family-ES")
                        {
                            cabinFamily.CabinFamilyName = "Economy Saver";
                            cabinFamily.Price = tempNode.InnerText;
                        }
                        else if (tdClass.Split(' ')[1] == "family-EF")
                        {
                            cabinFamily.CabinFamilyName = "Economy Flexible";
                            cabinFamily.Price = tempNode.InnerText;
                        }
                        else if (tdClass.Split(' ')[1] == "family-BS")
                        {
                            cabinFamily.CabinFamilyName = "Business Saver";
                            cabinFamily.Price = tempNode.InnerText;
                        }
                        else if (tdClass.Split(' ')[1] == "family-BF")
                        {
                            cabinFamily.CabinFamilyName = "Business Flexible";
                            cabinFamily.Price = tempNode.InnerText;
                        }

                        try
                        {
                            HtmlNode inputNode = tempNode.ParentNode.ParentNode.ChildNodes.First(x => x.Name == "input" && x.Attributes["type"].Value == "radio");


                            if (inputNode.Attributes["value"] != null)
                                cabinFamily.EncodedValue = inputNode.Attributes["value"].Value;
                        }
                        catch (Exception)
                        {

                        }

                        try
                        {
                            HtmlNode labelNode = tempNode.ParentNode.ParentNode;
                            if (labelNode.Name == "label" && labelNode.Attributes["data-title"] != null)
                            {
                                string[] strArr = labelNode.Attributes["data-title"].Value.Split(' ');
                                cabinFamily.TotalPrice = strArr[2] + " " + strArr[3];
                            }
                        }
                        catch (Exception)
                        {

                        }

                        flightDetails.CabinFamilyCollection.Add(cabinFamily);
                    }





                }
            }

            foreach (HtmlNode node in tempNode.ChildNodes)
            {
                GetNodeText(node);
            }

        }

        #endregion TestFlightSelection


        #region TestFlightBookingInfo


        private BookFlightDataModel bookFlightDataModel = new BookFlightDataModel();

        //[TestMethod]
        public void TestFlightBookingInfo()
        {
            string url2 = "http://www.biman-airlines.com/home";
            var cookies = new CookieContainer();
            ServicePointManager.Expect100Continue = false;


            var request = (HttpWebRequest)WebRequest.Create(url2);
            request.CookieContainer = cookies;

            request.Method = "GET";
            request.ContentType = "application/x-www-form-urlencoded";
            request.UserAgent = "Mozilla/5.0 (Windows NT 6.3; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0";
            request.KeepAlive = true;




            var WebGet = new HtmlDocument();
            using (var responseStream = request.GetResponse().GetResponseStream())
            using (var reader = new StreamReader(responseStream))
            {

                WebGet.Load(reader.BaseStream);

                FileStream file = File.Create("TestFileGet.htm");

            }

            bookFlightDataModel.CityList = new ObservableCollection<BookFlightDataModel.City>();

            //var nodes = from comboBox in WebGet.DocumentNode.Descendants()
            //            where comboBox.Name == "select" && comboBox.Attributes["name"].Value == "DC"
            //            select new
            //            {
            //               childCollection =comboBox.ChildNodes
            //            };

            var nodes = from comboBox in WebGet.DocumentNode.Descendants()
                        where comboBox.Name == "select" && comboBox.Attributes["name"].Value == "DC"
                        select comboBox;
            foreach (var v in nodes)
            {
                Debug.WriteLine(v.InnerHtml);
            }



            //WebGet.DocumentNode.SelectNodes("//select[@name='DC']");
            foreach (var selectNode in nodes)
            {
                foreach (var v in selectNode.ChildNodes)
                {

                    if (v.InnerText != "" && v.InnerText.Split('(').Count() > 1)
                    {
                        BookFlightDataModel.City city = new BookFlightDataModel.City();
                        //Greedy Search Regex*** Because there can be cities with names with multiple words. 
                        //The regex is applied to ommit the last space after the name.
                        city.Name = new Regex(@"(\S).+(\S)").Match(v.InnerText.Split('(')[0]).Value;
                        Debug.Write(city.Name);
                        city.CityID = new Regex(@"(\S).+(\w|\d)").Match(v.InnerText.Split('(')[1]).Value;
                        Debug.WriteLine(": " + city.CityID);

                        bookFlightDataModel.CityList.Add(city);

                    }
                }

            }

            //var nodes2 = WebGet.DocumentNode.SelectNodes("//select[@name='PA']");
            //var var2 = from comboBox in WebGet.DocumentNode.Descendants()
            //           where comboBox.Name == "select" && comboBox.Attributes["name"].Value == "PA"
            //           select comboBox.ChildNodes;


            //if (nodes != null)
            //{
            //    bookFlightDataModel.BookingLimit = ((HtmlNodeCollection)var2)[0].ChildNodes.Count;
            //    //bookFlightDataModel.BookingLimit = nodes2[0].ChildNodes.Count;
            //}




            var nodes2 = (from combo in WebGet.DocumentNode.Descendants()
                          where combo.Name == "select" && combo.Attributes["name"].Value == "PA"
                          select combo.ChildNodes).First();
            if (nodes != null)
            {
                int count = 0;
                foreach (var v in nodes2)
                {
                    if (new Regex(@"(\S).*(\d)").Match(v.InnerText).Value != "")
                    {
                        count++;
                    }

                }
                bookFlightDataModel.BookingLimit = count;
            }
        }

        #endregion TestFlightBookingInfo

        #region FlightSelection

        private delegate void MyDelegate();

        [TestMethod]
        public void TestMethodToFlightSelection()
        {


            MyDelegate dlgt = new MyDelegate(TestFlightSelectionAsync);

            IAsyncResult ar = dlgt.BeginInvoke(null, null);
            //Thread.Sleep(10000);
            ar.AsyncWaitHandle.WaitOne();
            dlgt.EndInvoke(ar);
            ar.AsyncWaitHandle.Close();
            while (true) { ;}

        }

        public async void TestFlightSelectionAsync()
        {
            Debug.WriteLine("I'm In");
            //IF

            string url1 = "https://www.biman-airlines.com/bookings/captcha.aspx";

            string url2 = "https://www.biman-airlines.com/bookings/flight_selection.aspx?TT=RT&DC=DAC&AC=CGP&AM=2016-06&AD=08&RM=2016-06&RD=10&FL=on&PA=1&PT=&PC=&PI=&CC=&CD=&x=0&y=0&FS=FA22A789";

            var cookies = new CookieContainer();



            var request = (HttpWebRequest)WebRequest.Create(url1);
            request.CookieContainer = cookies;

            request.Method = "HEAD";
            request.ContentType = "text/html; charset=utf-8";
            request.Headers.Add("X-Hash-Validate", "TT=RT&DC=DAC&AC=CGP&AM=2016-06&AD=08&RM=2016-06&RD=10&FL=on&PA=1&PT=&PC=&PI=&CC=&CD=");

            using (var responseStream = await request.GetResponseAsync())
            {
                string str = responseStream.Headers.Get("X-Hash");
                Debug.WriteLine(str);
            }


            var request2 = (HttpWebRequest)WebRequest.Create(url2);
            request2.CookieContainer = cookies;

            request2.Method = "GET";
            request2.ContentType = "text/html; charset=utf-8";
            request2.AllowAutoRedirect = true;
            var WebGet = new HtmlDocument();
            using (var responseStream = await request2.GetResponseAsync())
            {



                using (Stream stream = responseStream.GetResponseStream())
                {
                    using (var reader = new StreamReader(stream))
                    {
                        // Debug.WriteLine("I'm Through");
                        WebGet.Load(reader.BaseStream);
                        Debug.WriteLine(WebGet.DocumentNode.InnerHtml);

                    }

                }
            }

            //foreach (var v in request.CookieContainer.GetCookies(new Uri(url1)))
            //{
            //    Debug.WriteLine(v);
            //}
            //Debug.WriteLine("\n\n");
            //foreach(var v in request.CookieContainer.GetCookies(new Uri(url2))){
            //    Debug.WriteLine(v);
            //}

            //try
            //{
            //    var nodes = from div in WebGet.DocumentNode.Descendants()
            //                where div.Name == "div" && div.Attributes["class"] != null && (div.Attributes["class"].Value == "errors" || div.Attributes["class"].Value == "warnings")
            //                select div;

            //    //foreach (var v in (HtmlNodeCollection)nodes)
            //    //{
            //    //    Debug.WriteLine(nodes.Count());
            //    //}

            //    ErrorFlag = HasErrors(nodes);
            //}
            //catch (Exception)
            //{

            //}

            Debug.WriteLine("I'm Out");
        }

        public bool HasErrors(IEnumerable<HtmlNode> nodes)
        {
            if (nodes.Count() == 0) return false;

            flightSelectionDataModel.ErrorCollection = new ObservableCollection<string>();


            foreach (var v in nodes)
            {
                //foreach(var node in v.ChildNodes)
                GetErrorStrings(v);
            }

            return true;
        }

        #endregion FlightSelection



        #region TestReservationInfo

        public void TestReservation()
        {
            var cookies = new CookieContainer();
            string url = "passengerName=Abdullah&PNR=HVPB6&x=44&y=15";
            string mainUrl = "https://www.biman-airlines.com/bookings/view_reservation.aspx";
            var request = (HttpWebRequest)WebRequest.Create(mainUrl);
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            request.CookieContainer = cookies;
            request.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";


            using (var requestStream = request.GetRequestStream())
            {
                using (var writer = new StreamWriter(requestStream))
                {
                    writer.Write(url);

                }
            }


            var WebGet = new HtmlDocument();
            using (var response = request.GetResponse())
            {
                using (Stream stream = response.GetResponseStream())
                {
                    using (var reader = new StreamReader(stream))
                    {

                        WebGet.Load(reader.BaseStream);

                    }
                }
            }

            bool HasErrors = false;
            string ErrorDescription = "";
            var errors = from div in WebGet.DocumentNode.Descendants()
                         where div.Name == "div" && div.Attributes["class"] != null && (div.Attributes["class"].Value == "errors" || div.Attributes["class"].Value == "warnings")
                         select div;
            if (errors.Count() > 0)
            {
                HasErrors = true;
                var error = from e in WebGet.DocumentNode.Descendants()
                            where e.Name == "div" && e.Attributes["class"] != null && e.Attributes["class"].Value == "error"
                            select e;
                foreach (var e in error)
                {
                    ErrorDescription = e.InnerText;
                }

                Debug.WriteLine(ErrorDescription);
                return;
            }
            else HasErrors = false;

            var names = from d in WebGet.DocumentNode.Descendants()
                        where d.Name == "div" && d.Attributes["class"] != null && d.Attributes["class"].Value == "passengers"
                        from t in d.ChildNodes
                        where t.Name == "table"
                        from tb in t.ChildNodes
                        where tb.Name == "tbody"
                        from tr in tb.ChildNodes
                        where tr.Name == "tr"
                        select tr;


            foreach (var name in names)
            {
                int count = 1;
                foreach (var n in name.ChildNodes)
                {
                    if (count % 2 == 0)
                    {

                        Debug.WriteLine(new Regex(@"(\S).*").Match(n.InnerText).Value);
                    }
                    count++;


                }
            }

            var itineraries = from i in WebGet.DocumentNode.Descendants()
                              where i.Name == "div" && i.Attributes["class"] != null && i.Attributes["class"].Value == "itinerary"
                              from t in i.ChildNodes
                              where t.Name == "table"
                              select t;
            if (itineraries.Count() == 1)
                ;
            else ;

            Debug.WriteLine("__________________________________________________________");
            foreach (var itinerary in itineraries)
            {
                var tds = from tb in itinerary.ChildNodes
                          where tb.Name == "tbody"
                          from tr in tb.ChildNodes
                          where tr.Name == "tr"
                          from td in tr.ChildNodes
                          where td.Name == "td"
                          select td;

                foreach (var td in tds)
                {
                    foreach (var str in td.ChildNodes)
                    {
                        if (new Regex(@"(\S).*").Match(str.InnerText).Value != "")
                        {
                            Debug.WriteLine(new Regex(@"(\S).*").Match(str.InnerText).Value);
                        }
                    }


                }
                Debug.WriteLine("__________________________________________________________");
            }


            //total cost
            var total_costs = from d in WebGet.DocumentNode.Descendants()
                              where d.Name == "div" && d.Attributes["class"] != null && d.Attributes["class"].Value == "ticket_details"
                              from t in d.ChildNodes
                              where t.Name == "table"
                              from tf in t.ChildNodes
                              where tf.Name == "tfoot"
                              from tr in tf.ChildNodes
                              where tr.Name == "tr"
                              from th in tr.ChildNodes
                              where th.Name == "th" && th.Attributes["class"] != null && th.Attributes["class"].Value == "ticket-value"
                              select th;

            Debug.WriteLine(total_costs.First().InnerText);




            //Due Amount
            var trans = from d in WebGet.DocumentNode.Descendants()
                        where d.Name == "div" && d.Attributes["class"] != null && d.Attributes["class"].Value == "transaction-status"
                        from s in d.ChildNodes
                        where s.Name == "strong"
                        select s;

            Debug.WriteLine(new Regex(@"(\S).*").Match(trans.First().InnerText).Value);



        }

        #endregion TestReservationInfo
    }

}
