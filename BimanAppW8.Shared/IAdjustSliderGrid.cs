﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BimanAppW8.Data;
using BimanAppW8.DataModel;
using BimanAppW8.Common;
using Windows.UI.Xaml.Shapes;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;

namespace BimanAppW8
{
    public class IAdjustSliderGrid
    {
        private string[] monthList = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
        public void AdjustDepartureSliderGrid(bool flag)
        {
            string strErr = "";
            try
            {
                DateTime tempDate2 = DateTime.UtcNow;
                strErr = "Departure Date is not Valid";
                DateTime tempDate = new DateTime(Convert.ToInt32(SliderUserControlData.DepartureData.YearData), monthList.ToList().FindIndex(x => x == SliderUserControlData.DepartureData.MonthData) + 1, Convert.ToInt32(SliderUserControlData.DepartureData.DayData));

                strErr = "Return Date is not Valid";
                bool tempFlag = flag;
                flag = false;
                if (!InitializeBooking.radioData.OneWayTicket)
                    tempDate2 = new DateTime(Convert.ToInt32(SliderUserControlData.ReturnData.YearData), monthList.ToList().FindIndex(x => x == SliderUserControlData.ReturnData.MonthData) + 1, Convert.ToInt32(SliderUserControlData.ReturnData.DayData));
                flag = tempFlag;
                if (!InitializeBooking.radioData.OneWayTicket && (tempDate2 - tempDate).TotalDays < 0)
                {
                    InitializeBooking.sliderUserControlData.DepartureInfoEnabled = true;
                    InitializeBooking.sliderUserControlData.DepartureInfo = "The Return Date has to be after the Departure Date.";

                    SliderUserControlData.CheckButtonVisibility.SliderStatus = false;
                    SliderUserControlData.DepartureStatus = false;

                }
                else if (tempDate < DateTime.Now.Date)
                {
                    InitializeBooking.sliderUserControlData.DepartureInfoEnabled = true;
                    InitializeBooking.sliderUserControlData.DepartureInfo = "Departure date has already passed.";

                    SliderUserControlData.CheckButtonVisibility.SliderStatus = false;
                    SliderUserControlData.DepartureStatus = false;
                }
                else
                {

                    InitializeBooking.sliderUserControlData.DepartureInfoEnabled = false;

                    SliderUserControlData.DepartureStatus = true;
                    HubPage.departureInfoGridControl.HideInfoGrid();
                    HubPage.departureInfoFlag = true;
                    SliderUserControlData.CheckButtonVisibility.SliderStatus = SliderUserControlData.DepartureStatus && SliderUserControlData.ReturnStatus;
                }

            }
            catch (Exception)
            {
                InitializeBooking.sliderUserControlData.DepartureInfoEnabled = true;
                InitializeBooking.sliderUserControlData.DepartureInfo = strErr;

                SliderUserControlData.CheckButtonVisibility.SliderStatus = false;
                SliderUserControlData.DepartureStatus = false;
            }
            finally
            {
                if (flag)
                {
                    AdjustReturnSliderGrid(false);
                    InitializeBooking.checkButtonVisibility.OverallStatus = SliderUserControlData.CheckButtonVisibility.TripStatus && SliderUserControlData.CheckButtonVisibility.TicketStatus && SliderUserControlData.CheckButtonVisibility.SliderStatus;
                }
            }
        }

        public void AdjustReturnSliderGrid(bool flag)
        {
            string ErrStr = null; ;
            try
            {
                ErrStr = "Departure date is not Valid";
                DateTime tempDate = new DateTime(Convert.ToInt32(SliderUserControlData.DepartureData.YearData), monthList.ToList().FindIndex(x => x == SliderUserControlData.DepartureData.MonthData) + 1, Convert.ToInt32(SliderUserControlData.DepartureData.DayData));
                ErrStr = "Return date is not Valid";
                bool tempFlag = flag;
                flag = false;
                DateTime tempDate2 = new DateTime(Convert.ToInt32(SliderUserControlData.ReturnData.YearData), monthList.ToList().FindIndex(x => x == SliderUserControlData.ReturnData.MonthData) + 1, Convert.ToInt32(SliderUserControlData.ReturnData.DayData));
                flag = tempFlag;


                if ((tempDate2 - tempDate).TotalDays < 0)
                {
                    InitializeBooking.sliderUserControlData.ReturnInfoEnabled = true;
                    InitializeBooking.sliderUserControlData.ReturnInfo = "The Return Date has to be after the Departure Date.";

                    SliderUserControlData.ReturnStatus = false;
                    SliderUserControlData.CheckButtonVisibility.SliderStatus = false;
                }
                else if (tempDate2 < DateTime.Now.Date)
                {
                    InitializeBooking.sliderUserControlData.ReturnInfoEnabled = true;
                    InitializeBooking.sliderUserControlData.ReturnInfo = "Return date has already passed.";

                    SliderUserControlData.ReturnStatus = false;
                    SliderUserControlData.CheckButtonVisibility.SliderStatus = false;

                }
                else
                {
                    InitializeBooking.sliderUserControlData.ReturnInfoEnabled = false;


                    SliderUserControlData.ReturnStatus = true;
                    HubPage.returnInfoGridControl.HideInfoGrid();
                    HubPage.returnInfoFlag = true;
                    SliderUserControlData.CheckButtonVisibility.SliderStatus = SliderUserControlData.DepartureStatus && SliderUserControlData.ReturnStatus; ;
                }




            }
            catch (Exception)
            {
                InitializeBooking.sliderUserControlData.ReturnInfoEnabled = true;
                InitializeBooking.sliderUserControlData.ReturnInfo = ErrStr;

                SliderUserControlData.ReturnStatus = false;
                SliderUserControlData.CheckButtonVisibility.SliderStatus = false;

            }
            finally
            {
                if (flag)
                {
                    AdjustDepartureSliderGrid(false);
                    InitializeBooking.checkButtonVisibility.OverallStatus = SliderUserControlData.CheckButtonVisibility.TripStatus && SliderUserControlData.CheckButtonVisibility.TicketStatus && SliderUserControlData.CheckButtonVisibility.SliderStatus;
                }
            }
        }

        public void AdjustSliderGrid(string Year, string Month, string Day, int flag)
        {
            DateTime ComparisionDate = DateTime.Now.Date;

            if (flag == 1)
            {
                SliderUserControlData.DepartureData.DayData = Day;
                SliderUserControlData.DepartureData.MonthData = Month;
                SliderUserControlData.DepartureData.YearData = Year;

            }
            else if (flag == 2)
            {
                SliderUserControlData.ReturnData.DayData = Day;
                SliderUserControlData.ReturnData.MonthData = Month;
                SliderUserControlData.ReturnData.YearData = Year;
            }
            try
            {
                if (flag == 1 && InitializeBooking.radioData.OneWayTicket)
                {
                    AdjustDepartureSliderGrid(false);
                    SliderUserControlData.CheckButtonVisibility.SliderStatus = SliderUserControlData.DepartureStatus;
                    InitializeBooking.checkButtonVisibility.OverallStatus = SliderUserControlData.CheckButtonVisibility.TripStatus && SliderUserControlData.CheckButtonVisibility.TicketStatus && SliderUserControlData.CheckButtonVisibility.SliderStatus;


                }
                else if (flag == 1 && !InitializeBooking.radioData.OneWayTicket)
                {
                    AdjustDepartureSliderGrid(true);
                }
                else if (flag == 2)
                {
                    AdjustReturnSliderGrid(true);
                }




            }
            catch (Exception)
            {



            }
            finally
            {

                SliderUserControlData.CheckButtonVisibility.SetOverallStatus();
            }
        }


        public void SetSliderColor(Dictionary<double,Line>LineList, int PrevLineNum, int CurrentLineNum,SolidColorBrush PrimaryColor, SolidColorBrush SecondaryColor)
        {
            if (PrevLineNum > CurrentLineNum)
            {
                for (int i = PrevLineNum; i >= CurrentLineNum; i--)
                {
                    LineList[i].Stroke = PrimaryColor;
                }
            }
            else
            {
                for (int i = CurrentLineNum; i >= PrevLineNum; i--)
                {
                    LineList[i].Stroke = SecondaryColor;
                }
            }

        }

    }
}
