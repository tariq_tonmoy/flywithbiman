﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using BimanPortableClassLibrary.DataModel;
using BimanPortableClassLibrary.Stories;
using BimanAppW8.DataModel;
using System.Collections.ObjectModel;
using BimanAppW8.Common;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace BimanAppW8
{
    public sealed partial class PassengerInfoUserControl : UserControl
    {

        private ObservableDictionary defaultViewModel = new ObservableDictionary();

        public ObservableDictionary DefaultViewModel
        {
            get { return defaultViewModel; }
            set { defaultViewModel = value; }
        }
        public PassengerInfoUserControl()
        {
            this.InitializeComponent();

            
        }

        private int _Category;

        public int Category
        {
            get { return _Category; }
            set { _Category = value; }
        }

        PassengerInfoDataModel _PassangerInfoDataModel = new PassengerInfoDataModel();//StoryCollection.StoryCollectionInstance.BookFlight.GetPassengerInfoDataModel();

        public PassengerInfoDataModel PassangerInfoDataModel
        {
            get { return _PassangerInfoDataModel; }
            set { _PassangerInfoDataModel = value; }
        }
        //category 0: Adult #1
        //category 1: Other Adults
        //category 2: Other Children
        //category 3: Other Infants
        public void SetupPassangerInfoForm(int category,int serial)
        {
            //_PassangerInfoDataModel = StoryCollection.StoryCollectionInstance.BookFlight.GetPassengerInfoDataModel();
            _PassangerInfoDataModel.PhoneCountryCollection = StoryCollection.StoryCollectionInstance.BookFlight.GetPassengerInfoDataModel().PhoneCountryCollection;
            this.Category = category;
            if (category == 0)
            {
                _PassangerInfoDataModel.SpecificAdultInfo = new PassengerInfoDataModel.AdultInfo(StoryCollection.StoryCollectionInstance.BookFlight.GetPassengerInfoDataModel().PhoneCountryCollection,StoryCollection.StoryCollectionInstance.BookFlight.GetPassengerInfoDataModel().CountryNameCollection);
                _PassangerInfoDataModel.SpecificAdultInfo.PassportExpirationDate = new PassengerInfoDataModel.BasicInfo.CalenderDate();
                _PassangerInfoDataModel.SpecificAdultInfo.BirthDate = new PassengerInfoDataModel.BasicInfo.CalenderDate();
                _PassangerInfoDataModel.SpecificAdultInfo.BirthDate.BirthYearCollection = new System.Collections.ObjectModel.ObservableCollection<string>();
                for (int i = 0; i < 100; i++)
                {
                    _PassangerInfoDataModel.SpecificAdultInfo.BirthDate.BirthYearCollection.Add((DateTime.Now.Year-i).ToString());
                }
                this.GenderCombo.Visibility = Visibility.Collapsed;
                HeaderText.Text = "Adult #" + serial + ":";
                DefaultViewModel["PersonSpecificData"] = _PassangerInfoDataModel.SpecificAdultInfo;
                DefaultViewModel["PersonGenericData"] = _PassangerInfoDataModel; 



            }
            else if (category == 1)
            {
                _PassangerInfoDataModel.SpecificAdultInfo = new PassengerInfoDataModel.AdultInfo(StoryCollection.StoryCollectionInstance.BookFlight.GetPassengerInfoDataModel().PhoneCountryCollection, StoryCollection.StoryCollectionInstance.BookFlight.GetPassengerInfoDataModel().CountryNameCollection);
                _PassangerInfoDataModel.SpecificAdultInfo.PassportExpirationDate = new PassengerInfoDataModel.BasicInfo.CalenderDate();
                _PassangerInfoDataModel.SpecificAdultInfo.BirthDate = new PassengerInfoDataModel.BasicInfo.CalenderDate();
                _PassangerInfoDataModel.SpecificAdultInfo.BirthDate.BirthYearCollection = new System.Collections.ObjectModel.ObservableCollection<string>();
                for (int i = 0; i < 100; i++)
                {
                    _PassangerInfoDataModel.SpecificAdultInfo.BirthDate.BirthYearCollection.Add(((Convert.ToInt32(StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().DepartDate.Year)-i).ToString()));
                }
                this.GenderCombo.Visibility = Visibility.Collapsed;
                AlternatePhoneGrid.Visibility = Visibility.Collapsed;
                EmailText.Visibility = Visibility.Collapsed;
                HeaderText.Text = "Adult #" + serial + ":";
                DefaultViewModel["PersonSpecificData"] = _PassangerInfoDataModel.SpecificAdultInfo;
                DefaultViewModel["PersonGenericData"] = _PassangerInfoDataModel; 
            }
            else if (category == 2)
            {
                _PassangerInfoDataModel.SpecificChildInfo = new PassengerInfoDataModel.ChildInfo(StoryCollection.StoryCollectionInstance.BookFlight.GetPassengerInfoDataModel().CountryNameCollection);
                _PassangerInfoDataModel.SpecificChildInfo.PassportExpirationDate = new PassengerInfoDataModel.BasicInfo.CalenderDate();
                _PassangerInfoDataModel.SpecificChildInfo.BirthDate = new PassengerInfoDataModel.BasicInfo.CalenderDate();
                _PassangerInfoDataModel.SpecificChildInfo.BirthDate.BirthYearCollection = new ObservableCollection<string>();
                for (int i = 0; i < 13; i++)
                {
                    _PassangerInfoDataModel.SpecificChildInfo.BirthDate.BirthYearCollection.Add(((Convert.ToInt32(StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().DepartDate.Year) - i).ToString()));
                }

                this.GenderCombo.Visibility = Visibility.Collapsed;
                ContactGrid.Visibility = Visibility.Collapsed;
                HeaderText.Text = "Child #" + serial + ":";
                DefaultViewModel["PersonSpecificData"] = _PassangerInfoDataModel.SpecificChildInfo;
                ContactHeader.Visibility = Visibility.Collapsed;
               
            }
            else
            {
                _PassangerInfoDataModel.SpecificInfantInfo = new PassengerInfoDataModel.InfantInfo(StoryCollection.StoryCollectionInstance.BookFlight.GetPassengerInfoDataModel().CountryNameCollection);
                _PassangerInfoDataModel.SpecificInfantInfo.PassportExpirationDate = new PassengerInfoDataModel.BasicInfo.CalenderDate();
                _PassangerInfoDataModel.SpecificInfantInfo.BirthDate = new PassengerInfoDataModel.BasicInfo.CalenderDate();
                _PassangerInfoDataModel.SpecificInfantInfo.BirthDate.BirthYearCollection = new ObservableCollection<string>();
                for (int i = 0; i < 3; i++)
                {
                    _PassangerInfoDataModel.SpecificInfantInfo.BirthDate.BirthYearCollection.Add((Convert.ToInt32(StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().DepartDate.Year) - i).ToString());
                }

                this.TitleCombo.Visibility = Visibility.Collapsed;
                ContactGrid.Visibility = Visibility.Collapsed;
                ContactHeader.Visibility = Visibility.Collapsed;
                ClubMemberText.Visibility = Visibility.Collapsed;
                HeaderText.Text = "Infant #" + serial + ":";
                DefaultViewModel["PersonSpecificData"] = _PassangerInfoDataModel.SpecificInfantInfo;
                
            }
        }

      
    }
}
