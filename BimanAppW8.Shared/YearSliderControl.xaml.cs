﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

using Windows.UI.Xaml.Shapes;
using Windows.UI;
using BimanAppW8.Data;
using System.Diagnostics;
using Windows.UI.Xaml.Media.Animation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace BimanAppW8
{
    public sealed partial class YearSliderControl : UserControl
    {
        private  Dictionary<double, Line> LineList = new Dictionary<double, Line>();
        private Storyboard repositionStoryboard = new Storyboard();
        private string[] monthList = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };

        private SolidColorBrush PrimaryColor = new SolidColorBrush(Colors.LightGreen), SecondaryColor = new SolidColorBrush(Colors.DarkOliveGreen);
       
        
        TextBlock textBlock = new TextBlock();
        private double size =144.0;
        private double divisor = .5;
        public static int YearFlag { get; set; }
        public delegate void ActionPointerEnter(object sender, PointerRoutedEventArgs e);

        public event ActionPointerEnter PointerEnterEvent;
        
        public YearSliderControl()
        {
            this.InitializeComponent();
            
            ContainerGrid.Height = size;
            ContainerGrid.Width = size;

            Ellipse ellipse = new Ellipse();
            SolidColorBrush scb = new SolidColorBrush() { Color = new Color() { R = 29, G = 29, B = 29, A = 255 } };
            ellipse.Fill = scb;



            ellipse.Width = size * divisor;
            ellipse.Height = size * divisor;

            textBlock.Name = "YearText";
            textBlock.HorizontalAlignment = HorizontalAlignment.Center;
            textBlock.VerticalAlignment = VerticalAlignment.Center;

            textBlock.FontSize = size * (1.0 / 3.0) * divisor;
            textBlock.Text = DateTime.Now.Year.ToString();

            textBlock.MaxHeight = size * divisor;
            textBlock.MaxWidth = size * divisor;



            for (double i = 0.0; i < 360.0; i = i + 1.0)
            {
                Line line = new Line();

                line.X1 = (size/2);
                line.Y1 = (size / 2);

                line.X2 = (size / 2) + ((size / 2) * Math.Cos((Math.PI / 180.0) * i));
                line.Y2 = (size / 2) + ((size / 2) * Math.Sin((Math.PI / 180.0) * i));

                line.StrokeThickness = 2.0;
                line.Stroke = PrimaryColor;
                line.PointerEntered += line_PointerEntered;

               

                ContainerGrid.Children.Add(line);
                LineList.Add(i, line);

                

            }

            ContainerGrid.Children.Add(ellipse);
            ContainerGrid.Children.Add(textBlock);

            SliderUserControlData.CheckButtonVisibility.SliderStatus = true;

            PrevLineNum = 0;
            
        }

        private int PrevLineNum;
        void line_PointerEntered(object sender, PointerRoutedEventArgs e)
        {
            if (PointerEnterEvent != null)
            {
                PointerEnterEvent(sender, e);
            }


            double CurrentLineNum = LineList.First(x => x.Value == (Line)sender).Key;
            new IAdjustSliderGrid().SetSliderColor(LineList, PrevLineNum, (int)CurrentLineNum, PrimaryColor, SecondaryColor);
            PrevLineNum = (int)CurrentLineNum;



            string temp = textBlock.Text;
            int flr = (int)Math.Floor(CurrentLineNum / 180.0);
            textBlock.Text = (Convert.ToInt32(DateTime.Now.Year) + flr).ToString();

            if (temp != textBlock.Text)
            {
                TestStoryboard.Begin();

                if (YearFlag == 1)
                {
                    SliderUserControlData.DepartureData.YearData = textBlock.Text;

                    new IAdjustSliderGrid().AdjustSliderGrid(SliderUserControlData.DepartureData.YearData, SliderUserControlData.DepartureData.MonthData, SliderUserControlData.DepartureData.DayData, YearFlag);
                }
                else if (YearFlag == 2)
                {
                    SliderUserControlData.ReturnData.YearData = textBlock.Text;
                    new IAdjustSliderGrid().AdjustSliderGrid(SliderUserControlData.ReturnData.YearData, SliderUserControlData.ReturnData.MonthData, SliderUserControlData.ReturnData.DayData, YearFlag);
                }

            }

            
            //for (double i = 0.0; i < 360.0; i = i + 1.0)
            //{
               



            //    //bool flag = true;
            //    //try
            //    //{
            //    //    Line line = LineList[i];
            //    //    if (flag == true && line != (Line)sender)
            //    //        line.Stroke = SecondaryColor;
            //    //    else if (line == (Line)sender)
            //    //    {
            //    //        flag = false;
            //    //        line.Stroke = SecondaryColor;


            //    //        string temp = textBlock.Text;
            //    //        int flr = (int)Math.Floor(i/180.0);
            //    //        textBlock.Text = (Convert.ToInt32(DateTime.Now.Year) + flr).ToString();

            //    //        if (temp != textBlock.Text)
            //    //        {
            //    //            TestStoryboard.Begin();
                            
            //    //            if (YearFlag == 1)
            //    //            {
            //    //                SliderUserControlData.DepartureData.YearData = textBlock.Text;

            //    //                new IAdjustSliderGrid().AdjustSliderGrid(SliderUserControlData.DepartureData.YearData, SliderUserControlData.DepartureData.MonthData, SliderUserControlData.DepartureData.DayData, YearFlag);
            //    //            }
            //    //            else if (YearFlag == 2)
            //    //            {
            //    //                SliderUserControlData.ReturnData.YearData = textBlock.Text;
            //    //                new IAdjustSliderGrid().AdjustSliderGrid(SliderUserControlData.ReturnData.YearData, SliderUserControlData.ReturnData.MonthData, SliderUserControlData.ReturnData.DayData,YearFlag);
            //    //            }

                              
                            
            //    //        }
                        

            //    //    }
            //    //    else if (flag == false)
            //    //        line.Stroke = PrimaryColor;


                   


            //    //}
            //    //catch (Exception)
            //    //{
                   
            //    //}
            //}
        }

       
    }


}
