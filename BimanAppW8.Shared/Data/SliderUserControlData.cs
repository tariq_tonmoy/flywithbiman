﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BimanAppW8.Common;
using BimanAppW8.DataModel;
using Windows.UI.Xaml;


namespace BimanAppW8.Data
{
    public  class SliderUserControlData:CommonNotifyPropertyChanged
    {
        public static class DepartureData
        {
            public static string YearData { get; set; }
            public static string MonthData { get; set; }
            public static string DayData { get; set; }
            
        
        
        }

        public static class ReturnData
        {
            public static string YearData { get; set; }
            public static string MonthData { get; set; }
            public static string DayData { get; set; }
            
        }

        public static bool _DepartureStatus=true, _ReturnStatus=true;
        
        public static bool ReturnStatus
        {
            get { return SliderUserControlData._ReturnStatus; }
            set { SliderUserControlData._ReturnStatus = value; }
        }

        public static bool DepartureStatus
        {
            get { return SliderUserControlData._DepartureStatus; }
            set { SliderUserControlData._DepartureStatus = value; }
        }


        private  Visibility _departureInfoVisibility, _returnInfoVisibility;


        private bool _departureInfoEnabled, _returnInfoEnabled;

        public bool ReturnInfoEnabled
        {
            get { return _returnInfoEnabled; }
            set { SetProperty(ref _returnInfoEnabled , value); }
        }

        public bool DepartureInfoEnabled
        {
            get { return _departureInfoEnabled; }
            set { SetProperty(ref _departureInfoEnabled , value); }
        }

        public string DepartureInfo { get; set; }
        public string ReturnInfo { get; set; }

        public  class CheckButtonVisibility:CommonNotifyPropertyChanged
        {
            private static bool _ticketStatus, _tripStatus, _sliderStatus, _fromStatus, _toStatus;

            public static void SetOverallStatus()
            {

                InitializeBooking.checkButtonVisibility.OverallStatus = TicketStatus && TripStatus && SliderStatus && FromStatus && ToStatus;
            }

            public static bool ToStatus
            {
                get { return CheckButtonVisibility._toStatus; }
                set { CheckButtonVisibility._toStatus = value; }
            }

            public static bool FromStatus
            {
                get { return CheckButtonVisibility._fromStatus; }
                set { CheckButtonVisibility._fromStatus = value; }
            }
            private bool _overallStatus;
            

            

            public bool OverallStatus
            {
                get { return this._overallStatus; }
                set 
                { 
                    SetProperty(ref this._overallStatus , value); 
                    
                }
            }

            public static bool SliderStatus
            {
                get { return CheckButtonVisibility._sliderStatus; }
                set
                {
                    
                    CheckButtonVisibility._sliderStatus = value;
                   
                }
            }

            public static bool TripStatus
            {
                get { return CheckButtonVisibility._tripStatus; }
                set 
                {
                    CheckButtonVisibility._tripStatus = value;
                    

                }
            }

            public static bool TicketStatus
            {
                get { return CheckButtonVisibility._ticketStatus; }
                set
                {
                    CheckButtonVisibility._ticketStatus = value;
                    
                }
            }


        }
        

    }
}
