﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using BimanAppW8.DataModel;

namespace BimanAppW8.Data
{
    public class ComboDataSource:CommonNotifyPropertyChanged
    {
        public ObservableCollection<string> AdultsComboData { get; set; }
        public ObservableCollection<string> ChildrenComboData { get; set; }
        public ObservableCollection<string> InfantsComboData { get; set; }
        public ObservableCollection<string> CabinComboData { get; set; }
        public ObservableCollection<string> PromoComboData { get; set; }
        

        private int _adultsSelectedIndex,_childrenSelectedIndex,_infantsSelectedIndex,_cabinSelectedIndex,_promoSelectedIndex;

        public int PromoSelectedIndex
        {
            get { return _promoSelectedIndex; }
            set { SetProperty(ref _promoSelectedIndex, value); }
        }

        public int CabinSelectedIndex
        {
            get { return _cabinSelectedIndex; }
            set { SetProperty(ref _cabinSelectedIndex, value); }
        }

        public int InfantsSelectedIndex
        {
            get { return _infantsSelectedIndex; }
            set { SetProperty(ref _infantsSelectedIndex, value); }
        }

        public int ChildrenSelectedIndex
        {
            get { return _childrenSelectedIndex; }
            set { SetProperty(ref _childrenSelectedIndex, value); }
        }

        public int AdultsSelectedIndex
        {
            get { return _adultsSelectedIndex; }
            set { SetProperty(ref _adultsSelectedIndex, value); }
        }




    }
}
