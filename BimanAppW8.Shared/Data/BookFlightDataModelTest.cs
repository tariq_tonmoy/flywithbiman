﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BimanAppW8.DataModel;
using System.Collections.ObjectModel;


namespace BimanAppW8.Data
{
    public class BookFlightDataModelTest:CommonNotifyPropertyChanged
    {

        public string BookingID { get; set; }
        
        private int _bookingLimit=6;
        public int BookingLimit { get { return _bookingLimit; } set { this._bookingLimit = value; } }
        public class City
        {
            public string CityID { get; set; }
            public string Name { get; set; }
            public string ImagePath { get; set; }
        }

        public class FlightDate
        {
            public string Day { get; set; }
            public string Month { get; set; }
            public string Year { get; set; }
        }

        public FlightDate DepartDate { get; set; }
        public FlightDate ReturnDate { get; set; }
        public ObservableCollection<City> CityList { get; set; }

        
        private string _cityIDFrom, _cityIDTo;
        public string CityIDFrom 
        {
            get { return this._cityIDFrom; }
            set { SetProperty(ref _cityIDFrom, value); }
        }
        public string CityIDTo
        {
            get { return this._cityIDTo; }
            set { SetProperty(ref _cityIDTo, value); }
        }

        private int _noOfAdults, _noOfChildren, _noOfInfants;
        private string _cabinClass,_promoCode;
        private bool _fixedDate, _oneWayTicket, _flexibleDate, _roundTripTicket;

        public bool RoundTripTicket
        {
            get {  return _roundTripTicket; }
            set { SetProperty(ref _roundTripTicket, value); }
        }
        public bool OneWayTicket
        {
            get {  return _oneWayTicket; }
            set { SetProperty(ref _oneWayTicket, value);  }
        }
        public bool FlexibleDate
        {
            get {  return _flexibleDate; }
            set { SetProperty(ref _flexibleDate, value); }
        }

        public bool FixedDate {
            get {  return _fixedDate; }
            set { SetProperty(ref _fixedDate, value); } 
        }
      


       


        public int NoOfAdults { get { return _noOfAdults; } set { SetProperty(ref _noOfAdults,value); } }
        public int NoOfChildren { get { return _noOfChildren; } set { SetProperty(ref _noOfChildren, value); } }
        public int NoOfInfants { get { return _noOfInfants; } set { SetProperty(ref _noOfInfants, value); } }

        public string CabinClass { get { return _cabinClass; } set { SetProperty(ref _cabinClass, value); } }
        public string PromoCode { get { return _promoCode; } set { SetProperty(ref _promoCode, value); } }

      


    }
}
