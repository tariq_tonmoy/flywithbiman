﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using BimanPortableClassLibrary.DataModel;
namespace BimanAppW8.Data
{
    public class ImageUserControlData:INotifyPropertyChangedCommon
    {
        public string ImageID { get; set; }
        public string ImageName { get; set; }
        public string ImagePath1 { get; set; }
        public string ImagePath2 { get; set; }
        public string ImagePath3 { get; set; }

        private bool _IsError;

        Windows.UI.Xaml.Visibility _LoadingVisibility;

        public Windows.UI.Xaml.Visibility LoadingVisibility
        {
            get { return _LoadingVisibility; }
            set { SetProperty(ref _LoadingVisibility , value); }
        }
        public bool IsError
        {
            get { return _IsError; }
            set { SetProperty(ref _IsError , value); }
        }
        private bool _IsLoading;

        public bool IsLoading
        {
            get { return _IsLoading; }
            set 
            {
                if(value)
                    this.LoadingVisibility = Windows.UI.Xaml.Visibility.Visible;
                else this.LoadingVisibility = Windows.UI.Xaml.Visibility.Collapsed;
                SetProperty(ref _IsLoading , value);
            
            }
        }
        public ObservableCollection<ImageUserControlData> Images { get; set; }
    }
}
