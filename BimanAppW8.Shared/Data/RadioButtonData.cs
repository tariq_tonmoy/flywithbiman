﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BimanAppW8.DataModel;
using System.Collections.ObjectModel;
using Windows.UI.Xaml;
using BimanAppW8.Common;

namespace BimanAppW8.Data
{
    public class RadioButtonData:CommonNotifyPropertyChanged
    {
        private bool _fixedDate, _oneWayTicket, _flexibleDate, _roundTripTicket;

        public bool RoundTripTicket
        {
            get { this.SetTicketType(); return _roundTripTicket; }
            set { SetProperty(ref _roundTripTicket, value); this.SetTicketType(); }
        }
        public bool OneWayTicket
        {
            get { this.SetTicketType(); return _oneWayTicket; }
            set { SetProperty(ref _oneWayTicket, value); this.SetTicketType(); this.SetOneWayStatus(); }
        }
        public bool FlexibleDate
        {
            get { this.SetTripType(); return _flexibleDate; }
            set { SetProperty(ref _flexibleDate, value); this.SetTripType(); }
        }

        public bool FixedDate
        {
            get { this.SetTripType(); return _fixedDate; }
            set { SetProperty(ref _fixedDate, value); this.SetTripType(); }
        }

       

        public void SetOneWayStatus()
        {


            new IAdjustSliderGrid().AdjustSliderGrid(SliderUserControlData.DepartureData.YearData, SliderUserControlData.DepartureData.MonthData, SliderUserControlData.DepartureData.DayData, 1);
            if (_oneWayTicket)
            {
                InitializeBooking.sliderUserControlData.ReturnInfoEnabled = false;
                HubPage.returnInfoGridControl.ShowInfoGrid("");
                HubPage.returnInfoFlag = true;
                
            }
            else
            {
                HubPage.returnInfoGridControl.HideInfoGrid();
                HubPage.returnInfoFlag = true;
                new IAdjustSliderGrid().AdjustSliderGrid(SliderUserControlData.ReturnData.YearData, SliderUserControlData.ReturnData.MonthData, SliderUserControlData.ReturnData.DayData, 2);
            }
            

           
        }

        private void SetTicketType()
        {
            SliderUserControlData.CheckButtonVisibility.TripStatus = _oneWayTicket || _roundTripTicket;
            SliderUserControlData.CheckButtonVisibility.SetOverallStatus();
            
        }


        private void SetTripType()
        {
            SliderUserControlData.CheckButtonVisibility.TicketStatus = _fixedDate || _flexibleDate;
            SliderUserControlData.CheckButtonVisibility.SetOverallStatus();
        }

    }
}
