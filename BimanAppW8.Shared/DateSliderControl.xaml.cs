﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;



using Windows.UI.Xaml.Shapes;
using Windows.UI;
using BimanAppW8.Data;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace BimanAppW8
{
    public sealed partial class DateSliderControl : UserControl
    {

        private Dictionary<double, Line> LineList = new Dictionary<double, Line>();
        TextBlock textBlock = new TextBlock();
        private double size = 144.0;
        private double divisor = 0.5;
        private string[] monthList = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
        
        public static int DateFlag { get; set; }
        public delegate void ActionPointerEnter(object sender, PointerRoutedEventArgs e);

        public event ActionPointerEnter PointerEnterEvent;

        private SolidColorBrush PrimaryColor = new SolidColorBrush(Colors.LightPink), SecondaryColor = new SolidColorBrush(Colors.DeepPink);
        
        public DateSliderControl()
        {


            this.InitializeComponent();


            ContainerGrid.Height = size;
            ContainerGrid.Width = size;

            Ellipse ellipse = new Ellipse();
            SolidColorBrush scb = new SolidColorBrush() { Color = new Color() { R = 29, G = 29, B = 29, A = 255 } };
            ellipse.Fill = scb;



            ellipse.Width = size * divisor;
            ellipse.Height = size * divisor;

            textBlock.Name = "DateText";
            textBlock.HorizontalAlignment = HorizontalAlignment.Center;
            textBlock.VerticalAlignment = VerticalAlignment.Center;

            textBlock.FontSize = size * (1.0/3.0)*divisor;
            textBlock.Text = DateTime.Now.Day.ToString();

            textBlock.MaxHeight = size * divisor;
            textBlock.MaxWidth = size * divisor;



            for (double i = 0.0; i < 360.0; i = i + 1.0)
            {
                Line line = new Line();

                line.X1 = (size / 2);
                line.Y1 = (size / 2);

                line.X2 = (size / 2) + ((size / 2) * Math.Cos((Math.PI / 180.0) * i));
                line.Y2 = (size / 2) + ((size / 2) * Math.Sin((Math.PI / 180.0) * i));

                line.StrokeThickness = 2.0;


                line.Stroke = PrimaryColor;
                line.PointerEntered += line_PointerEntered;



                ContainerGrid.Children.Add(line);
                LineList.Add(i, line);



            }

            ContainerGrid.Children.Add(ellipse);
            ContainerGrid.Children.Add(textBlock);

            SliderUserControlData.CheckButtonVisibility.SliderStatus = true;

            InitializeCurrentDate();
        }

        
        int PrevLineNum;
        private void InitializeCurrentDate()
        {
            for (int i = 0; i < (DateTime.Now.Day * 11.61); i++)
            {
                LineList[i].Stroke = SecondaryColor;
               
                PrevLineNum = i;
            }
        }

        
        void line_PointerEntered(object sender, PointerRoutedEventArgs e)
        {
            
            if (PointerEnterEvent != null)
            {
                PointerEnterEvent(sender, e);
            }

            double CurrentLineNum=LineList.First(x=>x.Value==(Line)sender).Key;
            new IAdjustSliderGrid().SetSliderColor(LineList,PrevLineNum,(int)CurrentLineNum,PrimaryColor,SecondaryColor);
            PrevLineNum = (int)CurrentLineNum;

            string temp = textBlock.Text;
            int flr = (int)Math.Floor(CurrentLineNum / 11.61);

            textBlock.Text = (1 + flr).ToString();

            if (temp != textBlock.Text)
            {

                TestStoryboard.Begin();



                if (DateFlag == 1)
                {
                    SliderUserControlData.DepartureData.DayData = textBlock.Text;
                    new IAdjustSliderGrid().AdjustSliderGrid(SliderUserControlData.DepartureData.YearData, SliderUserControlData.DepartureData.MonthData, SliderUserControlData.DepartureData.DayData, DateFlag);
                }
                else if (DateFlag == 2)
                {
                    SliderUserControlData.ReturnData.DayData = textBlock.Text;
                    new IAdjustSliderGrid().AdjustSliderGrid(SliderUserControlData.ReturnData.YearData, SliderUserControlData.ReturnData.MonthData, SliderUserControlData.ReturnData.DayData, DateFlag);
                }
            }



            //bool flag = true;
            //for (double i = 0.0; i < 360.0; i = i + 1.0)
            //{

            //    try
            //    {
            //        Line line = LineList[i];
            //        if (flag == true && line != (Line)sender)
            //            line.Stroke = SecondaryColor;
            //        else if (line == (Line)sender)
            //        {
            //            flag = false;
            //            line.Stroke = SecondaryColor;



            //            string temp = textBlock.Text;
            //            int flr = (int)Math.Floor(i / 11.61);

            //            textBlock.Text = (1 + flr).ToString();
                        
            //            if (temp != textBlock.Text)
            //            {

            //                TestStoryboard.Begin();


                            
            //                if (DateFlag == 1)
            //                {
            //                    SliderUserControlData.DepartureData.DayData = textBlock.Text;
            //                    new IAdjustSliderGrid().AdjustSliderGrid(SliderUserControlData.DepartureData.YearData, SliderUserControlData.DepartureData.MonthData, SliderUserControlData.DepartureData.DayData,DateFlag);
            //                }
            //                else if (DateFlag == 2)
            //                {
            //                    SliderUserControlData.ReturnData.DayData = textBlock.Text;
            //                    new IAdjustSliderGrid().AdjustSliderGrid(SliderUserControlData.ReturnData.YearData, SliderUserControlData.ReturnData.MonthData, SliderUserControlData.ReturnData.DayData, DateFlag);
            //                }
            //            }
                       
            //        }
            //        else if (flag == false)
            //            line.Stroke = PrimaryColor;




                //}
                //catch (Exception)
                //{

                //}
            //}
        }
    }
}
