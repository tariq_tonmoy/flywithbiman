﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System.Diagnostics;
using Windows.UI.StartScreen;
using BimanPortableClassLibrary.DataModel;
using BimanPortableClassLibrary.Stories;
using Windows.UI.Popups;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace BimanAppW8
{
    public sealed partial class FindAppBarControl : UserControl
    {
        public FindAppBarControl()
        {
            this.InitializeComponent();
            
            
            
        }

          private void FindButton_Click(object sender, RoutedEventArgs e)
        {
            SearchGrid.Visibility = Visibility.Visible;
            TopGrid.Visibility = Visibility.Collapsed;

            AppBarOpened(NameTextBox);
            AppBarOpened(BookingTextBox);
        
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
          

            AppBarOpened(NameTextBox);
            AppBarOpened(BookingTextBox);
            ResetAppBar();

        }

        


        private void NameTextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            if(!IsNameTextFull)
                NameTextBox.Text = "";
            NameTextBox.Foreground = new SolidColorBrush(Windows.UI.Colors.Black);
            IsNameInitialized = IsNameTextFull = true;
            ErrorTextBlock.Text = "";
        }

        private void BookingTextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            if(!IsBookingIdTextFull)
                BookingTextBox.Text = "";
            BookingTextBox.Foreground = new SolidColorBrush(Windows.UI.Colors.Black);
            IsBookingInitialized = IsBookingIdTextFull = true;
            ErrorTextBlock.Text = "";
            
        }

        private void NameTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (!IsNameTextFull) AppBarOpened(NameTextBox);
        }

        private void BookingTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            if (!IsBookingIdTextFull) AppBarOpened(BookingTextBox);
        }


        private void NameTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (IsNameInitialized)
            {
                if (((TextBox)sender).Text == "") IsNameTextFull = false;
                else IsNameTextFull = true;
            }
        }

        private void BookingTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (IsBookingInitialized)
            {
                if (((TextBox)sender).Text == "") IsBookingIdTextFull = false;
                else IsBookingIdTextFull = true;
            }
        }



        private bool IsNameTextFull=false,IsBookingIdTextFull=false,IsNameInitialized=false,IsBookingInitialized=false;
       

        private void AppBarOpened(TextBox sender)
        {
            sender.Text = sender.Name == "NameTextBox" ? "First/Last Name" : "Booking ID";
            sender.Foreground = new SolidColorBrush(Windows.UI.Colors.Gray);
            IsNameInitialized = IsNameTextFull = sender.Name == "NameTextBox" ? false : IsNameTextFull;
            IsBookingInitialized = IsBookingIdTextFull = sender.Name == "NameTextBox" ? IsBookingIdTextFull : false;

        }

       
        public void ResetAppBar()
        {
            SearchGrid.Visibility = Visibility.Collapsed;
            TopGrid.Visibility = Visibility.Visible;
            ErrorTextBlock.Text = "";
            Shadegrid.Visibility = Visibility.Collapsed;
        }

        public void SetAppBar(int i)
        {
            Shadegrid.Visibility = Visibility.Collapsed;
            if (i==0)
            {
                SaveButton.Visibility = Visibility.Collapsed;
                DeleteButton.Visibility = Visibility.Collapsed;
                PinButton.Visibility = Visibility.Collapsed;
            }

            else if(i==1 && !SecondaryTile.Exists(StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoDataModel().BookingID)) 
            {
                DeleteButton.Visibility = Visibility.Collapsed;
                SaveButton.Visibility = Visibility.Visible;
               
            }
            else if(!SecondaryTile.Exists(StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoDataModel().BookingID))
            {
                SaveButton.Visibility = Visibility.Collapsed;
                DeleteButton.Visibility = Visibility.Visible;
                
            }
            else if (SecondaryTile.Exists(StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoDataModel().BookingID))
            {
                DeleteButton.Visibility = Visibility.Collapsed;
                PinButton.Visibility = Visibility.Collapsed;
            }

            if (i > 1)
            {
                PinButton.Visibility = Visibility.Visible;
                ToggleAppBarButton(!SecondaryTile.Exists(StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoDataModel().BookingID)); ;
                
                
            }
            
        }

        public void ToggleAppBarButton(bool ShowPinButton)
        {

            this.PinButton.Icon = ShowPinButton == true ? new SymbolIcon(Symbol.Pin) : new SymbolIcon(Symbol.UnPin);
            this.PinButton.Label = ShowPinButton == true ? "Pin" : "Unpin";
            this.PinButton.UpdateLayout();

            if(!ShowPinButton)
            {
                DeleteButton.Visibility = Visibility.Collapsed;
                SaveButton.Visibility = Visibility.Collapsed;
            }
            else
            {
                DeleteButton.Visibility = Visibility.Visible;
                SaveButton.Visibility = Visibility.Collapsed;
            }
        }


        public delegate void FindButtonTappedDelegate(object sender, TappedRoutedEventArgs e);
        public delegate void SaveButtonTappedDelegate(object sender, TappedRoutedEventArgs e);
        public delegate void DeleteButtonTappedDelegate(object sender, TappedRoutedEventArgs e);
        public delegate void PinButtonTappeddDelegate(object sender, TappedRoutedEventArgs e);


        public event FindButtonTappedDelegate FindButtonTapped;
        public event SaveButtonTappedDelegate SaveButtonTapped;
        public event DeleteButtonTappedDelegate DeleteButtonTapped;
        public event PinButtonTappeddDelegate PinButtonTapped;

        public string NameText, BookingText;
        private void FindBookingButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
           
            
            if (FindButtonTapped != null && IsBookingIdTextFull && IsNameTextFull)
            {
                Shadegrid.Visibility = Visibility.Visible;
                NameText = NameTextBox.Text;
                BookingText = BookingTextBox.Text;
                FindButtonTapped(sender, e);
            }
            else
            {
                if (!IsBookingIdTextFull || !IsNameTextFull) ErrorTextBlock.Text = "Please Enter Passenger Name and Booking ID";
            }
        }

        private void DeleteButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            PinButton.Visibility = Visibility.Collapsed;
            if (DeleteButtonTapped != null)
                DeleteButtonTapped(sender, e);
        }

        private void SaveButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            PinButton.Visibility = Visibility.Visible;
            if (SaveButtonTapped != null)
                SaveButtonTapped(sender, e);
        }

        private  void PinButton_Tapped(object sender, TappedRoutedEventArgs e)
        {
            if (PinButtonTapped != null)
            {
                PinButtonTapped(sender, e);
            }

            
        }

    

       

       

    }
}
