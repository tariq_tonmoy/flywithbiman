﻿using BimanAppW8.Common;
using BimanAppW8.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System.Diagnostics;
using BimanPortableClassLibrary.DataModel;
using BimanPortableClassLibrary.Stories;
using BimanAppW8.DataModel;
using System.Net;
using Windows.UI.Xaml.Media.Animation;
using System.Net.Http;
using System.Threading.Tasks;
using BimanAppW8.DataModel.HtmlScrapingImplementation;
using System.Collections.ObjectModel;
using System.Threading;
using HtmlAgilityPack;
using Windows.Storage;
using Windows.Storage.Streams;

namespace BimanAppW8.Common
{
    public class InitializeBooking
    {
        public static RadioButtonData radioData = new RadioButtonData();
        public ComboDataSource comboDataSource = new ComboDataSource();
        public static SliderUserControlData sliderUserControlData = new SliderUserControlData();
        public static SliderUserControlData.CheckButtonVisibility checkButtonVisibility = new SliderUserControlData.CheckButtonVisibility();
        public static ImageUserControlData ImageData = new ImageUserControlData();
        private static bool _IsBookingInitialized = false;
        private DispatcherTimer timer;
        public static bool IsBookingInitialized
        {
            get { return InitializeBooking._IsBookingInitialized; }
            set { InitializeBooking._IsBookingInitialized = value; }
        }


        private bool BookingError = false;
        private ObservableDictionary _DefaultViewModel = new ObservableDictionary();

        public ObservableDictionary DefaultViewModel
        {
            get { return _DefaultViewModel; }
            set { _DefaultViewModel = value; }
        }
         private delegate Task<BookFlightDataModel> BookingDelegate();


        public InitializeBooking(ObservableDictionary PassedViewModel)
        {
            DefaultViewModel = PassedViewModel;
            InitializeFlightBooking();
        }
        
        
        
        private async void InitializeFlightBooking()
        {
            this.InitializeDestinationList();
            this.InitializeRadioButton();
            this.InitializeComboBox();
            this.InitializeSliders();
            this.InitializeCheckButton();

            try
            {
                if (!IsBookingInitialized)
                {
                    BookingDelegate dlgt = new BookingDelegate(StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataFromBimanAsync);
                    IAsyncResult ar = dlgt.BeginInvoke(null, null);
                    ar.AsyncWaitHandle.WaitOne();
                    var v = dlgt.EndInvoke(ar);



                    BookFlightDataModel b = await v;
                    if (b == null) throw new Exception();
                    StoryCollection.StoryCollectionInstance.BookFlight.SetBookFlightDataModel(b);
                }

                this.InputDataToFlightBooking();


            }
            catch (Exception)
            {
                timer = new DispatcherTimer();
                timer.Tick += BookingTimer_Tick;
                timer.Interval = new TimeSpan(0, 0, 5);
                BookingError = true;
                timer.Start();
                return;
            }

        }

        public void StopTimer()
        {
            if(timer!=null && timer.IsEnabled)
                timer.Stop();
            
        }

        public void StartTimer()
        {
            if (timer != null && !timer.IsEnabled && !IsBookingInitialized)
                timer.Start();
           
        }

        private async void CheckForData(DispatcherTimer timer)
        {
            try
            {
                if (BookingError)
                {
                    BookingError = false;

                    IFlightBookingImplementation implementation = new IFlightBookingImplementation();
                    BookingDelegate dlgt = new BookingDelegate(implementation.GetBookFlightDataFromBimanAsync);
                    IAsyncResult ar = dlgt.BeginInvoke(null, null);
                    ar.AsyncWaitHandle.WaitOne();
                    var v = dlgt.EndInvoke(ar);


                    BookFlightDataModel b = await v;
                    if (b == null) throw new Exception();
                    timer.Stop();
                    StoryCollection.StoryCollectionInstance.BookFlight.SetBookFlightDataModel(b);
                    this.InputDataToFlightBooking();

                }
            }
            catch (Exception)
            {
                BookingError = true;
            }
        }
        private  void BookingTimer_Tick(object sender, object e)
        {
            
            CheckForData((DispatcherTimer)sender);

        }

        private void InputDataToFlightBooking()
        {
            for (int i = 0; i < StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().BookingLimit; i++)
            {
                comboDataSource.AdultsComboData.Add((i + 1).ToString());

                comboDataSource.InfantsComboData.Add(i.ToString());
            }


            for (int i = 0; i < StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().CabinClassCollection.Count; i++)
            {
                comboDataSource.CabinComboData.Add(StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().CabinClassCollection[i]);
            }
            for (int i = 0; i < StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().PromoCodeCollection.Count; i++)
            {
                comboDataSource.PromoComboData.Add(StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().PromoCodeCollection[i]);
            }

            if (comboDataSource.AdultsComboData.Count>1)
                comboDataSource.AdultsSelectedIndex = 1;
            if(comboDataSource.CabinComboData.Count>1)
                comboDataSource.CabinSelectedIndex = 1;
            if(comboDataSource.PromoComboData.Count>1)
                comboDataSource.PromoSelectedIndex = 1;

            comboDataSource.AdultsSelectedIndex = 0;
            comboDataSource.CabinSelectedIndex = 0;
            comboDataSource.PromoSelectedIndex = 0;
            DispatcherTimer timer = new DispatcherTimer();
            timer.Tick += timer_Tick;




            timer.Interval = new TimeSpan(0, 0, 0, 0, 100);
            timer.Start();

            SliderUserControlData.CheckButtonVisibility.FromStatus = false;
            SliderUserControlData.CheckButtonVisibility.ToStatus = false;
            SliderUserControlData.CheckButtonVisibility.SetOverallStatus();
            StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().CityIDFrom = "";
            StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().CityIDTo = "";
            radioData.FixedDate = false;
            radioData.FlexibleDate = false;
            radioData.OneWayTicket = false;
            radioData.RoundTripTicket = false;





        }
        int imageCounter = 0;
        void timer_Tick(object sender, object e)
        {


            PopulateImages(imageCounter, (DispatcherTimer)sender);
            imageCounter++;
        }

        private void PopulateImages(int i, DispatcherTimer timer)
        {

            if (StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().CityList.Count > i)
            {
                var v = StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel().CityList[i];
                ImageUserControlData tempData = new ImageUserControlData()
                {
                    ImageID = v.CityID,
                    ImageName = v.Name,
                    ImagePath1 = "Assets/City/" + v.CityID + "1.jpg",
                    ImagePath2 = "Assets/City/" + v.CityID + "2.jpg",
                    ImagePath3 = "Assets/City/" + v.CityID + "3.jpg"
                };
                ImageData.Images.Add(tempData);

            }
            else
            {
                ((DispatcherTimer)timer).Stop();
                ImageData.IsLoading = false;
                IsBookingInitialized = true;


            }

        }




        private void InitializeDestinationList()
        {



            //Images Data
            ImageData.Images = new ObservableCollection<ImageUserControlData>();
            ImageData.IsError = false;
            ImageData.IsLoading = true;
            this.DefaultViewModel["ImageList"] = ImageData;

            //Set & get City Name + ID beside headers over images
            this.DefaultViewModel["DestinationName"] = StoryCollection.StoryCollectionInstance.BookFlight.GetBookFlightDataModel();

        }



        private void InitializeRadioButton()
        {
            //Radio Buttons Data
            this.DefaultViewModel["BookingRadioData"] = radioData;
        }

        private void InitializeComboBox()
        {
            comboDataSource.AdultsComboData = new System.Collections.ObjectModel.ObservableCollection<string>();
            comboDataSource.ChildrenComboData = new System.Collections.ObjectModel.ObservableCollection<string>();
            comboDataSource.InfantsComboData = new System.Collections.ObjectModel.ObservableCollection<string>();
            comboDataSource.PromoComboData = new System.Collections.ObjectModel.ObservableCollection<string>();
            comboDataSource.CabinComboData = new System.Collections.ObjectModel.ObservableCollection<string>();




            //ComboBox Data Source
            this.DefaultViewModel["ComboDataSource"] = comboDataSource;
        }


        private void InitializeSliders()
        {
            sliderUserControlData.DepartureInfoEnabled = false;



            SliderUserControlData.DepartureData.DayData = DateTime.Now.Day.ToString();
            SliderUserControlData.DepartureData.MonthData = CommonVariables.MonthDictionary[DateTime.Now.Month.ToString()];
            SliderUserControlData.DepartureData.YearData = DateTime.Now.Year.ToString();

            SliderUserControlData.ReturnData.DayData = DateTime.Now.Day.ToString(); ;
            SliderUserControlData.ReturnData.MonthData = CommonVariables.MonthDictionary[DateTime.Now.Month.ToString()];
            SliderUserControlData.ReturnData.YearData = DateTime.Now.Year.ToString();

            //Calender Slider Setup
            this.DefaultViewModel["SliderUserControlData"] = sliderUserControlData;

        }


        private void InitializeCheckButton()
        {
            //Check Button Setup
            this.DefaultViewModel["CheckButtonVisibility"] = checkButtonVisibility;
        }

    }
}
