﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BimanAppW8.Common
{

    public class CommonVariables
    {

        private static Dictionary<string, string> monthDictionary = new Dictionary<string, string>(){
            {"1", "Jan"},
            {"01", "Jan"},
            {"2", "Feb"},
            {"02", "Feb"},
            {"3", "Mar"},
            {"03", "Mar"},
            {"4", "Apr"},
            {"04", "Apr"},
            {"5", "May"},
            {"05", "May"},
            {"6", "Jun"},
            {"06", "Jun"},
            {"7", "Jul"},
            {"07", "Jul"},
            {"8", "Aug"},
            {"08", "Aug"},
            {"9", "Sep"},
            {"09", "Sep"},
            {"10", "Oct"},
            {"11", "Nov"},
            {"12", "Dec"}
        };

        public static Dictionary<string, string> MonthDictionary
        {
            get { return CommonVariables.monthDictionary; }
            set { CommonVariables.monthDictionary = value; }
        }


    }
}
