﻿using System;
using BimanPortableClassLibrary.DataModel;
using BimanPortableClassLibrary.Stories;
using System.Net;
using System.IO;
using System.Text;
using HtmlAgilityPack;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using System.Threading;
using System.Xml;
using System.Xml.Linq;
using System.Net.Http;
using System.Globalization;
using BimanAppW8.Common;
using Windows.ApplicationModel.Resources;


namespace BimanAppW8.DataModel.HtmlScrapingImplementation
{
    public class IFlightBookingImplementation : IFlightBookingHandler
    {
        #region CommonVariables
        private static CookieContainer cookies = null;
        #endregion CommonVariables


        #region BookFlightInitiation
        public override BookFlightDataModel GetBookFlightDataModel()
        {

            if (_BookFlightDataModel == null)
            {
                return _BookFlightDataModel = new BookFlightDataModel();
            }
            return _BookFlightDataModel;
        }




        public async override Task<BookFlightDataModel> GetBookFlightDataFromBimanAsync()
        {
            if (_BookFlightDataModel == null)
                _BookFlightDataModel = new BookFlightDataModel();
            _BookFlightDataModel.CityList = new ObservableCollection<BookFlightDataModel.City>();



            string url = new ResourceLoader().GetString("BimanHome");
            cookies = new CookieContainer();





            var request = (HttpWebRequest)HttpWebRequest.Create(url);

            request.CookieContainer = cookies;
            request.Method = new ResourceLoader().GetString("GetMethod");
            request.ContentType = new ResourceLoader().GetString("HeaderContentType"); ;


            var WebGet = new HtmlDocument();


            using (var response = await request.GetResponseAsync())
            {

                using (Stream stream = response.GetResponseStream())
                {
                    using (var reader = new StreamReader(stream))
                    {

                        WebGet.Load(reader.BaseStream);
                    }


                }
            }




            var nodes = from combo in WebGet.DocumentNode.Descendants()
                        where combo.Name == "select" && combo.Attributes["name"].Value == "DC"
                        select combo;

            foreach (var combo in nodes)
            {

                foreach (var v in combo.ChildNodes)
                {
                    if (v.InnerText != "" && v.InnerText.Split('(').Count() > 1)
                    {
                        BookFlightDataModel.City city = new BookFlightDataModel.City();
                        //Greedy Search Regex*** Because there can be cities with names with multiple words. 
                        //The regex is applied to ommit the last space after the name.
                        city.Name = WebUtility.HtmlDecode(new Regex(@"(\S).+(\S)").Match(v.InnerText.Split('(')[0]).Value);
                        city.CityID = WebUtility.HtmlDecode(new Regex(@"(\S).+(\w|\d)").Match(v.InnerText.Split('(')[1]).Value);


                        _BookFlightDataModel.CityList.Add(city);

                    }

                }
            }
            var nodes2 = (from combo in WebGet.DocumentNode.Descendants()
                          where combo.Name == "select" && combo.Attributes["name"] != null && combo.Attributes["name"].Value == "PA"
                          select combo.ChildNodes).FirstOrDefault();
            if (nodes2 != null)
            {
                int count = 0;
                foreach (var v in nodes2)
                {
                    if (new Regex(@"(\S).*(\d)").Match(v.InnerText).Value != "")
                    {
                        count++;
                    }

                }
                _BookFlightDataModel.BookingLimit = count;
            }


            var nodes3 = (from combo in WebGet.DocumentNode.Descendants()
                          where combo.Name == "select" && combo.Attributes["name"] != null && combo.Attributes["name"].Value == "CC"
                          select combo.ChildNodes).FirstOrDefault();
            if (nodes3 != null)
            {
                _BookFlightDataModel.CabinClassCollection = new ObservableCollection<string>();
                _BookFlightDataModel.CabinClassValueCollection = new ObservableCollection<string>();
                foreach (var v in nodes3)
                {
                    if (new Regex(@"(\S).+").Match(v.InnerText).Value != "")
                    {
                        _BookFlightDataModel.CabinClassCollection.Add(v.InnerText);

                    }

                    if (v.Attributes["value"] != null)
                    {
                        _BookFlightDataModel.CabinClassValueCollection.Add(v.Attributes["value"].Value);

                    }

                }

            }



            var nodes4 = (from combo in WebGet.DocumentNode.Descendants()
                          where combo.Name == "div" && combo.Attributes["id"] != null && combo.Attributes["id"].Value == "flight_deals"
                          select combo);

            if (nodes4 != null)
            {
                _BookFlightDataModel.PromoCodeCollection = new ObservableCollection<string>() { "None" };
                foreach (var v in nodes4)
                {

                    if (v.Attributes["id"] != null && v.Attributes["id"].Value == "flight_deals")
                    {
                        foreach (var node in v.ChildNodes)
                        {
                            GetTheDeals(node, _BookFlightDataModel);

                        }

                    }

                }
            }



            return _BookFlightDataModel;
        }


        private void GetTheDeals(HtmlNode node, BookFlightDataModel I_BookFlightDataModel)
        {
            if (node.HasChildNodes)
            {
                foreach (var v in node.ChildNodes)
                {
                    GetTheDeals(v, I_BookFlightDataModel);
                }
            }
            else
            {
                if (new Regex(@"(promo code).+(\S)", RegexOptions.IgnoreCase).Match(node.InnerText).Value != "")
                    I_BookFlightDataModel.PromoCodeCollection.Add(new Regex(@"[A-Za-z0-9].+[A-Za-z0-9]").Match(new Regex(@"(PROMO CODE).+(\S)", RegexOptions.IgnoreCase).Match(node.InnerText).Value.Split(':')[1]).Value);
            }
        }

        public override void SetBookFlightDataModel(BookFlightDataModel LocalBookFlightDataModel)
        {
            _BookFlightDataModel = LocalBookFlightDataModel;
        }

        #endregion BookFlightInitiation


        #region FlightSelection


        public override FlightSelectionDataModel GetFlightSelectionDataModel()
        {
            return _FlightSelectionDataModel;

        }


        #region GetDataFromBiman

        private FlightSelectionDataModel.FlightDetails flightDetails = null;

        private int FlightIDFlag = -1;
        private bool flag = false;
        private bool ErrorFlag = false;



        public async override Task<FlightSelectionDataModel> GetFlightSelectionFromBimanAsync()
        {
            if (_FlightSelectionDataModel == null)
                _FlightSelectionDataModel = new FlightSelectionDataModel();

            Dictionary<string, string> FlightSelectionDictionary = new Dictionary<string, string>();
            string DepartureDay, ReturnDay;

            if (_BookFlightDataModel.DepartDate.Day.Count() == 1) DepartureDay = "0" + _BookFlightDataModel.DepartDate.Day;
            else DepartureDay = _BookFlightDataModel.DepartDate.Day;

            if (_BookFlightDataModel.ReturnDate.Day.Count() == 1) ReturnDay = "0" + _BookFlightDataModel.ReturnDate.Day;
            else ReturnDay = _BookFlightDataModel.ReturnDate.Day;



            if (_BookFlightDataModel.RoundTripTicket)
                FlightSelectionDictionary["TT"] = "RT";
            else FlightSelectionDictionary["TT"] = "OW";

            FlightSelectionDictionary["DC"] = _BookFlightDataModel.CityIDFrom.Split('(')[1].Split(')')[0];
            FlightSelectionDictionary["AC"] = _BookFlightDataModel.CityIDTo.Split('(')[1].Split(')')[0];

            FlightSelectionDictionary["AM"] = _BookFlightDataModel.DepartDate.Year + "-" + CommonVariables.MonthDictionary.FirstOrDefault(x => x.Value == _BookFlightDataModel.DepartDate.Month && x.Key.Length >= 2).Key;
            FlightSelectionDictionary["AD"] = DepartureDay;
            FlightSelectionDictionary["RM"] = _BookFlightDataModel.ReturnDate.Year + "-" + CommonVariables.MonthDictionary.FirstOrDefault(x => x.Value == _BookFlightDataModel.ReturnDate.Month && x.Key.Length >= 2).Key;
            FlightSelectionDictionary["RD"] = ReturnDay;

            if (_BookFlightDataModel.FixedDate)
                FlightSelectionDictionary["FL"] = "";
            else FlightSelectionDictionary["FL"] = "on";

            FlightSelectionDictionary["PA"] = _BookFlightDataModel.NoOfAdults.ToString();
            FlightSelectionDictionary["PC"] = _BookFlightDataModel.NoOfChildren.ToString();
            FlightSelectionDictionary["PI"] = _BookFlightDataModel.NoOfInfants.ToString();



            FlightSelectionDictionary["CC"] = _BookFlightDataModel.CabinClassValueCollection[_BookFlightDataModel.CabinClassCollection.IndexOf(_BookFlightDataModel.CabinClass)];


            if (_BookFlightDataModel.PromoCode == "None")
                FlightSelectionDictionary["CD"] = "";
            else
                FlightSelectionDictionary["CD"] = _BookFlightDataModel.PromoCode;





            string url = new ResourceLoader().GetString("BimanFlightSelection");
            string urlCaptcha = new ResourceLoader().GetString("BimanCaptcha");
            cookies = new CookieContainer();

            var requestCaptcha = (HttpWebRequest)WebRequest.Create(urlCaptcha);

            requestCaptcha.CookieContainer = cookies;
            requestCaptcha.Method = new ResourceLoader().GetString("HeadMethod");
            requestCaptcha.ContentType = new ResourceLoader().GetString("HeaderContentType");
            string hash = "", header = "";



            for (int i = 0; i < FlightSelectionDictionary.Count; i++)
            {
                if (i == 0)
                {
                    url += "?";
                }
                else
                {
                    url += "&";
                    header += "&";
                }

                url += Uri.EscapeDataString(FlightSelectionDictionary.ElementAtOrDefault(i).Key) + "=" + Uri.EscapeDataString(FlightSelectionDictionary.ElementAtOrDefault(i).Value);
                header += Uri.EscapeDataString(FlightSelectionDictionary.ElementAtOrDefault(i).Key) + "=" + Uri.EscapeDataString(FlightSelectionDictionary.ElementAtOrDefault(i).Value); ;
            }

            requestCaptcha.Headers["X-Hash-Validate"] = header;

            using (var response = await requestCaptcha.GetResponseAsync())
            {
                hash = response.Headers["X-Hash"];
            }

            url += "&x=0&y=0&FS=" + hash;

            var request = (HttpWebRequest)HttpWebRequest.Create(url);
            request.Method = new ResourceLoader().GetString("GetMethod");
            request.ContentType = new ResourceLoader().GetString("HeaderContentType");
            request.Accept = new ResourceLoader().GetString("HeaderAccept");
            request.CookieContainer = cookies;


            var WebGet = new HtmlDocument();


            WebGet.Load(new StreamReader((await request.GetResponseAsync()).GetResponseStream()).BaseStream);



            try
            {
                var nodes = from div in WebGet.DocumentNode.Descendants()
                            where div.Name == "div" && div.Attributes["class"] != null && (div.Attributes["class"].Value == "errors" || div.Attributes["class"].Value == "warnings")
                            select div;

                ErrorFlag = HasErrors(nodes);

                if (ErrorFlag)
                {

                    _FlightSelectionDataModel.OneWayFlag = _FlightSelectionDataModel.RoundTripFlag = false;
                    return _FlightSelectionDataModel;
                }

            }
            catch (Exception)
            {

            }




            if (_FlightSelectionDataModel.FlightCollection == null)
                _FlightSelectionDataModel.FlightCollection = new ObservableCollection<FlightSelectionDataModel.FlightDetails>();



            var table_trip_1 = from div in WebGet.DocumentNode.Descendants()
                               where div.Name == "div" && div.Attributes["id"] != null && div.Attributes["id"].Value == "trip_1"
                               from table in div.ChildNodes
                               where table.Name == "table"
                               select table;



            if (table_trip_1 != null)
                GetFlightData(table_trip_1);


            var table_trip_2 = from div in WebGet.DocumentNode.Descendants()
                               where div.Name == "div" && div.Attributes["id"] != null && div.Attributes["id"].Value == "trip_2"
                               from table in div.ChildNodes
                               where table.Name == "table"
                               select table;
            if (table_trip_2 != null)
                GetFlightData(table_trip_2);
            GetBaggageAllowanceDetails(table_trip_1);

            //GET FSC for the flight selection page. That's a hidden field in HTML wabpage of Biman.
            try
            {
                var FscNode = (from input in WebGet.DocumentNode.Descendants()
                               where input.Name == "input" && input.Attributes["name"] != null && input.Attributes["name"].Value == "fsc"
                               select input).FirstOrDefault();

                var SspNode = (from input in WebGet.DocumentNode.Descendants()
                               where input.Name == "input" && input.Attributes["name"] != null && input.Attributes["name"].Value == "ssp"
                               select input).FirstOrDefault();

                _FlightSelectionDataModel.FSC = FscNode.Attributes["value"].Value;
                _FlightSelectionDataModel.SSP = SspNode.Attributes["value"].Value;


            }
            catch (Exception)
            {
            }


            if (_FlightSelectionDataModel.FlightCollection.FirstOrDefault(x => x.TripID == "trip_1") == null)
                _FlightSelectionDataModel.OneWayFlag = false;
            else _FlightSelectionDataModel.OneWayFlag = true;
            if (_FlightSelectionDataModel.FlightCollection.FirstOrDefault(x => x.TripID == "trip_2") == null)
                _FlightSelectionDataModel.RoundTripFlag = false;
            else _FlightSelectionDataModel.RoundTripFlag = true;


            return _FlightSelectionDataModel;

        }




        private bool HasErrors(IEnumerable<HtmlNode> nodes)
        {
            if (nodes.Count() == 0) return false;

            _FlightSelectionDataModel.ErrorCollection = new ObservableCollection<string>();


            foreach (var v in nodes)
            {
                GetErrorStrings(v);
            }

            return true;
        }


        private void GetErrorStrings(HtmlNode node)
        {
            if (!node.HasChildNodes)
            {
                if (node.ParentNode.Name == "h2" && new Regex(@"\S.*").Match(node.InnerText).Value != "")
                {
                    _FlightSelectionDataModel.ErrorHeader = new Regex(@"\S.*").Match(node.InnerText).Value;

                }
                else if (new Regex(@"\S.*").Match(node.InnerText).Value != "")
                {
                    _FlightSelectionDataModel.ErrorCollection.Add(new Regex(@"\S.*").Match(node.InnerText).Value);

                }
                return;
            }

            //recursive call
            foreach (var v in node.ChildNodes)
            {
                GetErrorStrings(v);
            }
        }


        private void GetBaggageAllowanceDetails(IEnumerable<HtmlNode> tableNodes)
        {
            _FlightSelectionDataModel.BaggageAllowanceCollection = new ObservableCollection<FlightSelectionDataModel.BaggageAllowance>();
            foreach (HtmlNode tableNode in tableNodes)
            {
                try
                {
                    HtmlNode thNodes = tableNode.ChildNodes.FirstOrDefault(x => x.Name == "thead").ChildNodes.FirstOrDefault(x => x.Name == "tr");

                    foreach (var th in thNodes.ChildNodes)
                    {


                        if (th.Attributes["class"] != null && th.Attributes["class"].Value.Split(' ')[0] == "family")
                        {


                            FlightSelectionDataModel.BaggageAllowance baggageAllowance = new FlightSelectionDataModel.BaggageAllowance();

                            if (th.Attributes["class"].Value.Split(' ')[1] == "family-SS")
                            {
                                if (th.ChildNodes.FirstOrDefault(x => x.Name == "span").Attributes["title"] != null)
                                {
                                    baggageAllowance.CabinFamilyName = "Super Saver";


                                }


                            }
                            else if (th.Attributes["class"].Value.Split(' ')[1] == "family-ES")
                            {
                                if (th.ChildNodes.FirstOrDefault(x => x.Name == "span").Attributes["title"] != null)
                                {
                                    baggageAllowance.CabinFamilyName = "Economy Saver";


                                }
                            }
                            else if (th.Attributes["class"].Value.Split(' ')[1] == "family-EF")
                            {
                                if (th.ChildNodes.FirstOrDefault(x => x.Name == "span").Attributes["title"] != null)
                                {
                                    baggageAllowance.CabinFamilyName = "Economy Flexible";


                                }
                            }
                            else if (th.Attributes["class"].Value.Split(' ')[1] == "family-BS")
                            {
                                if (th.ChildNodes.FirstOrDefault(x => x.Name == "span").Attributes["title"] != null)
                                {
                                    baggageAllowance.CabinFamilyName = "Business Saver";


                                }
                            }
                            else if (th.Attributes["class"].Value.Split(' ')[1] == "family-BF")
                            {
                                if (th.ChildNodes.FirstOrDefault(x => x.Name == "span").Attributes["title"] != null)
                                {
                                    baggageAllowance.CabinFamilyName = "Business Flexible";


                                }
                            }

                            string[] strArr = GetBaggageClassification(th.ChildNodes.FirstOrDefault(x => x.Name == "span").Attributes["title"].Value);
                            baggageAllowance.ChangeOrRefund = strArr[0];
                            baggageAllowance.FromBangladesh = strArr[1];
                            baggageAllowance.OutsideBangladesh = strArr[2];
                            baggageAllowance.Domestic = strArr[3];

                            _FlightSelectionDataModel.BaggageAllowanceCollection.Add(baggageAllowance);


                        }

                    }
                    if (_FlightSelectionDataModel.BaggageAllowanceCollection != null) break;

                }
                catch (Exception)
                {
                    continue;

                }
            }
        }




        private string[] GetBaggageClassification(string baggage)
        {
            string[] strFinal = new string[4];
            string[] strArr = baggage.Split('\r');

            strFinal[0] = new Regex(@"(\S).*").Match(WebUtility.HtmlDecode(strArr[1])).Value;

            strFinal[1] = strArr[3].Split(':')[1];
            strFinal[2] = strArr[4].Split(':')[1];
            strFinal[3] = strArr[5].Split(':')[1];

            return strFinal;
        }

        private void GetFlightData(IEnumerable<HtmlNode> nodes)
        {


            //nodes.count=7;
            foreach (var tag in nodes)
            {

                var tableRows = from tbodies in tag.ChildNodes
                                where tbodies.Name == "tbody"
                                from trs in tbodies.ChildNodes
                                where trs.Name == "tr"
                                select trs;


                foreach (var row in tableRows)
                {
                    flightDetails = new FlightSelectionDataModel.FlightDetails();
                    flightDetails.CabinFamilyCollection = new ObservableCollection<FlightSelectionDataModel.FlightDetails.CabinFamily>();

                    if (tag.Attributes["id"] != null)
                    {
                        string[] strArr = tag.Attributes["id"].Value.Split('_');
                        if (strArr.Count() == 6)
                        {
                            flightDetails.TripID = strArr[0] + "_" + strArr[1];
                            flightDetails.FlightYear = strArr[3];
                            flightDetails.FlightMonth = strArr[4];
                            flightDetails.FlightDay = strArr[5];
                        }
                    }

                    if (row.ChildNodes.Count <= 3) continue;
                    foreach (var childNode in row.ChildNodes)
                    {
                        tdClass = null;
                        GetNodeText(childNode);
                    }

                    if (flag)
                    {
                        FlightIDFlag++;
                        flightDetails.FlightID = FlightIDFlag.ToString();
                    }
                    else
                    {
                        flightDetails.FlightID = FlightIDFlag.ToString();
                    }
                    flag = false;
                    _FlightSelectionDataModel.FlightCollection.Add(flightDetails);
                }
            }
        }

        string tdClass = null;
        Regex rgx = new Regex(@"\S+");
        private void GetNodeText(HtmlNode tempNode)
        {

            if (tempNode.Name == "td" && tempNode.Attributes["class"] != null)
            {
                tdClass = tempNode.Attributes["class"].Value;
            }
            if (!tempNode.HasChildNodes)
            {
                if (rgx.Match(tempNode.InnerText).Value.Length == 0)
                {
                    return;
                }
                else
                {
                    if (tdClass == "flight")
                    {
                        flightDetails.FlightName = tempNode.InnerText;
                    }
                    else if (tdClass == "time leaving")
                    {
                        if (flightDetails.TakeOff == null || flightDetails.TakeOff == "")
                            flightDetails.TakeOff = tempNode.InnerText;
                        else
                        {
                            flightDetails.TakeOffTimeShift = tempNode.InnerText;
                        }
                    }
                    else if (tdClass == "route")
                    {
                        if (flightDetails.Route == null || flightDetails.Route == "")
                        {
                            flightDetails.Route = tempNode.InnerText;
                        }
                        else
                        {
                            flightDetails.Route += " " + tempNode.InnerText;
                        }
                    }
                    else if (tdClass == "time landing")
                    {

                        if (flightDetails.Landing == null || flightDetails.Landing == "")
                        {
                            flightDetails.Landing = tempNode.InnerText;
                        }
                        else
                        {
                            flightDetails.LandingTimeShift = tempNode.InnerText;
                        }
                    }
                    else if (tdClass.Split(' ')[0] == "family")
                    {

                        //Flights can be segmented with stos at airports other than the destination.
                        //This flag is required to mark those flights.
                        flag = true;

                        FlightSelectionDataModel.FlightDetails.CabinFamily cabinFamily = new FlightSelectionDataModel.FlightDetails.CabinFamily();
                        if (tdClass.Split(' ')[1] == "family-SS")
                        {
                            cabinFamily.CabinFamilyName = "Super Saver";
                            cabinFamily.Price = tempNode.InnerText;

                        }
                        else if (tdClass.Split(' ')[1] == "family-ES")
                        {
                            cabinFamily.CabinFamilyName = "Economy Saver";
                            cabinFamily.Price = tempNode.InnerText;
                        }
                        else if (tdClass.Split(' ')[1] == "family-EF")
                        {
                            cabinFamily.CabinFamilyName = "Economy Flexible";
                            cabinFamily.Price = tempNode.InnerText;
                        }
                        else if (tdClass.Split(' ')[1] == "family-BS")
                        {
                            cabinFamily.CabinFamilyName = "Business Saver";
                            cabinFamily.Price = tempNode.InnerText;
                        }
                        else if (tdClass.Split(' ')[1] == "family-BF")
                        {
                            cabinFamily.CabinFamilyName = "Business Flexible";
                            cabinFamily.Price = tempNode.InnerText;
                        }

                        try
                        {
                            HtmlNode inputNode = tempNode.ParentNode.ParentNode.ChildNodes.FirstOrDefault(x => x.Name == "input" && x.Attributes["type"].Value == "radio");
                            if (inputNode.Attributes["value"] != null)
                                cabinFamily.EncodedValue = inputNode.Attributes["value"].Value;
                        }
                        catch (Exception)
                        {

                        }

                        try
                        {
                            HtmlNode labelNode = tempNode.ParentNode.ParentNode;
                            if (labelNode.Name == "label" && labelNode.Attributes["data-title"] != null)
                            {
                                string[] strArr = labelNode.Attributes["data-title"].Value.Split(' ');
                                cabinFamily.TotalPrice = strArr[2];
                            }
                        }
                        catch (Exception)
                        {

                        }

                        if (flightDetails.CabinFamilyCollection.FirstOrDefault(x => x.CabinFamilyName == cabinFamily.CabinFamilyName) == null)
                            flightDetails.CabinFamilyCollection.Add(cabinFamily);
                        else if (flightDetails.CabinFamilyCollection.FirstOrDefault(x => x.CabinFamilyName == cabinFamily.CabinFamilyName) != null && cabinFamily.EncodedValue != null)
                            flightDetails.CabinFamilyCollection[flightDetails.CabinFamilyCollection.IndexOf(flightDetails.CabinFamilyCollection.FirstOrDefault(x => x.CabinFamilyName == cabinFamily.CabinFamilyName))] = cabinFamily;
                    }
                }
            }

            foreach (HtmlNode node in tempNode.ChildNodes)
            {
                //Recursive Call
                GetNodeText(node);
            }

        }

        #endregion GetDataFromBiman



        #region SetData

        public override void SetFlightSelectionDataModel(FlightSelectionDataModel LocalFlightScheduleDataModel)
        {
            if (_FlightSelectionDataModel == null) _FlightSelectionDataModel = new FlightSelectionDataModel();
            _FlightSelectionDataModel = LocalFlightScheduleDataModel;
        }
        #endregion SetData






        #endregion FlightSelection

        #region TravelInitiary

        public override TravelInitiaryDataModel GetTravelInitiaryDataModel()
        {

            if (_TravelInitiaryDataModel == null)
            {
                return _TravelInitiaryDataModel = new TravelInitiaryDataModel();
            }
            return _TravelInitiaryDataModel;
        }

        #region GetDataFromBiman
        public async override Task<TravelInitiaryDataModel> SetTravelInitiaryFromBimanAsync()
        {

            if (_TravelInitiaryDataModel == null)
                _TravelInitiaryDataModel = new TravelInitiaryDataModel();
            _TravelInitiaryDataModel.PriceCollection = new ObservableCollection<TravelInitiaryDataModel.PriceDetails>();



            string url = new ResourceLoader().GetString("BimanFlightSelection");




            HttpClientHandler clientHandler = new HttpClientHandler();
            clientHandler.AllowAutoRedirect = true;
            clientHandler.MaxAutomaticRedirections = 100;
            if (cookies == null) clientHandler.CookieContainer = new CookieContainer();
            else clientHandler.CookieContainer = cookies;



            var WebGet = new HtmlDocument();
            string str = _BookFlightDataModel.RoundTripTicket ? "&trip_2=" + Uri.EscapeDataString(_FlightSelectionDataModel.SelectedCabinFamilyReturn.EncodedValue) : "";

            HttpContent content = new StringContent("ssp=" + Uri.EscapeDataString(_FlightSelectionDataModel.SSP) + "&fsc=" + Uri.EscapeDataString(_FlightSelectionDataModel.FSC) + "&trip_1=" + Uri.EscapeDataString(_FlightSelectionDataModel.SelectedCabinFamilyDeparture.EncodedValue) + str);
            content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue(new ResourceLoader().GetString("HeaderContentType"));
            HttpClient client = new HttpClient(clientHandler);
            client.DefaultRequestHeaders.Add("accept", new ResourceLoader().GetString("HeaderAccept"));
            client.DefaultRequestHeaders.Add("user-agent", new ResourceLoader().GetString("HeaderUserAgent"));
            HttpResponseMessage response = await client.PostAsync(new Uri(url, UriKind.Absolute), content);

            var resContent = await response.Content.ReadAsStringAsync();


            WebGet.LoadHtml(resContent);



            var errors = from div in WebGet.DocumentNode.Descendants()
                         where div.Name == "div" && div.Attributes["class"] != null && (div.Attributes["class"].Value == "errors" || div.Attributes["class"].Value == "warnings")
                         select div;
            if (errors.Count() > 0)
            {
                _TravelInitiaryDataModel.HasErrors = true;
                var error = from e in WebGet.DocumentNode.Descendants()
                            where e.Name == "div" && e.Attributes["class"] != null && e.Attributes["class"].Value == "error"
                            select e;
                foreach (var e in error)
                {
                    _TravelInitiaryDataModel.ErrorDescription = e.InnerText;
                }


                return _TravelInitiaryDataModel;
            }
            else _TravelInitiaryDataModel.HasErrors = false;


            var tableElements = from div in WebGet.DocumentNode.Descendants()
                                where div.Name == "div" && div.Attributes["class"] != null && div.Attributes["class"].Value == "passenger_summary"
                                from table in div.ChildNodes
                                from tableElement in table.ChildNodes
                                where tableElement.Name == "tbody" || tableElement.Name == "tfoot"
                                select tableElement;


            foreach (var tableElement in tableElements)
            {


                if (tableElement.Name == "tbody")
                {
                    IEnumerable<HtmlNode> trs = from tr in tableElement.ChildNodes
                                                where tr.Name == "tr"
                                                select tr;

                    var _PriceDetails = new TravelInitiaryDataModel.PriceDetails();
                    string _PassangerType = null;
                    for (int i = 0; i < trs.Count(); i++)
                    {

                        if (i == 0)
                        {

                            _PassangerType = ((HtmlNode)trs.ElementAtOrDefault(i)).ChildNodes.FirstOrDefault(x => x.Name == "td").InnerText;

                        }
                        else
                        {
                            _PriceDetails.PassengerType = _PassangerType;
                            if (((HtmlNode)trs.ElementAtOrDefault(i)).ChildNodes.FirstOrDefault(x => x.Name == "td" && x.Attributes["class"] != null && x.Attributes["class"].Value == "fare-basis") != null)
                            {
                                _PriceDetails.TripID = new Regex(@"(\S).+(\S)").Match(WebUtility.HtmlDecode(((HtmlNode)trs.ElementAtOrDefault(i)).ChildNodes.FirstOrDefault(x => x.Name == "td" && x.Attributes["class"] != null && x.Attributes["class"].Value == "fare-basis").InnerText).Split(':')[1]).Value;
                            }
                            if (((HtmlNode)trs.ElementAtOrDefault(i)).ChildNodes.FirstOrDefault(x => x.Name == "td" && x.Attributes["class"] != null && x.Attributes["class"].Value == "fare-type").ChildNodes.FirstOrDefault(x => x.Name == "span" && x.Attributes["class"] != null && x.Attributes["class"].Value == "fare-normal") != null)
                            {
                                _PriceDetails.FareType = ((HtmlNode)trs.ElementAtOrDefault(i)).ChildNodes.FirstOrDefault(x => x.Name == "td" && x.Attributes["class"] != null && x.Attributes["class"].Value == "fare-type").ChildNodes.FirstOrDefault(x => x.Name == "span" && x.Attributes["class"] != null && x.Attributes["class"].Value == "fare-normal").InnerText;
                            }
                            if (((HtmlNode)trs.ElementAtOrDefault(i)).ChildNodes.FirstOrDefault(x => x.Name == "td" && x.Attributes["class"] != null && x.Attributes["class"].Value == "segment-total") != null)
                            {
                                _PriceDetails.Fare = ((HtmlNode)trs.ElementAtOrDefault(i)).ChildNodes.FirstOrDefault(x => x.Name == "td" && x.Attributes["class"] != null && x.Attributes["class"].Value == "segment-total").InnerText;
                            }
                            if (((HtmlNode)trs.ElementAtOrDefault(i)).ChildNodes.FirstOrDefault(x => x.Name == "td" && x.Attributes["class"] != null && x.Attributes["class"].Value == "pax-count") != null)
                            {
                                _PriceDetails.Quantity = ((HtmlNode)trs.ElementAtOrDefault(i)).ChildNodes.FirstOrDefault(x => x.Name == "td" && x.Attributes["class"] != null && x.Attributes["class"].Value == "pax-count").ChildNodes.FirstOrDefault(x => x.Name == "strong").InnerText;
                            }
                            if (((HtmlNode)trs.ElementAtOrDefault(i)).ChildNodes.FirstOrDefault(x => x.Name == "td" && x.Attributes["class"] != null && x.Attributes["class"].Value == "pax-type-total") != null)
                            {
                                _PriceDetails.TotalFare = ((HtmlNode)trs.ElementAtOrDefault(i)).ChildNodes.FirstOrDefault(x => x.Name == "td" && x.Attributes["class"] != null && x.Attributes["class"].Value == "pax-type-total").InnerText;
                            }

                            _TravelInitiaryDataModel.PriceCollection.Add(_PriceDetails);
                            _PriceDetails = new TravelInitiaryDataModel.PriceDetails();


                        }


                    }



                }
                else
                {
                    if (_BookFlightDataModel != null && _BookFlightDataModel.PromoCode != null && _BookFlightDataModel.PromoCode != "")
                    {
                        _TravelInitiaryDataModel.PromoMessage = "* Priced using " + _BookFlightDataModel.PromoCode + " promotion";
                    }

                    var currency = from cur in tableElement.ChildNodes.FirstOrDefault(x => x.Name == "tr").ChildNodes.FirstOrDefault(x => x.Name == "td").ChildNodes
                                   select cur;

                    List<string> TempList = new List<string>();
                    foreach (var v in currency)
                        GetPriceSummary(v, TempList);

                    _TravelInitiaryDataModel.GrandTotal = TempList[TempList.Count - 2];

                    if (tableElement.ChildNodes.FirstOrDefault(x => x.Name == "tr").ChildNodes.FirstOrDefault(x => x.Name == "td" && x.Attributes["class"] != null && x.Attributes["class"].Value == "pnr-fare-total").ChildNodes.FirstOrDefault(x => x.Name == "strong" && x.Attributes["class"] != null && x.Attributes["class"].Value == "amount-due") != null)
                    {
                        _TravelInitiaryDataModel.GrandTotal += tableElement.ChildNodes.FirstOrDefault(x => x.Name == "tr").ChildNodes.FirstOrDefault(x => x.Name == "td" && x.Attributes["class"] != null && x.Attributes["class"].Value == "pnr-fare-total").ChildNodes.FirstOrDefault(x => x.Name == "strong" && x.Attributes["class"] != null && x.Attributes["class"].Value == "amount-due").InnerText;
                    }


                    var rules = from rule in WebGet.DocumentNode.Descendants()
                                where rule.Name == "table" && rule.Attributes["class"] != null && rule.Attributes["class"].Value == "Text"
                                select rule;

                    TempList.Clear();
                    foreach (var v in rules)
                    {
                        GetTermsConditions(v, TempList);
                    }

                    foreach (var v in TempList)
                        _TravelInitiaryDataModel.TremsConditions += v + "\n";

                }

            }

            return _TravelInitiaryDataModel;


        }

        private void GetTermsConditions(HtmlNode node, List<String> list)
        {
            if (node.HasChildNodes)
            {
                foreach (var v in node.ChildNodes)
                    GetTermsConditions(v, list);
            }
            else
            {
                if (new Regex(@"(\S).+(\S)").Match(node.InnerText).Value != "")
                {
                    string str = node.InnerText;

                    str = str.Replace('\n', '\0');
                    str = str.Replace("  ", "\0");
                    list.Add(str + "\n");
                }

            }
        }

        private void GetPriceSummary(HtmlNode node, List<String> list)
        {
            if (node.HasChildNodes)
            {
                foreach (var v in node.ChildNodes)
                    GetPriceSummary(v, list);
            }
            else
            {
                if (new Regex(@"(\S).+(\S)").Match(node.InnerText).Value != "")
                    list.Add(new Regex(@"(\S).+(\S)").Match(node.InnerText).Value);


            }
        }


        #endregion GetDataFromBiman
        public override void SetTravelInitiaryDataModel(TravelInitiaryDataModel LocalTravelInitiatyDataModel)
        {
            _TravelInitiaryDataModel = LocalTravelInitiatyDataModel;
        }


        #endregion TravelInitiary


        #region PassangerInfo
        public override PassengerInfoDataModel GetPassengerInfoDataModel()
        {
            if (_PassengerInfoDataModel == null)
            {
                return _PassengerInfoDataModel = new PassengerInfoDataModel();
            }
            return _PassengerInfoDataModel;
        }

        #region GetDataFromBiman
        public async override Task<PassengerInfoDataModel> GetPassengerInfoFromBimanAsync()
        {
            if (_PassengerInfoDataModel == null)
                _PassengerInfoDataModel = new PassengerInfoDataModel();
            _PassengerInfoDataModel.DataPopulated = false;
            _PassengerInfoDataModel.AdultInfoCollection = new List<PassengerInfoDataModel.AdultInfo>();
            _PassengerInfoDataModel.ChildInfoCollection = new List<PassengerInfoDataModel.ChildInfo>();
            _PassengerInfoDataModel.InfantInfoCollection = new List<PassengerInfoDataModel.InfantInfo>();
            _PassengerInfoDataModel.PhoneCountryCollection = new ObservableCollection<string>();
            _PassengerInfoDataModel.CountryNameCollection = new ObservableCollection<string>();
            _PassengerInfoDataModel.CountryCodeCollection = new ObservableCollection<string>();





            string url = new ResourceLoader().GetString("BimanItinerary");
            var request = (HttpWebRequest)WebRequest.Create(url);
            request.Method = new ResourceLoader().GetString("PostMethod");
            request.ContentType = new ResourceLoader().GetString("HeaderContentType");
            if (cookies == null) request.CookieContainer = new CookieContainer();
            else request.CookieContainer = cookies;
            request.Accept = new ResourceLoader().GetString("HeaderAccept");


            using (var requestStream = await request.GetRequestStreamAsync())
            {
                using (var writer = new StreamWriter(requestStream))
                {
                    writer.Write("submit=Continue&agreed=on");

                }
            }


            var WebGet = new HtmlDocument();

            using (var response = await request.GetResponseAsync())
            {
                using (Stream stream = response.GetResponseStream())
                {
                    using (var reader = new StreamReader(stream))
                    {

                        WebGet.Load(reader.BaseStream);

                    }
                }
            }


            var pnr = from v in WebGet.DocumentNode.Descendants()
                      where v.Name == "input" && v.Attributes["name"] != null && v.Attributes["name"].Value == "pnr"
                      select v;

            if (pnr != null && ((HtmlNode)pnr.ElementAtOrDefault(0)).Attributes["value"] != null)
            {
                _PassengerInfoDataModel.Pnr = ((HtmlNode)pnr.ElementAtOrDefault(0)).Attributes["value"].Value;

            }

            var held_seats_ref = from v in WebGet.DocumentNode.Descendants()
                                 where v.Name == "input" && v.Attributes["name"] != null && v.Attributes["name"].Value == "held_seats_ref"
                                 select v;

            if (held_seats_ref != null && ((HtmlNode)held_seats_ref.ElementAtOrDefault(0)).Attributes["value"] != null)
            {
                _PassengerInfoDataModel.Held_Seat_Ref = ((HtmlNode)held_seats_ref.ElementAtOrDefault(0)).Attributes["value"].Value;

            }



            var PhoneCountryCode = from selectNode in WebGet.DocumentNode.Descendants()
                                   where selectNode.Name == "select" && selectNode.Attributes["name"] != null && selectNode.Attributes["name"].Value == "PX[0].PH[0].countryCode"
                                   from optGroup in selectNode.ChildNodes
                                   where optGroup.Name == "optgroup"
                                   select optGroup;

            foreach (var v in PhoneCountryCode)
            {
                foreach (var str in v.InnerText.Split('\n'))
                {
                    if (new Regex(@"(\S).+\S").Match(str).Value != "")
                    {
                        _PassengerInfoDataModel.PhoneCountryCollection.Add(new Regex(@"(\S).+\S").Match(str).Value);

                    }
                }
            }

            var CountryNameCode = from CountryCode in WebGet.DocumentNode.Descendants()
                                  where CountryCode.Name == "select" && CountryCode.Attributes["name"] != null && CountryCode.Attributes["name"].Value == "PX[0].ID[0].countryCode"
                                  from optGroup in CountryCode.ChildNodes
                                  where optGroup.Name == "optgroup"
                                  select optGroup;

            foreach (var v in CountryNameCode)
            {
                foreach (var str in v.InnerText.Split('\r'))
                {
                    if (new Regex(@"(\S).+\S").Match(str).Value != "")
                    {
                        _PassengerInfoDataModel.CountryNameCollection.Add(new Regex(@"(\S).+\S").Match(str).Value);
                    }
                }

                foreach (var country in v.ChildNodes)
                {
                    if (country.Name == "option" && country.Attributes["value"] != null)
                    {
                        _PassengerInfoDataModel.CountryCodeCollection.Add(country.Attributes["value"].Value);
                    }
                }



            }

            _PassengerInfoDataModel.DataPopulated = true;
            return _PassengerInfoDataModel;
        }


        private class ListWithDuplicatesDictionary : List<KeyValuePair<string, string>>
        {

            public void Add(string key, string value)
            {
                var element = new KeyValuePair<string, string>(key, value);
                this.Add(element);
            }
        }

        public async override Task<ReservationInfoDataModel> CreateReservationToBimanAsync()
        {

            while (!_PassengerInfoDataModel.DataPopulated) { ;}

            _PassengerInfoDataModel.Action = "Make Reservation";


            ListWithDuplicatesDictionary ReservationDictionary = new ListWithDuplicatesDictionary() 
            {
                {"pnr",_PassengerInfoDataModel.Pnr},
                {"held_seats_ref",_PassengerInfoDataModel.Held_Seat_Ref}
            };

            int NoOfAdults = _PassengerInfoDataModel.AdultInfoCollection.Count();
            int NoOfChildren = _PassengerInfoDataModel.ChildInfoCollection.Count();
            int NoOfInfants = _PassengerInfoDataModel.InfantInfoCollection.Count();

            int index = 0;
            string px = "";

            //populate adults data
            for (; index < NoOfAdults; index++)
            {
                px = "PX[" + index + "]";


                ReservationDictionary.Add("PX[]", index.ToString());
                ReservationDictionary.Add(px + ".ticketReference", "");
                ReservationDictionary.Add(px + ".paxType", _PassengerInfoDataModel.PaxDictionary["adult"]);
                ReservationDictionary.Add(px + ".paxCode", _PassengerInfoDataModel.PaxDictionary["adult"]);
                ReservationDictionary.Add(px + ".gender", "0");
                ReservationDictionary.Add(px + ".title", _PassengerInfoDataModel.AdultInfoCollection[index].Title);
                ReservationDictionary.Add(px + ".firstName", _PassengerInfoDataModel.AdultInfoCollection[index].FirstName);
                ReservationDictionary.Add(px + ".lastName", _PassengerInfoDataModel.AdultInfoCollection[index].LastName);
                ReservationDictionary.Add(px + ".dateOfBirth.day", _PassengerInfoDataModel.AdultInfoCollection[index].BirthDate.Day);
                ReservationDictionary.Add(px + ".dateOfBirth.month", _PassengerInfoDataModel.AdultInfoCollection[index].BirthDate.Month);
                ReservationDictionary.Add(px + ".dateOfBirth.year", _PassengerInfoDataModel.AdultInfoCollection[index].BirthDate.Year);
                ReservationDictionary.Add(px + ".loyaltyNumber", _PassengerInfoDataModel.AdultInfoCollection[index].ClubMemberNumber);
                ReservationDictionary.Add(px + ".PH[]", "0");
                ReservationDictionary.Add(px + ".PH[0].type", "Mobile");
                if (_PassengerInfoDataModel.AdultInfoCollection[index].PhoneCountryCode != null)
                    ReservationDictionary.Add(px + ".PH[0].countryCode", new Regex(@"\d*\d").Match(_PassengerInfoDataModel.AdultInfoCollection[index].PhoneCountryCode).Value);
                else ReservationDictionary.Add(px + ".PH[0].countryCode", "");
                ReservationDictionary.Add(px + ".PH[0].phoneNumber", _PassengerInfoDataModel.AdultInfoCollection[index].MobilePhone);

                if (index == 0)
                {
                    ReservationDictionary.Add(px + ".PH[]", "1");
                    ReservationDictionary.Add(px + ".PH[1].type", "Unknown");
                    if (_PassengerInfoDataModel.AltPhoneCountryCode != null)
                        ReservationDictionary.Add(px + ".PH[1].countryCode", new Regex(@"\d*\d").Match(_PassengerInfoDataModel.AltPhoneCountryCode).Value);
                    else ReservationDictionary.Add(px + ".PH[1].countryCode", "");
                    ReservationDictionary.Add(px + ".PH[1].phoneNumber", _PassengerInfoDataModel.AlternatePhone);
                    ReservationDictionary.Add(px + ".EM[]", "0");
                    ReservationDictionary.Add(px + ".EM[0].emailAddress", _PassengerInfoDataModel.Email);

                }
                ReservationDictionary.Add(px + ".ID[]", "0");
                ReservationDictionary.Add(px + ".ID[0].documentReference", "");
                ReservationDictionary.Add(px + ".ID[0].documentType", "Passport");
                ReservationDictionary.Add(px + ".ID[0].documentID", _PassengerInfoDataModel.AdultInfoCollection[index].PassportNumber);
                ReservationDictionary.Add(px + ".ID[0].countryCode", _PassengerInfoDataModel.CountryCodeCollection[_PassengerInfoDataModel.CountryNameCollection.IndexOf(_PassengerInfoDataModel.AdultInfoCollection[index].IssuingCountry)]);
                ReservationDictionary.Add(px + ".ID[0].expiration.day", _PassengerInfoDataModel.AdultInfoCollection[index].PassportExpirationDate.Day);
                ReservationDictionary.Add(px + ".ID[0].expiration.month", _PassengerInfoDataModel.AdultInfoCollection[index].PassportExpirationDate.Month);
                ReservationDictionary.Add(px + ".ID[0].expiration.year", _PassengerInfoDataModel.AdultInfoCollection[index].PassportExpirationDate.Year);
                ReservationDictionary.Add(px + ".ID[0].nationCode", _PassengerInfoDataModel.CountryCodeCollection[_PassengerInfoDataModel.CountryNameCollection.IndexOf(_PassengerInfoDataModel.AdultInfoCollection[index].Nationality)]);


            }


            //populate child data
            for (; index < NoOfAdults + NoOfChildren; index++)
            {
                px = "PX[" + index + "]";

                ReservationDictionary.Add("PX[]", index.ToString());
                ReservationDictionary.Add(px + ".ticketReference", "");
                ReservationDictionary.Add(px + ".paxType", _PassengerInfoDataModel.PaxDictionary["child"]);
                ReservationDictionary.Add(px + ".paxCode", _PassengerInfoDataModel.PaxDictionary["child"]);
                ReservationDictionary.Add(px + ".gender", "0");
                ReservationDictionary.Add(px + ".title", _PassengerInfoDataModel.ChildInfoCollection[index - NoOfAdults].Title);
                ReservationDictionary.Add(px + ".firstName", _PassengerInfoDataModel.ChildInfoCollection[index - NoOfAdults].FirstName);
                ReservationDictionary.Add(px + ".lastName", _PassengerInfoDataModel.ChildInfoCollection[index - NoOfAdults].LastName);
                ReservationDictionary.Add(px + ".dateOfBirth.day", _PassengerInfoDataModel.ChildInfoCollection[index - NoOfAdults].BirthDate.Day);
                ReservationDictionary.Add(px + ".dateOfBirth.month", _PassengerInfoDataModel.ChildInfoCollection[index - NoOfAdults].BirthDate.Month);
                ReservationDictionary.Add(px + ".dateOfBirth.year", _PassengerInfoDataModel.ChildInfoCollection[index - NoOfAdults].BirthDate.Year);
                ReservationDictionary.Add(px + ".loyaltyNumber", _PassengerInfoDataModel.ChildInfoCollection[index - NoOfAdults].ClubMemberNumber);


                ReservationDictionary.Add(px + ".ID[]", "0");
                ReservationDictionary.Add(px + ".ID[0].documentReference", "");
                ReservationDictionary.Add(px + ".ID[0].documentType", "Passport");
                ReservationDictionary.Add(px + ".ID[0].documentID", _PassengerInfoDataModel.ChildInfoCollection[index - NoOfAdults].PassportNumber);
                ReservationDictionary.Add(px + ".ID[0].countryCode", _PassengerInfoDataModel.CountryCodeCollection[_PassengerInfoDataModel.CountryNameCollection.IndexOf(_PassengerInfoDataModel.ChildInfoCollection[index - NoOfAdults].IssuingCountry)]);
                ReservationDictionary.Add(px + ".ID[0].expiration.day", _PassengerInfoDataModel.ChildInfoCollection[index - NoOfAdults].PassportExpirationDate.Day);
                ReservationDictionary.Add(px + ".ID[0].expiration.month", _PassengerInfoDataModel.ChildInfoCollection[index - NoOfAdults].PassportExpirationDate.Month);
                ReservationDictionary.Add(px + ".ID[0].expiration.year", _PassengerInfoDataModel.ChildInfoCollection[index - NoOfAdults].PassportExpirationDate.Year);
                ReservationDictionary.Add(px + ".ID[0].nationCode", _PassengerInfoDataModel.CountryCodeCollection[_PassengerInfoDataModel.CountryNameCollection.IndexOf(_PassengerInfoDataModel.ChildInfoCollection[index - NoOfAdults].Nationality)]);


            }

            //populate Infant data
            for (; index < NoOfAdults + NoOfChildren + NoOfInfants; index++)
            {
                px = "PX[" + index + "]";

                ReservationDictionary.Add("PX[]", index.ToString());
                ReservationDictionary.Add(px + ".ticketReference", "");
                ReservationDictionary.Add(px + ".paxType", _PassengerInfoDataModel.PaxDictionary["infant"]);
                ReservationDictionary.Add(px + ".paxCode", _PassengerInfoDataModel.PaxDictionary["infant"]);
                ReservationDictionary.Add(px + ".title", "");
                ReservationDictionary.Add(px + ".gender", _PassengerInfoDataModel.InfantInfoCollection[index - NoOfChildren - NoOfAdults].Gender);
                ReservationDictionary.Add(px + ".firstName", _PassengerInfoDataModel.InfantInfoCollection[index - NoOfChildren - NoOfAdults].FirstName);
                ReservationDictionary.Add(px + ".lastName", _PassengerInfoDataModel.InfantInfoCollection[index - NoOfChildren - NoOfAdults].LastName);
                ReservationDictionary.Add(px + ".dateOfBirth.day", _PassengerInfoDataModel.InfantInfoCollection[index - NoOfChildren - NoOfAdults].BirthDate.Day);
                ReservationDictionary.Add(px + ".dateOfBirth.month", _PassengerInfoDataModel.InfantInfoCollection[index - NoOfChildren - NoOfAdults].BirthDate.Month);
                ReservationDictionary.Add(px + ".dateOfBirth.year", _PassengerInfoDataModel.InfantInfoCollection[index - NoOfChildren - NoOfAdults].BirthDate.Year);


                ReservationDictionary.Add(px + ".ID[]", "0");
                ReservationDictionary.Add(px + ".ID[0].documentReference", "");
                ReservationDictionary.Add(px + ".ID[0].documentType", "Passport");
                ReservationDictionary.Add(px + ".ID[0].documentID", _PassengerInfoDataModel.InfantInfoCollection[index - NoOfChildren - NoOfAdults].PassportNumber);
                ReservationDictionary.Add(px + ".ID[0].countryCode", _PassengerInfoDataModel.CountryCodeCollection[_PassengerInfoDataModel.CountryNameCollection.IndexOf(_PassengerInfoDataModel.InfantInfoCollection[index - NoOfChildren - NoOfAdults].IssuingCountry)]);
                ReservationDictionary.Add(px + ".ID[0].expiration.day", _PassengerInfoDataModel.InfantInfoCollection[index - NoOfChildren - NoOfAdults].PassportExpirationDate.Day);
                ReservationDictionary.Add(px + ".ID[0].expiration.month", _PassengerInfoDataModel.InfantInfoCollection[index - NoOfChildren - NoOfAdults].PassportExpirationDate.Month);
                ReservationDictionary.Add(px + ".ID[0].expiration.year", _PassengerInfoDataModel.InfantInfoCollection[index - NoOfChildren - NoOfAdults].PassportExpirationDate.Year);
                ReservationDictionary.Add(px + ".ID[0].nationCode", _PassengerInfoDataModel.CountryCodeCollection[_PassengerInfoDataModel.CountryNameCollection.IndexOf(_PassengerInfoDataModel.InfantInfoCollection[index - NoOfChildren - NoOfAdults].Nationality)]);


            }

            ReservationDictionary.Add("action", _PassengerInfoDataModel.Action);




            string url = "";

            foreach (var v in ReservationDictionary)
            {

                if (url != "") url += "&";
                if (v.Value == null)
                    url += Uri.EscapeDataString(v.Key) + "=" + "";
                else
                    url += Uri.EscapeDataString(v.Key) + "=" + Uri.EscapeDataString(v.Value);
            }



            string mainUrl = new ResourceLoader().GetString("BimanPassengerInfo");
            var request = (HttpWebRequest)WebRequest.Create(mainUrl);
            request.Method = new ResourceLoader().GetString("PostMethod");
            request.ContentType = new ResourceLoader().GetString("HeaderContentType");
            if (cookies == null) request.CookieContainer = new CookieContainer();
            else request.CookieContainer = cookies;
            request.Accept = new ResourceLoader().GetString("HeaderAccept");


            using (var requestStream = await request.GetRequestStreamAsync())
            {
                using (var writer = new StreamWriter(requestStream))
                {
                    writer.Write(url);

                }
            }


            var WebGet = new HtmlDocument();
            using (var response = await request.GetResponseAsync())
            {
                using (Stream stream = response.GetResponseStream())
                {
                    using (var reader = new StreamReader(stream))
                    {

                        WebGet.Load(reader.BaseStream);

                    }
                }
            }

            return StoryCollection.StoryCollectionInstance.ViewReservation.ExtractReservationInfoFromWebpage(WebGet.DocumentNode.InnerHtml);



        }


        #endregion GetDataFromBiman

        public override void SetPassengerInfoDataModel(PassengerInfoDataModel LocalPassengerInfoDataModel)
        {
            _PassengerInfoDataModel = LocalPassengerInfoDataModel;
        }


        #endregion PassangerInfo



    }
}
