﻿using System;
using BimanPortableClassLibrary.DataModel;
using BimanPortableClassLibrary.Stories;
using System.Net;
using System.IO;
using System.Text;
using HtmlAgilityPack;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using System.Threading;
using System.Xml;
using System.Xml.Linq;
using System.Net.Http;
using System.Globalization;
using Windows.Storage;
using Windows.Storage.Streams;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.UI.Xaml;
using Windows.ApplicationModel.Resources;


namespace BimanAppW8.DataModel.HtmlScrapingImplementation
{
    public class IReservationInfoViewerImplementation : IReservationInfoViewer
    {

        #region reservation_info


        public override ReservationInfoDataModel GetReservationInfoDataModel()
        {
            if (_ReservationInfoDataModel != null)
            {
                return _ReservationInfoDataModel;
            }
            else
            {
                _ReservationInfoDataModel = new ReservationInfoDataModel();
                return _ReservationInfoDataModel;
            }
        }

        public override void SetReservationInfoDataModel(ReservationInfoDataModel LocalReservationInfoDataModel)
        {
            _ReservationInfoDataModel = LocalReservationInfoDataModel;
        }
        public async override Task<ReservationInfoDataModel> RetrieveReservationInfoFromBimanAsync(string PassedBookingID, string PassedName)
        {


            _ReservationInfoDataModel = new ReservationInfoDataModel();
            var cookies = new CookieContainer();
            string url = "passengerName=" + Uri.EscapeDataString(PassedName) + "&PNR=" + Uri.EscapeDataString(PassedBookingID) + "&x=44&y=15";
            string mainUrl = new ResourceLoader().GetString("BimanReservation");
            var request = (HttpWebRequest)WebRequest.Create(mainUrl);
            request.Method = new ResourceLoader().GetString("PostMethod");
            request.ContentType = new ResourceLoader().GetString("HeaderContentType");
            request.CookieContainer = cookies;
            request.Accept = new ResourceLoader().GetString("HeaderAccept");


            using (var requestStream = await request.GetRequestStreamAsync())
            {
                using (var writer = new StreamWriter(requestStream))
                {
                    writer.Write(url);

                }
            }


            var WebGet = new HtmlDocument();
            using (var response = await request.GetResponseAsync())
            {
                using (Stream stream = response.GetResponseStream())
                {
                    using (var reader = new StreamReader(stream))
                    {

                        WebGet.Load(reader.BaseStream);

                    }
                }
            }


            ExtractReservationInfoFromWebpage(WebGet.DocumentNode.InnerHtml);


            return _ReservationInfoDataModel;



        }

        private string[] GetInnerText(HtmlNodeCollection PassedNodes, int count)
        {
            string[] strInnerText = new string[count];
            int i = 0;
            foreach (var str in PassedNodes)
            {

                if (new Regex(@"(\S).*(\S)").Match(str.InnerText).Value != "")
                {
                    string s = WebUtility.HtmlDecode(new Regex(@"(\S).*(\S)").Match(str.InnerText).Value);
                    strInnerText[i] = s;
                    if (i == count - 1) return strInnerText;
                    i++;
                }
            }

            strInnerText = new string[count];
            i = 0;
            foreach (var str in PassedNodes)
            {

                if (new Regex(@"(\S).*").Match(str.InnerText).Value != "")
                {
                    string s = WebUtility.HtmlDecode(new Regex(@"(\S).*").Match(str.InnerText).Value);
                    strInnerText[i] = s;
                    if (i == count - 1) return strInnerText;
                    i++;
                }
            }
            return null;
        }

        public override ReservationInfoDataModel ExtractReservationInfoFromWebpage(string Webpage)
        {
            _ReservationInfoDataModel = new ReservationInfoDataModel();
            HtmlDocument WebGet = new HtmlDocument();
            WebGet.LoadHtml(Webpage);

            //error info
            bool HasErrors = false;
            string ErrorDescription = "";
            var errors = from div in WebGet.DocumentNode.Descendants()
                         where div.Name == "div" && div.Attributes["class"] != null && (div.Attributes["class"].Value == "errors" || div.Attributes["class"].Value == "warnings")
                         select div;
            if (errors.Count() > 0)
            {
                HasErrors = true;
                var error = from e in WebGet.DocumentNode.Descendants()
                            where e.Name == "div" && e.Attributes["class"] != null && e.Attributes["class"].Value == "error"
                            select e;
                foreach (var e in error)
                {
                    ErrorDescription = e.InnerText;
                }

            }
            else HasErrors = false;
            _ReservationInfoDataModel.HasErrors = HasErrors;
            _ReservationInfoDataModel.ErrorMessage = ErrorDescription;
            if (HasErrors) return _ReservationInfoDataModel;

            //Reservation Info
            var status = from spn in WebGet.DocumentNode.Descendants()
                         where spn.Name == "span" && spn.Attributes["class"] != null && spn.Attributes["class"].Value == "pnr_type"
                         select spn;
                       
            _ReservationInfoDataModel.BookingStatus = status.FirstOrDefault().InnerText;

            //Passenger Info
            var persons = from d in WebGet.DocumentNode.Descendants()
                          where d.Name == "div" && d.Attributes["class"] != null && d.Attributes["class"].Value == "passengers"
                          from t in d.ChildNodes
                          where t.Name == "table"
                          from tb in t.ChildNodes
                          where tb.Name == "tbody"
                          from tr in tb.ChildNodes
                          where tr.Name == "tr"
                          select tr;


            foreach (var person in persons)
            {
                int count = 1;
                ReservationInfoDataModel.Passanger TempPerson = new ReservationInfoDataModel.Passanger();
                foreach (var p in person.ChildNodes)
                {

                    if (count % 2 == 0)
                    {
                        string str = new Regex(@"(\S).*").Match(p.InnerText).Value;
                        if (count == 2)
                        {
                            TempPerson.Name = str;
                        }
                        else if (count == 4)
                        {
                            string[] strArr = str.Split('+');
                            for (int i = 0; i < strArr.Count(); i++)
                            {
                                if (strArr[i].Length > 0)
                                {
                                    TempPerson.Contact += "+" + strArr[i];
                                    if (i != strArr.Count() - 1) TempPerson.Contact += "\n";
                                }
                            }
                        }
                    }
                    count++;

                }
                _ReservationInfoDataModel.PassangerCollection.Add(TempPerson);
            }


            //Flight Info
            var itineraries = from i in WebGet.DocumentNode.Descendants()
                              where i.Name == "div" && i.Attributes["class"] != null && i.Attributes["class"].Value == "itinerary"
                              from t in i.ChildNodes
                              where t.Name == "table"
                              select t;

            if (itineraries.Count() == 1)
                _ReservationInfoDataModel.WillReturn = false;
            else _ReservationInfoDataModel.WillReturn = true;

            var captions = from c in itineraries.ElementAt(0).ChildNodes
                           where c.Name == "caption"
                           select c;

            foreach (var c in captions)
            {
                string str = new Regex(@"(\S).*(\S)").Match(c.InnerText).Value;
                _ReservationInfoDataModel.From = WebUtility.HtmlDecode(str).Split(WebUtility.HtmlDecode("&rarr;")[0])[0];
                _ReservationInfoDataModel.To = WebUtility.HtmlDecode(str).Split(WebUtility.HtmlDecode("&rarr;")[0])[1];

            }

            int FlightID = 0;
            DateTime dtFlight = DateTime.UtcNow;
            string day = "1", DepartureFlightNote = "", ReturnFlightNote = "";
            bool IsReturnFlightType = false;
            foreach (var itinerary in itineraries)
            {

                var trs = from tb in itinerary.ChildNodes
                          where tb.Name == "tbody"
                          from tr in tb.ChildNodes
                          where tr.Name == "tr"
                          select tr;
                int trCount = 0;
                foreach (var tr in trs)
                {
                    if (tr.Attributes["class"] != null && trCount % 2 == 0)
                    {
                        ReservationInfoDataModel.Flight TempFlight = new ReservationInfoDataModel.Flight();
                        TempFlight.FlightStatus = tr.Attributes["class"].Value;

                        var tds = from td in tr.ChildNodes
                                  where td.Name == "td"
                                  select td;

                        int count = 0;
                        foreach (var td in tds)
                        {
                            if (td.Attributes["class"] != null)
                            {
                                string strAttr = td.Attributes["class"].Value;
                                string[] strInnerText = null;
                                if (strAttr == "flight-date")
                                {
                                    count = 2;
                                    strInnerText = new string[count];
                                    strInnerText = GetInnerText(td.ChildNodes, count);
                                    if (strInnerText != null)
                                    {
                                        TempFlight.Day = strInnerText[0];
                                        TempFlight.Date = strInnerText[1];
                                    }
                                }
                                else if (strAttr == "flight-time")
                                {
                                    count = 2;
                                    strInnerText = new string[count];
                                    strInnerText = GetInnerText(td.ChildNodes, count);
                                    if (strInnerText != null)
                                    {
                                        TempFlight.DepartTime = strInnerText[0];
                                        TempFlight.LandTime = strInnerText[1];
                                    }
                                }

                                else if (strAttr == "flight-info")
                                {
                                    count = 6;
                                    strInnerText = new string[count];
                                    strInnerText = GetInnerText(td.ChildNodes, count);
                                    if (strInnerText != null)
                                    {
                                        TempFlight.DepartPlace = strInnerText[1] + strInnerText[2];
                                        TempFlight.LandPlace = strInnerText[4] + strInnerText[5];
                                    }
                                }
                                else if (strAttr == "terminal")
                                {
                                    count = 2;
                                    strInnerText = new string[count];
                                    strInnerText = GetInnerText(td.ChildNodes, count);
                                    if (strInnerText != null)
                                    {
                                        TempFlight.Terminal = TempFlight.DepartPlace + ": " + strInnerText[0] + "\n" + TempFlight.LandPlace + ": " + strInnerText[1];
                                    }
                                }
                                else if (strAttr == "flight-number")
                                {
                                    count = 2;
                                    strInnerText = new string[count];
                                    strInnerText = GetInnerText(td.ChildNodes, count);
                                    if (strInnerText != null)
                                    {
                                        TempFlight.FlightNumber = strInnerText[0];
                                        TempFlight.CabinFamily = strInnerText[1];
                                    }
                                }
                                else if (strAttr == "flight-notes")
                                {
                                    count = 1;
                                    strInnerText = new string[count];
                                    strInnerText = GetInnerText(td.ChildNodes, count);
                                    if (strInnerText != null)
                                    {
                                        TempFlight.Notes = strInnerText[0];
                                    }
                                }

                            }



                        }
                        if (!IsReturnFlightType) TempFlight.FlightType = FlightTypeEnum.DepartureFlight;
                        else TempFlight.FlightType = FlightTypeEnum.ReturnFlight;

                        _ReservationInfoDataModel.FlightCollection.Add(TempFlight);


                    }

                    else if (tr.Attributes["class"] != null && trCount % 2 == 1)
                    {
                        var tds = from td in tr.ChildNodes
                                  where td.Name == "td"
                                  select td;

                        foreach (var td in tds)
                        {
                            foreach (var str in td.ChildNodes)
                            {
                                if (new Regex(@"(\S).*(\S)").Match(str.InnerText).Value != "")
                                {
                                    string s = new Regex(@"(\S).*(\S)").Match(str.InnerText).Value;
                                    if (!IsReturnFlightType)
                                        DepartureFlightNote += "\n" + s;
                                    else ReturnFlightNote += "\n" + s;

                                }
                            }
                        }

                    }
                    trCount++;

                }

                if (!IsReturnFlightType)
                    foreach (var v in _ReservationInfoDataModel.FlightCollection.Where(x => x.FlightType == FlightTypeEnum.DepartureFlight))
                        v.Notes += DepartureFlightNote;
                else foreach (var v in _ReservationInfoDataModel.FlightCollection.Where(x => x.FlightType == FlightTypeEnum.ReturnFlight))
                        v.Notes += ReturnFlightNote;

                FlightID++;
                IsReturnFlightType = true;

            }

            //total cost
            var total_costs = from d in WebGet.DocumentNode.Descendants()
                              where d.Name == "div" && d.Attributes["class"] != null && d.Attributes["class"].Value == "ticket_details"
                              from t in d.ChildNodes
                              where t.Name == "table"
                              from tf in t.ChildNodes
                              where tf.Name == "tfoot"
                              from tr in tf.ChildNodes
                              where tr.Name == "tr"
                              from th in tr.ChildNodes
                              where th.Name == "th" && th.Attributes["class"] != null && th.Attributes["class"].Value == "ticket-value"
                              select th;
            if (total_costs.Count() > 0)
                _ReservationInfoDataModel.TotalCost = total_costs.First().InnerText;




            //Due Amount
            var trans = from d in WebGet.DocumentNode.Descendants()
                        where d.Name == "div" && d.Attributes["class"] != null && d.Attributes["class"].Value == "transaction-status"
                        from s in d.ChildNodes
                        where s.Name == "strong"
                        select s;

            _ReservationInfoDataModel.DueAmount = new Regex(@"(\S).*").Match(trans.First().InnerText).Value;


            //website
            var websites = from s in WebGet.DocumentNode.Descendants()
                           where s.Name == "span" && s.Attributes["class"] != null && s.Attributes["class"].Value == "agency-branch"
                           select s;
            foreach (var w in websites)
            {
                _ReservationInfoDataModel.Website = w.InnerText;
            }

            //Phone
            var phones = from p in WebGet.DocumentNode.Descendants()
                         where p.Name == "span" && p.Attributes["class"] != null && p.Attributes["class"].Value == "branch-phone"
                         select p;
            foreach (var p in phones)
            {
                _ReservationInfoDataModel.Phone = p.InnerText;
            }

            _ReservationInfoDataModel.Website = _ReservationInfoDataModel.Website.Split('(')[1].Split(')')[0];


            //GMT calculation
            double milliSec = 0;
            var expires = from e in WebGet.DocumentNode.Descendants()
                          where e.Name == "p" && e.Attributes["class"] != null && e.Attributes["class"].Value == "reservation-expiration"
                          select e;
            foreach (var e in expires)
            {
                _ReservationInfoDataModel.ExpirationDateString = e.InnerText;
                if (e.Attributes["data-expires"] != null)
                    milliSec = Convert.ToDouble(e.Attributes["data-expires"].Value);
            }

            string[] expArr = _ReservationInfoDataModel.ExpirationDateString.Split(' ');
            _ReservationInfoDataModel.GmtString = expArr[7];
            try
            {
                _ReservationInfoDataModel.GmtString = _ReservationInfoDataModel.GmtString.Split('(')[1].Split(')')[0];
            }
            catch (Exception)
            {
                _ReservationInfoDataModel.GmtString = "GMT+6:00";
            }
            int gmtHr = 0;
            int gmtMin = 0;

            if (new Regex(@"\S\+\S").Match(_ReservationInfoDataModel.GmtString).Value != "")
            {

                gmtHr = Convert.ToInt32(_ReservationInfoDataModel.GmtString.Split('+')[1].Split(':')[0]);
                gmtMin = Convert.ToInt32(_ReservationInfoDataModel.GmtString.Split('+')[1].Split(':')[1]);
            }
            else if (new Regex(@"\S\-\S").Match(_ReservationInfoDataModel.GmtString).Value != "")
            {

                gmtHr = Convert.ToInt32(_ReservationInfoDataModel.GmtString.Split('-')[1].Split(':')[0]);
                gmtMin = -Convert.ToInt32(_ReservationInfoDataModel.GmtString.Split('-')[1].Split(':')[1]);
            }




            //Collect Flight Dates&Times
            var dtTbody = from d in WebGet.DocumentNode.Descendants()
                          where d.Name == "div" && d.Attributes["class"] != null && d.Attributes["class"].Value == "ticket_details"
                          from table in d.ChildNodes
                          where table.Name == "table"
                          from tbd in table.ChildNodes
                          where tbd.Name == "tbody"
                          from tr in tbd.ChildNodes
                          where tr.Name == "tr"
                          from td in tr.ChildNodes
                          where td.Name == "td" && td.Attributes["class"] != null && td.Attributes["class"].Value == "segment-date"
                          select td;
            //from td in tr.ChildNodes
            //where tr.Attributes["class"] != null && tr.Attributes["class"].Value == "segment-date"
            //select td;
            int dtCount = 0;

            foreach (var v in dtTbody)
            {
                string dtStr = v.InnerText;
                DateTime dtTempFlight = new DateTime();
                string hh1 = "", mm1 = "", tt1 = "";
                hh1 = _ReservationInfoDataModel.FlightCollection[dtCount].DepartTime.Split(':')[0];
                mm1 = _ReservationInfoDataModel.FlightCollection[dtCount].DepartTime.Split(':')[1].Split(' ')[0];
                tt1 = _ReservationInfoDataModel.FlightCollection[dtCount].DepartTime.Split(' ')[1];

                if (_ReservationInfoDataModel.FlightCollection[dtCount].DepartTime.Length < 8)
                {
                    if (hh1.Length < 2) hh1 = "0" + hh1;
                    if (mm1.Length < 2) mm1 = "0" + mm1;
                    if (tt1.Length < 2) tt1 = "0" + tt1;

                }
                DateTime.TryParseExact(dtStr + " " + hh1 + ":" + mm1 + " " + tt1, "dd MMM yyyy hh:mm tt", null, DateTimeStyles.None, out dtTempFlight);

                _ReservationInfoDataModel.FlightCollection[dtCount].FlightDT = dtTempFlight - new TimeSpan(gmtHr, gmtMin, 0);
                dtTempFlight = _ReservationInfoDataModel.FlightCollection[dtCount].FlightDT;



                DateTime lndTempFlight = new DateTime();
                hh1 = ""; mm1 = ""; tt1 = "";
                hh1 = _ReservationInfoDataModel.FlightCollection[dtCount].LandTime.Split(':')[0];
                mm1 = _ReservationInfoDataModel.FlightCollection[dtCount].LandTime.Split(':')[1].Split(' ')[0];
                tt1 = _ReservationInfoDataModel.FlightCollection[dtCount].LandTime.Split(' ')[1];
                if (_ReservationInfoDataModel.FlightCollection[dtCount].LandTime.Length < 8)
                {
                    if (hh1.Length < 2) hh1 = "0" + hh1;
                    if (mm1.Length < 2) mm1 = "0" + mm1;
                    if (tt1.Length < 2) tt1 = "0" + tt1;

                }
                DateTime.TryParseExact(dtStr + " " + hh1 + ":" + mm1 + " " + tt1, "dd MMM yyyy hh:mm tt", null, DateTimeStyles.None, out lndTempFlight);
                lndTempFlight = lndTempFlight - new TimeSpan(gmtHr, gmtMin, 0);


                if ((lndTempFlight - dtTempFlight).TotalMilliseconds <= 0)
                {
                    lndTempFlight = lndTempFlight + new TimeSpan(1, 0, 0, 0);


                }



                _ReservationInfoDataModel.FlightCollection[dtCount].DepartDateTime = dtTempFlight.ToString("dd-MMM-yyyy   hh:mm tt");
                _ReservationInfoDataModel.FlightCollection[dtCount].DepartDay = dtTempFlight.DayOfWeek.ToString();


                _ReservationInfoDataModel.FlightCollection[dtCount].LandDateTime = lndTempFlight.ToString("dd-MMM-yyyy   hh:mm tt");
                _ReservationInfoDataModel.FlightCollection[dtCount].LandDay = lndTempFlight.DayOfWeek.ToString();





                dtCount++;
                if (dtCount >= _ReservationInfoDataModel.FlightCollection.Count)
                    break;
            }
           

            //expiration date
            string mm = "00", HH = "12", dd = "01", MMM = "JAN", yyyy = "0001", tt = "AM";
            DateTime dt = new DateTime(1970, 1, 1).AddMilliseconds(milliSec);


            _ReservationInfoDataModel.ExpirationDateGMT = dt;
            _ReservationInfoDataModel.ExpirationDateString = _ReservationInfoDataModel.ExpirationDateGMT.ToString("dd-MMM-yyyy   hh:mm tt");




            //Reservation Date
            var rDates = from d in WebGet.DocumentNode.Descendants()
                         where d.Name == "div" && d.Attributes["class"] != null && d.Attributes["class"].Value == "pnr-dates"
                         select d;

            string ResDate = "";

            foreach (var er in rDates)
            {
                ResDate = er.InnerText;
            }

            ResDate = new Regex(@"(\S).*(\S)").Match(ResDate).Value;

            ResDate = ResDate.Substring(ResDate.IndexOf("Reserved On: ") + "Reserved On: ".Length);

            string[] resArr = ResDate.Split(' ');

            try
            {
                dd = resArr[0].Split('-')[0];
                MMM = resArr[0].Split('-')[1];
                yyyy = resArr[0].Split('-')[2];
                tt = mm = resArr[2];
                mm = resArr[1].Split(':')[1];
                HH = resArr[1].Split(':')[0];
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }
            if (dd.Length <= 1) dd = "0" + dd;
            if (mm.Length <= 1) mm = "0" + mm;
            if (HH.Length <= 1) HH = "0" + HH;


            try
            {
                dt = new DateTime();
                DateTime.TryParseExact(dd + " " + MMM + " " + yyyy + " " + HH + ":" + mm + " " + tt, "dd MMM yyyy hh:mm tt", null, DateTimeStyles.None, out dt);
                _ReservationInfoDataModel.ReservationDateGMT = dt - new TimeSpan(gmtHr, gmtMin, 0);
                _ReservationInfoDataModel.ReservationDateString = _ReservationInfoDataModel.ReservationDateGMT.ToString("dd-MMM-yyyy   hh:mm tt");
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
            }


            var spanNodes = from spans in WebGet.DocumentNode.Descendants()
                            where spans.Name == "span" && spans.Attributes["class"] != null && (spans.Attributes["class"].Value == "pnr_number")
                            select spans;

            foreach (var v in spanNodes)
            {

                _ReservationInfoDataModel.BookingID = v.InnerText.Split('#')[1];
            }

            if (_ReservationInfoDataModel.WillReturn)
                _ReservationInfoDataModel.TripType = "Round Trip";
            else _ReservationInfoDataModel.TripType = "One Way";

            if (_ReservationInfoCollector == null) _ReservationInfoCollector = new ReservationInfoCollector(new ObservableCollection<ReservationInfoDataModel>());

            if (_ReservationInfoCollector.ReservationInfoCollection.FirstOrDefault(x => x.BookingID == _ReservationInfoDataModel.BookingID) == null)
            {
                _ReservationInfoCollector.ReservationInfoCollection.Add(_ReservationInfoDataModel);
            }



            return _ReservationInfoDataModel;
        }



        public async void ReservationCollectionJsonConverter(ReservationInfoCollector PassedReservationInfoCollector)
        {
            IJsonConverter json = new IJsonConverter();
            StorageFolder storageFolder = ApplicationData.Current.LocalFolder;
            StorageFile file = await storageFolder.CreateFileAsync("ReservationInfo.json", CreationCollisionOption.ReplaceExisting);

            await FileIO.WriteTextAsync(file, json.ReservationCollectionSerializeToJson(PassedReservationInfoCollector.ReservationInfoCollection));
        }





        #region search_data
        public async override Task<ReservationInfoDataModel> GetBookingSearchResult(string PassedBookingId, string PassedName)
        {
            if (_ReservationInfoCollector.ReservationInfoCollection.FirstOrDefault(x => (x.BookingID.ToUpper() == PassedBookingId.ToUpper() && x.PassangerCollection.FirstOrDefault(y => y.Name.Split(' ')[1].ToUpper() == PassedName.ToUpper() || y.Name.Split(' ')[2].ToUpper() == PassedName.ToUpper()) != null)) != null)
            {
                return _ReservationInfoCollector.ReservationInfoCollection.FirstOrDefault(x => (x.BookingID.ToUpper() == PassedBookingId.ToUpper() && x.PassangerCollection.FirstOrDefault(y => y.Name.Split(' ')[1].ToUpper() == PassedName.ToUpper() || y.Name.Split(' ')[2].ToUpper() == PassedName.ToUpper()) != null));
            }
            else
            {
                return await RetrieveReservationInfoFromBimanAsync(PassedBookingId.ToUpper(), PassedName.ToUpper());
            }
        }

        public override bool IsBookingPresentInDatabase(string PassedBookingId)
        {
            if (_ReservationInfoCollector.ReservationInfoCollection.FirstOrDefault(x => (x.BookingID == PassedBookingId)) != null)// && x.PassangerCollection.FirstOrDefault(y => y.Name.Split(' ')[1] == PassedName || y.Name.Split(' ')[2] == PassedName) != null)) != null)
            {
                return true;
            }
            else return false;
        }


        #endregion search_data



        #endregion reservation_info

        public async override void SetReservationInfoCollectionToJson(ObservableCollection<ReservationInfoDataModel> PassedReservationInfoCollection)
        {

            IJsonConverter json = new IJsonConverter();
            StorageFolder storageFolder = ApplicationData.Current.LocalFolder;
            StorageFile file = await storageFolder.CreateFileAsync("ReservationInfo.json", CreationCollisionOption.ReplaceExisting);

            await FileIO.WriteTextAsync(file, json.ReservationCollectionSerializeToJson(PassedReservationInfoCollection));

        }

        public async override Task<ObservableCollection<ReservationInfoDataModel>> GetReservationInfoCollectionByJson()
        {
            IJsonConverter jsonConverter = new IJsonConverter();
            StorageFile file = await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appdata:///local/ReservationInfo.json"));
            Windows.Storage.Streams.IBuffer bfr = await FileIO.ReadBufferAsync(file);
            using (StreamReader reader = new StreamReader(bfr.AsStream()))
            {
                Debug.WriteLine(await reader.ReadToEndAsync());
            }

            return jsonConverter.ReservationCollectionDeserializeFromJson(bfr.AsStream()).ReservationInfoCollection;
        }



        public override ReservationInfoCollector GetReservationInfoColletor()
        {
            if (_ReservationInfoCollector == null) _ReservationInfoCollector = new ReservationInfoCollector(new ObservableCollection<ReservationInfoDataModel>());
            return _ReservationInfoCollector;
        }

        public override void SetReservationInfoCollector(ReservationInfoCollector PassedreservationInfoCollector)
        {
            _ReservationInfoCollector = PassedreservationInfoCollector;
        }
    }
}
