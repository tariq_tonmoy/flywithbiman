﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BimanPortableClassLibrary.Stories;
using BimanPortableClassLibrary.DataModel;

namespace BimanAppW8.DataModel.DataFeedImplementation
{
    public class IReservationInfoViewerImplementation:IReservationInfoViewer
    {

        public override ReservationInfoDataModel GetReservationInfoDataModel()
        {
            if (_ReservationInfoDataModel == null) _ReservationInfoDataModel = new ReservationInfoDataModel();
            return _ReservationInfoDataModel;
        }

        public override void SetReservationInfoDataModel(ReservationInfoDataModel LocalReservationInfoDataModel)
        {
            _ReservationInfoDataModel = LocalReservationInfoDataModel;
        }


        public override Task<ReservationInfoDataModel> RetrieveReservationInfoFromBimanAsync(string PassedBookingID, string PassedName)
        {
            throw new NotImplementedException();
        }

        public override ReservationInfoDataModel ExtractReservationInfoFromWebpage(string Webpage)
        {
            throw new NotImplementedException();
        }

        public override void SetReservationInfoCollectionToJson(System.Collections.ObjectModel.ObservableCollection<ReservationInfoDataModel> PassedReservationInfoCollection)
        {
            throw new NotImplementedException();
        }

        public override Task<System.Collections.ObjectModel.ObservableCollection<ReservationInfoDataModel>> GetReservationInfoCollectionByJson()
        {
            throw new NotImplementedException();
        }

        public override Task<ReservationInfoDataModel> GetBookingSearchResult(string PassedBookingId, string PassedName)
        {
            throw new NotImplementedException();
        }

        public override ReservationInfoCollector GetReservationInfoColletor()
        {
            throw new NotImplementedException();
        }

        public override void SetReservationInfoCollector(ReservationInfoCollector PassedreservationInfoCollector)
        {
            throw new NotImplementedException();
        }

        public override bool IsBookingPresentInDatabase(string PassedBookingId)
        {
            throw new NotImplementedException();
        }
    }
}
