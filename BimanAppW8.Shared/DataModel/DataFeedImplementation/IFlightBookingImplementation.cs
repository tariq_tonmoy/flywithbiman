﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BimanPortableClassLibrary.Stories;
using BimanPortableClassLibrary.DataModel;

namespace BimanAppW8.DataModel.DataFeedImplementation
{
    public class IFlightBookingImplementation:IFlightBookingHandler
    {
        public override BookFlightDataModel GetBookFlightDataModel()
        {
            if (_BookFlightDataModel == null)
            {
                return _BookFlightDataModel = new BookFlightDataModel() ;
            }
            return _BookFlightDataModel;
        }

        public override void SetBookFlightDataModel(BookFlightDataModel LocalBookFlightDataModel)
        {
            _BookFlightDataModel = LocalBookFlightDataModel;
        }

        public override FlightSelectionDataModel GetFlightSelectionDataModel()
        {
            if (_FlightSelectionDataModel == null)
            {
                return _FlightSelectionDataModel = new FlightSelectionDataModel();
            }
            return _FlightSelectionDataModel;
        
        }

        public override void SetFlightSelectionDataModel(FlightSelectionDataModel LocalFlightSelectionDataModel)
        {

            _FlightSelectionDataModel = LocalFlightSelectionDataModel;    
        }

        public override TravelInitiaryDataModel GetTravelInitiaryDataModel()
        {

            if (_TravelInitiaryDataModel == null)
            {
                return _TravelInitiaryDataModel = new TravelInitiaryDataModel();
            }
            return _TravelInitiaryDataModel;
        }

        public override void SetTravelInitiaryDataModel(TravelInitiaryDataModel LocalTravelInitiatyDataModel)
        {
            _TravelInitiaryDataModel = LocalTravelInitiatyDataModel;
        }

        public override PassengerInfoDataModel GetPassengerInfoDataModel()
        {
            if (_PassengerInfoDataModel == null)
            {
                return _PassengerInfoDataModel = new PassengerInfoDataModel();
            }
            return _PassengerInfoDataModel;
        }

        public override void SetPassengerInfoDataModel(PassengerInfoDataModel LocalPassengerInfoDataModel)
        {
            _PassengerInfoDataModel = LocalPassengerInfoDataModel;    
        }

        public override async Task<BookFlightDataModel> GetBookFlightDataFromBimanAsync()
        {
            throw new NotImplementedException();
        }

        public override async Task<FlightSelectionDataModel> GetFlightSelectionFromBimanAsync()
        {
            throw new NotImplementedException();
        }

        public override async Task<TravelInitiaryDataModel> SetTravelInitiaryFromBimanAsync()
        {
            throw new NotImplementedException();
        }

        public override async Task<PassengerInfoDataModel> GetPassengerInfoFromBimanAsync()
        {
            throw new NotImplementedException();
        }

        public override async Task<ReservationInfoDataModel> CreateReservationToBimanAsync()
        {
            throw new NotImplementedException();
        }

        //public async override Task<ReservationInfoDataModel> RetrieveReservationInfoFromBimanAsync()
        //{
        //    throw new NotImplementedException();
        //}

        //public override ReservationInfoDataModel GetReservationInfoDataModel()
        //{
        //    throw new NotImplementedException();
        //}

        //public override void SetReservationInfoDataModel(ReservationInfoDataModel LocalReservationInfoDataModel)
        //{
        //    throw new NotImplementedException();
        //}
    }
}
