﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BimanPortableClassLibrary.DataModel;
using BimanPortableClassLibrary.Stories;

namespace BimanAppW8.DataModel
{
    public class InitalizeImplementation
    {
        public void InitializeHtmlScrapingImplementation()
        {
            StoryCollection.StoryCollectionInstance.BookFlight = new HtmlScrapingImplementation.IFlightBookingImplementation();
            StoryCollection.StoryCollectionInstance.ViewReservation = new HtmlScrapingImplementation.IReservationInfoViewerImplementation();
        }

        public void InitializeDataFeedImplemetation()
        {
            StoryCollection.StoryCollectionInstance.BookFlight = new DataFeedImplementation.IFlightBookingImplementation();
            StoryCollection.StoryCollectionInstance.ViewReservation = new DataFeedImplementation.IReservationInfoViewerImplementation();
        }
    }
}
