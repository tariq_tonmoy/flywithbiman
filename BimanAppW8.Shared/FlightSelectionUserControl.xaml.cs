﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using BimanPortableClassLibrary.DataModel;
using BimanPortableClassLibrary.Stories;
using BimanAppW8.DataModel.HtmlScrapingImplementation;
using System.Diagnostics;
// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace BimanAppW8
{
    public sealed partial class FlightSelectionUserControl : UserControl
    {
        public delegate void PrevButtonClickDelegate(object sender, RoutedEventArgs e);
        public event PrevButtonClickDelegate PrevButtonClickedEvent;

        public delegate void NextButtonClickDelegate(object sender, RoutedEventArgs e);
        public event NextButtonClickDelegate NextButtonClickedEvent;


        public delegate void ItemClickedDelegate(object sender, ItemClickEventArgs e);
        public event ItemClickedDelegate ItemClickedEvent;


        public delegate void FlightDetailsClickedDelegate(object sender, ItemClickEventArgs e);
        public event FlightDetailsClickedDelegate FlightDetailsClickedEvent;

        public delegate void RadioClickedDelegate(object sender, RoutedEventArgs e);
        public event RadioClickedDelegate RadioClickedEvent;


        public FlightSelectionUserControl()
        {
            this.InitializeComponent();
            CabinClassGrid.Visibility = Visibility.Collapsed;
            PricingHideStoryboard.Begin();


        }
        private List<Control> AllChildren(DependencyObject parent, int flag)
        {
            if (parent is Grid && (parent as Grid).Name.Equals("TickGrid") && flag == 1)
            {
                if (prevTickGrid != null) prevTickGrid.Visibility = Visibility.Collapsed;
                (parent as Grid).Visibility = Visibility.Visible;
                prevTickGrid = (parent as Grid);
                return null;
            }
            else if (parent is Border && (parent as Border).Name.Equals("FlightSelectionBorder") && flag == 2)
            {
                if (prevSelectionBorder != null)
                    prevSelectionBorder.BorderThickness = new Thickness(0d);

                if (prevSelectionBorder != (parent as Border))
                {
                    foreach (var v in RadioButtonList)
                        v.IsChecked = false;
                    if (IsPricingShown)
                    {
                        PricingShowStoryboard.Stop();
                        PricingHideStoryboard.Begin();
                        IsPricingShown = false;
                    }
                    (parent as Border).BorderThickness = new Thickness(2d);
                    prevSelectionBorder = parent as Border;
                }
                return null;

            }

            var _List = new List<Control>();
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(parent); i++)
            {
                var _Child = VisualTreeHelper.GetChild(parent, i);

                var v = AllChildren(_Child, flag);
                if (v == null) return null;
                else _List.AddRange(v);
            }
            return _List;
        }

        Grid prevTickGrid = null;
        Border prevSelectionBorder = null;

        private void DateGridView_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (e != null)
            {
                GridView gridView = (GridView)e.OriginalSource;
                var selectedItem = gridView.ContainerFromItem(e.ClickedItem);
                var itemContainer = (GridViewItem)selectedItem;
                AllChildren(itemContainer, 1);
            }

            if (ItemClickedEvent != null)
            {
                ItemClickedEvent(sender, e);
            }
        }



        public void ResetTickMark()
        {
            if (prevTickGrid != null)
                prevTickGrid.Visibility = Visibility.Collapsed;
        }


        private void PreviousDate_Click(object sender, RoutedEventArgs e)
        {

            if (PrevButtonClickedEvent != null)
                PrevButtonClickedEvent(sender, e);

        }

        private void NextDate_Click(object sender, RoutedEventArgs e)
        {

            if (NextButtonClickedEvent != null)
                NextButtonClickedEvent(sender, e);
        }

        private void FlightDetailsGridView_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (e != null)
            {
                ListView listView = (ListView)e.OriginalSource;
                var selectedItem = listView.ContainerFromItem(e.ClickedItem);
                var itemContainer = (ListViewItem)selectedItem;
                AllChildren(itemContainer, 2);
            }

            if (FlightDetailsClickedEvent != null)
                FlightDetailsClickedEvent(sender, e);
            CabinClassGrid.Visibility = Visibility.Visible;
        }

        public void ResetBorderAndRadio(bool ClearBorderList)
        {
            if (prevSelectionBorder != null)
                prevSelectionBorder.BorderThickness = new Thickness(0d);

            foreach (var v in RadioButtonList)
                v.IsChecked = false;

            if (IsPricingShown)
            {
                PricingShowStoryboard.Stop();
                PricingHideStoryboard.Begin();
                IsPricingShown = false;
            }

        }

        public void HideCabinClassGrid()
        {
            CabinClassGrid.Visibility = Visibility.Collapsed;
        }

       
        List<RadioButton> RadioButtonList = new List<RadioButton>();
        private void RadioButton_Loaded(object sender, RoutedEventArgs e)
        {
            RadioButtonList.Add((RadioButton)sender);
        }

        bool IsPricingShown = false;
        private void RadioButton_Click(object sender, RoutedEventArgs e)
        {
            if (RadioClickedEvent != null)
                RadioClickedEvent(sender, e);
            if (((RadioButton)sender).IsEnabled && !IsPricingShown)
            {
                PricingHideStoryboard.Stop();
                PricingShowStoryboard.Begin();
                IsPricingShown = true;
            }
        }

        public bool IsCabinSelected()
        {
            bool b = false;
            foreach (var v in RadioButtonList)
            {
                if ((bool)v.IsChecked)
                {
                    b = true;
                    break;
                }
            }
            return b;

        }


    }
}
