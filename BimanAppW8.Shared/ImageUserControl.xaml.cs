﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System.Diagnostics;
using Windows.UI.Xaml.Media.Animation;
using System.Windows;


// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace BimanAppW8
{
    public sealed partial class ImageUserControl : UserControl
    {
        public delegate void ActionClick(object sender, ItemClickEventArgs e);
        public event ActionClick ItemClicked;
        public ImageUserControl()
        {
            this.InitializeComponent();



        }

        private void ImageView_ItemClick(object sender, ItemClickEventArgs e)
        {
            if (ItemClicked != null)
            {
                ItemClicked(sender, e);
            }
        }


        List<ScrollViewerController> ScrollViewerList = new List<ScrollViewerController>();

        private static int i = 0;
        private void ScrollViewer_Loaded(object sender, RoutedEventArgs e)
        {

            ScrollViewer scroll = (ScrollViewer)sender;
            scroll.IsEnabled = false;
            int rand = new Random(DateTime.UtcNow.Millisecond + i).Next(3, 21);

            var v = new ScrollViewerController() { ImageScrollViewer = scroll, Rand = rand };
            v.SetupNextScroll();
            ScrollViewerList.Add(v);
            i++;
        }

        public void StopTimers()
        {
            foreach (var v in ScrollViewerList)
            {
                v.StopTimers();
            }
        }

        public void StartTimers()
        {
            foreach (var v in ScrollViewerList)
            {
                v.StartTimers();
            }
        }
    }

    public class ScrollViewerController
    {
        private static int i = 0;
        private ScrollViewer _ImageScrollViewer;
        private int _Rand;

        public int Rand
        {
            get { return _Rand; }
            set { _Rand = value; }
        }
        private double _OffsetValue;

        private long _TickSpeed;
        private int _ImageHeight;
        private int _NextTargetOffset;

        public int NextTargetOffset
        {
            get { return _NextTargetOffset; }
            set { _NextTargetOffset = value; }
        }

        public int ImageHeight
        {
            get { return _ImageHeight; }
            set { _ImageHeight = value; }
        }



        public long TickSpeed
        {
            get { return _TickSpeed; }
            set { _TickSpeed = value; }
        }

        public double OffsetValue
        {
            get { return _OffsetValue; }
            set { _OffsetValue = value; }
        }
        public ScrollViewer ImageScrollViewer
        {
            get { return _ImageScrollViewer; }
            set { _ImageScrollViewer = value; }
        }
        private DispatcherTimer NextScrollTimer, ScrollTimeTimer;
        private bool _DirectionFlag;

        public bool DirectionFlag
        {
            get { return _DirectionFlag; }
            set { _DirectionFlag = value; }
        }

        public void SetupNextScroll()
        {


            if (NextScrollTimer == null)
            {
                NextScrollTimer = new DispatcherTimer();
                NextScrollTimer.Tick += NextScrollTimer_Tick;

            }

            NextScrollTimer.Interval = new TimeSpan(0, 0, Rand);

            //random int between 3 to 20;
            Rand = new Random(DateTime.UtcNow.Millisecond + i).Next(3, 21);
            i++;
            i %= 1000;
            OffsetValue = 1d;
            TickSpeed = 5000;
            ImageHeight = 130;
            if (!DirectionFlag)
            {
                if (NextTargetOffset < ImageHeight * 2)
                    NextTargetOffset += ImageHeight;
                else NextTargetOffset -= ImageHeight;
            }
            else
            {
                if (NextTargetOffset > 0)
                    NextTargetOffset -= ImageHeight;
                else
                    NextTargetOffset += ImageHeight;
            }


            NextScrollTimer.Start();




        }

        void NextScrollTimer_Tick(object sender, object e)
        {
            if (ScrollTimeTimer == null)
            {
                ScrollTimeTimer = new DispatcherTimer();
                ScrollTimeTimer.Tick += ScrollTimeTimer_Tick;

            }


            ScrollTimeTimer.Interval = new TimeSpan(TickSpeed);

            ScrollTimeTimer.Start();


        }

        void ScrollTimeTimer_Tick(object sender, object e)
        {


            if (!DirectionFlag)
            {
                ImageScrollViewer.ChangeView(0, ImageScrollViewer.VerticalOffset + OffsetValue, 1);

                if (ImageScrollViewer.VerticalOffset >= (double)(ImageHeight * 2))
                    DirectionFlag = true;

            }
            else
            {
                if (ImageScrollViewer.VerticalOffset <= 0.0)
                {
                    DirectionFlag = false;

                }
                else
                {
                    ImageScrollViewer.ChangeView(0, ImageScrollViewer.VerticalOffset - OffsetValue, 1);
                }
            }


            if ((int)ImageScrollViewer.VerticalOffset == NextTargetOffset)
            {
                ScrollTimeTimer.Stop();
                SetupNextScroll();

            }


        }

        public void StopTimers()
        {
            if (NextScrollTimer != null && NextScrollTimer.IsEnabled)
                NextScrollTimer.Stop();
            if (ScrollTimeTimer != null && ScrollTimeTimer.IsEnabled)
                ScrollTimeTimer.Stop();
        }


        public void StartTimers()
        {
            if (NextScrollTimer != null && !NextScrollTimer.IsEnabled)
                NextScrollTimer.Start();
            if (ScrollTimeTimer != null && !ScrollTimeTimer.IsEnabled)
                ScrollTimeTimer.Start();
        }
    }
}
