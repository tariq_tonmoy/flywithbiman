﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using BimanAppW8.Common;
using Windows.ApplicationModel.Activation;
using Windows.ApplicationModel.Core;
using Windows.UI.Core;
using System.Threading.Tasks;
using BimanPortableClassLibrary.DataModel;
using BimanPortableClassLibrary.Stories;
using BimanAppW8.DataModel;


// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace BimanAppW8
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class ExtendedSplash : Page
    {
        private SplashScreen splash;
        internal Frame rootFrame;
        private delegate void InitializeHubPageDelegate();
        private bool IsInitialized=false;
        private bool loadState;
        private LaunchActivatedEventArgs e;

        public ExtendedSplash(SplashScreen splashScreen, bool loadState,Frame PassedRootFrame,LaunchActivatedEventArgs passedArgs)
        {
            this.InitializeComponent();
            splash = splashScreen;
            e = passedArgs;
            if (splash != null)
            {
                splash.Dismissed += new TypedEventHandler<SplashScreen, object>(splash_Dismissed);
                this.loadState = loadState;
            }
            rootFrame = PassedRootFrame;
          

            
           

        }

        
      
        async void splash_Dismissed(SplashScreen sender, object args)
        {

           
            
          

            StoryCollection.StoryCollectionInstance = new StoryCollection();
            new InitalizeImplementation().InitializeHtmlScrapingImplementation();

            try
            {
                StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoColletor().ReservationInfoCollection = await StoryCollection.StoryCollectionInstance.ViewReservation.GetReservationInfoCollectionByJson();
            }
            catch (Exception)
            {

            }
            


            CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, async () =>
            {
                await RestoreStateAsync(loadState);
                
                if (rootFrame.Content == null && e.TileId=="App")
                {
                    Window.Current.Content = rootFrame;
                    rootFrame.Navigate(typeof(HubPage));
                }
                else if (rootFrame.Content == null && e.TileId != "App")
                {
                    Window.Current.Content = rootFrame;
                    rootFrame.Navigate(typeof(ReservationInfoPage));
                }
                else
                {
                    Window.Current.Content = rootFrame;
                    Window.Current.Activate();
                }

            });
           
        }

       

        async Task RestoreStateAsync(bool loadState)
        {

            if (loadState)
            {
                try
                {
                    await SuspensionManager.RestoreAsync();

                    IsInitialized = true;
                    return;
                }
                catch (Exception)
                {
                    IsInitialized = false;
                }
            }
            IsInitialized = true;

            
        }

       

       


    }
}
