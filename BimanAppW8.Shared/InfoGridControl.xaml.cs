﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The User Control item template is documented at http://go.microsoft.com/fwlink/?LinkId=234236

namespace BimanAppW8
{
    public sealed partial class InfoGridControl : UserControl
    {
       // public delegate void ButtonClickDelegate(object sender,)
        public InfoGridControl()
        {
            this.InitializeComponent();
            InfoGrid.Margin = new Thickness(12, 0, 12, 180);
            InfoGrid.Visibility = Visibility.Collapsed;
            
        }


        public void ShowInfoGrid(string str)
        {
            InfoGrid.Visibility = Visibility.Visible;
            InfoText.Text = str;
            HideInfoGridStoryboard.Stop();
            InfoGrid.Margin = new Thickness(12, 0, 12, 0);
            ShowInfoGridStoryboard.Begin();
        }

        public void HideInfoGrid()
        {
            ShowInfoGridStoryboard.Stop();
            
            HideInfoGridStoryboard.Begin();
        }

        private void HideInfoGridStoryboard_Completed(object sender, object e)
        {
            InfoGrid.Margin = new Thickness(12, 0, 12, 180);
            InfoGrid.Visibility = Visibility.Collapsed;
        }

     
    }
}
