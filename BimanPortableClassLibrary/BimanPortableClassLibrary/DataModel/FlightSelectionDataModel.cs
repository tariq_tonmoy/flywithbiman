﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;



namespace BimanPortableClassLibrary.DataModel
{
    public class FlightSelectionDataModel:INotifyPropertyChangedCommon
    {

        private string[] MonthData = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
        private bool _OneWayFlag, _RoundTripFlag;
       

        public bool OneWayFlag
        {
            get { return _OneWayFlag; }
            set { _OneWayFlag = value; }
        }

        public bool RoundTripFlag
        {
            get { return _RoundTripFlag; }
            set { _RoundTripFlag = value; }
        }
           
        public class FlightDetails : INotifyPropertyChangedCommon
        {

          

            //flight ID can be created by numerical values which should be unique;
            //flight id is required for identifying the selected flight form UI;   
            //flight ID is required because routed flights takes several flights to complete one journey.
            private string _FlightID, _Departure, _Arrival;
            public string FlightID
            {
                get { return _FlightID; }
                set { SetProperty(ref _FlightID, value); }
            }

            public string Departure
            {
                get { return _Departure; }
                set { SetProperty(ref _Departure, value); }
            }

            public string Arrival
            {
                get { return _Arrival; }
                set { SetProperty(ref _Arrival, value); }
            }
           

            //trip id can indicate the trip serials. Ex.trip_1, trip_2;
            //trip id is required when showing pricing details
            private string _TripID;



            
            public string TripID
            {
                get { return _TripID; }
                set {SetProperty(ref _TripID , value); }
            }

            private string _FlightName, _FlightYear, _FlightMonth, _FlightDay, _TakeOff, _Landing, _Route, _SpecialOfferDetails, _TakeOffTimeShift, _LandingTimeShift;

            public string LandingTimeShift
            {
                get { return _LandingTimeShift; }
                set { SetProperty(ref _LandingTimeShift , value); }
            }

            public string TakeOffTimeShift
            {
                get { return _TakeOffTimeShift; }
                set {SetProperty(ref _TakeOffTimeShift , value); }
            }

            public string FlightYear
            {
                get { return _FlightYear; }
                set {SetProperty(ref _FlightYear , value); }
            }

            public string FlightMonth
            {
                get { return _FlightMonth; }
                set { SetProperty(ref _FlightMonth , value); }
            }

            

            public string FlightDay
            {
                get { return _FlightDay; }
                set { SetProperty(ref _FlightDay, value); }
            }

            public string TakeOff
            {
                get { return _TakeOff; }
                set { SetProperty(ref _TakeOff, value); }
            }

            public string Landing
            {
                get { return _Landing; }
                set { SetProperty(ref _Landing, value); }
            }

            public string Route
            {
                get { return _Route; }
                set { SetProperty(ref _Route, value); }
            }

            public string SpecialOfferDetails
            {
                get { return _SpecialOfferDetails; }
                set { SetProperty(ref _SpecialOfferDetails, value); }
            }

            public string FlightName
            {
                get { return _FlightName; }
                set { SetProperty(ref _FlightName, value); }
            }

            public class CabinFamily:INotifyPropertyChangedCommon
            {
                private string _CabinFamilyName,  _Price, _EncodedValue, _TotalPrice;

                public string TotalPrice
                {
                    get { return _TotalPrice; }
                    set {SetProperty(ref _TotalPrice , value); }
                }

                public string EncodedValue
                {
                    get { return _EncodedValue; }
                    set { _EncodedValue = value; }
                }

                public string Price
                {
                    get { return _Price; }
                    set { SetProperty(ref _Price, value); }
                }

                

                public string CabinFamilyName
                {
                    get { return _CabinFamilyName; }
                    set { SetProperty(ref _CabinFamilyName, value); }
                }

                
 
                
            }

            public ObservableCollection<CabinFamily> CabinFamilyCollection { get; set; }
            

        
        
        }

        public class BaggageAllowance : INotifyPropertyChangedCommon
        {
            private string _CabinFamilyName,_ChangeOrRefund, _FromBangladesh, _OutsideBangladesh, _Domestic;

            public string CabinFamilyName
            {
                get { return _CabinFamilyName; }
                set { SetProperty(ref _CabinFamilyName, value); }
            }

            public string FromBangladesh
            {
                get { return _FromBangladesh; }
                set { SetProperty(ref _FromBangladesh, value); }
            }

            public string OutsideBangladesh
            {
                get { return _OutsideBangladesh; }
                set { SetProperty(ref _OutsideBangladesh, value); }
            }

            public string Domestic
            {
                get { return _Domestic; }
                set { SetProperty(ref _Domestic, value); }
            }

            public string ChangeOrRefund
            {
                get { return _ChangeOrRefund; }
                set { SetProperty(ref _ChangeOrRefund, value); }
            }
        }

        private ObservableCollection<BaggageAllowance> _BaggageAllowanceCollection;

        public ObservableCollection<BaggageAllowance> BaggageAllowanceCollection
        {
            get { return _BaggageAllowanceCollection; }
            set { _BaggageAllowanceCollection = value; }
        }
        public ObservableCollection<FlightDetails> FlightCollection { get; set; }

        private string _FSC, _SSP;

        public string SSP
        {
            get { return _SSP; }
            set { _SSP = value; }
        }

        public string FSC
        {
            get { return _FSC; }
            set { _FSC = value; }
        }

        private string _ErrorHeader;

        public string ErrorHeader
        {
            get { return _ErrorHeader; }
            set {SetProperty(ref _ErrorHeader , value); }
        }
        private ObservableCollection<string> _ErrorCollection;

        public ObservableCollection<string> ErrorCollection
        {
            get { return _ErrorCollection; }
            set { _ErrorCollection = value; }
        }

        public class FlightDetailForShow
        {
            string _FlightID, _FlightName, _TakeOff, _TakeOffDate, _Landing, _LandingDate, _Route;

            public string FlightID
            {
                get { return _FlightID; }
                set { _FlightID = value; }
            }

            public string FlightName
            {
                get { return _FlightName; }
                set { _FlightName = value; }
            }

            public string TakeOff
            {
                get { return _TakeOff; }
                set { _TakeOff = value; }
            }

            public string TakeOffDate
            {
                get { return _TakeOffDate; }
                set { _TakeOffDate = value; }
            }

            public string Landing
            {
                get { return _Landing; }
                set { _Landing = value; }
            }

            public string LandingDate
            {
                get { return _LandingDate; }
                set { _LandingDate = value; }
            }

            public string Route
            {
                get { return _Route; }
                set { _Route = value; }
            }

        }

        public class CalendarDate
        {
            private string _Day, _Month, _Year;
            
            public string Day
            {
                get { return _Day; }
                set { _Day = value; }
            }

            public string Month
            {
                get { return _Month; }
                set { _Month = value; }
            }

            public string Year
            {
                get { return _Year; }
                set { _Year = value; }
            }
        }
        public ObservableCollection<FlightDetailForShow> FlightDetailsForShowCollection { get; set; }

        private ObservableCollection<CalendarDate> _CalendarDateCollection;
       
        public ObservableCollection<CalendarDate> CalendarDateCollection
        {
            get
            {
                if (_CalendarDateCollection == null) _CalendarDateCollection = new ObservableCollection<CalendarDate>();
                return _CalendarDateCollection;
            }

            set
            {
                 _CalendarDateCollection=value;
            }

        }

        private ObservableCollection<CalendarDate> _CalendarDateCollectionForShow;

        public ObservableCollection<CalendarDate> CalendarDateCollectionForShow
        {
            get
            { 
                if(_CalendarDateCollectionForShow==null)_CalendarDateCollectionForShow = new ObservableCollection<CalendarDate>();
            
                return _CalendarDateCollectionForShow;
            }
            set
            { 
                _CalendarDateCollectionForShow = value;
            }
        }

        



        private bool _IsSuperSaverAvailable, _IsEconomySaverAvailable, _IsEconomyFlexibleAvailable, _IsBusinessSaverAvailable, _IsBusinessFlexibleAvailable;

        public bool IsSuperSaverAvailable
        {
            get { return _IsSuperSaverAvailable; }
            set {SetProperty(ref _IsSuperSaverAvailable , value); }
        }

        public bool IsEconomySaverAvailable
        {
            get { return _IsEconomySaverAvailable; }
            set {SetProperty(ref _IsEconomySaverAvailable , value); }
        }

        public bool IsEconomyFlexibleAvailable
        {
            get { return _IsEconomyFlexibleAvailable; }
            set {SetProperty(ref _IsEconomyFlexibleAvailable , value); }
        }

        public bool IsBusinessSaverAvailable
        {
            get { return _IsBusinessSaverAvailable; }
            set {SetProperty(ref _IsBusinessSaverAvailable , value); }
        }

        public bool IsBusinessFlexibleAvailable
        {
            get { return _IsBusinessFlexibleAvailable; }
            set {SetProperty(ref _IsBusinessFlexibleAvailable , value); }
        }


       
        public FlightDetails.CabinFamily SelectedCabinFamily { get; set; }
        public FlightDetails.CabinFamily SelectedCabinFamilyDeparture { get; set; }
        public FlightDetails.CabinFamily SelectedCabinFamilyReturn { get; set; }

        public BaggageAllowance SelectedBaggageAllowance { get; set; }


        public class HeaderData:INotifyPropertyChangedCommon
        {
            private string _From, _To, _TripHeader, _FromImage, _ToImage;

            public string From
            {
                get { return _From; }
                set { SetProperty(ref _From , value); }
            }

            public string To
            {
                get { return _To; }
                set {SetProperty(ref _To , value); }
            }

            public string TripHeader
            {
                get { return _TripHeader; }
                set {SetProperty(ref _TripHeader , value); }
            }

            public string FromImage
            {
                get { return _FromImage; }
                set {SetProperty(ref _FromImage , value); }
            }

            public string ToImage
            {
                get { return _ToImage; }
                set {SetProperty(ref _ToImage , value); }
            }
        }

        public HeaderData Header { get; set; }

    }
}
