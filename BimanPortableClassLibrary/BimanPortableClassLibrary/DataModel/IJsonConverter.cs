﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Collections.ObjectModel;


namespace BimanPortableClassLibrary.DataModel
{
    public class IJsonConverter
    {
        public string ReservationCollectionSerializeToJson(ObservableCollection<ReservationInfoDataModel> ReservationInfoList)
        {
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(ObservableCollection<ReservationInfoDataModel>));
            MemoryStream stream = new MemoryStream();
            ser.WriteObject(stream, ReservationInfoList);
        
            stream.Position = 0;
            StreamReader sr = new StreamReader(stream);
            return sr.ReadToEnd();
        }

        public ReservationInfoCollector ReservationCollectionDeserializeFromJson(Stream jsonStream)
        {
            
            ObservableCollection<ReservationInfoDataModel> ridmList = new ObservableCollection<ReservationInfoDataModel>();
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(ObservableCollection<ReservationInfoDataModel>));
            ridmList = (ObservableCollection<ReservationInfoDataModel>)ser.ReadObject(jsonStream);
            ReservationInfoCollector ric = new ReservationInfoCollector(ridmList);
            return ric;
        }
    }
}
