﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace BimanPortableClassLibrary.DataModel
{
    public class TravelInitiaryDataModel:INotifyPropertyChangedCommon
    {

        private string _TremsConditions;

        private bool _HasErrors;

        public bool HasErrors
        {
            get { return _HasErrors; }
            set { _HasErrors = value; }
        }

        public string TremsConditions
        {
            get { return _TremsConditions; }
            set {SetProperty(ref _TremsConditions , value); }
        }

      

        private bool _AcceptsTerms;
        public bool AcceptsTerms
        {
            get { return _AcceptsTerms; }
            set {SetProperty(ref _AcceptsTerms , value); }
        }

        private string _PromoMessage;

        public string PromoMessage
        {
            get { return _PromoMessage; }
            set {SetProperty(ref _PromoMessage , value); }
        }

        public class PriceDetails:INotifyPropertyChangedCommon
        {
            private string _TripID, _PassengerType, _FareType, _Fare, _Quantity, _TotalFare;

            public string TotalFare
            {
                get { return _TotalFare; }
                set {SetProperty(ref _TotalFare , value); }
            }

            public string Quantity
            {
                get { return _Quantity; }
                set { SetProperty(ref _Quantity , value); }
            }

            public string Fare
            {
                get { return _Fare; }
                set { SetProperty(ref _Fare , value); }
            }

            public string FareType
            {
                get { return _FareType; }
                set { SetProperty(ref _FareType , value); }
            }

            public string PassengerType
            {
                get { return _PassengerType; }
                set { SetProperty(ref _PassengerType , value); }
            }

            public string TripID
            {
                get { return _TripID; }
                set { SetProperty(ref _TripID , value); }
            }

        }

        public ObservableCollection<PriceDetails> PriceCollection { get; set; }

        private string _GrandTotal;
        public string GrandTotal
        {
            get { return _GrandTotal; }
            set { SetProperty(ref _GrandTotal , value); }
        }

        private string _ErrorDescription;

        public string ErrorDescription
        {
            get { return _ErrorDescription; }
            set { SetProperty(ref _ErrorDescription ,value); }
        }
    }
}

