﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Runtime.Serialization;
using System.Net;

namespace BimanPortableClassLibrary.DataModel
{
    
    public class BookFlightDataModel:INotifyPropertyChangedCommon
    {
      

        private string _BookingID;
      

        public string BookingID
        {
            get { return _BookingID; }
            set { SetProperty(ref  _BookingID , value) ; }
        }
 

        private int _bookingLimit;

        public int BookingLimit { get { return _bookingLimit; } set {SetProperty(ref this._bookingLimit , value); } }

        public class City:INotifyPropertyChangedCommon
        {
            private string _CityID, _Name;

            
            public string Name
            {
                get { return _Name; }
                set { SetProperty( ref _Name , value); }
            }

            public string CityID
            {
                get { return _CityID; }
                set { SetProperty(ref _CityID , value); }
            }
          
        }

        public ObservableCollection<string> CabinClassCollection { get; set; }
        public ObservableCollection<string> CabinClassValueCollection { get; set; }
        public ObservableCollection<string> PromoCodeCollection { get; set; }

        public class FlightDate:INotifyPropertyChangedCommon
        {
            private string _Day, _Month, _Year;

            public string Year
            {
                get { return _Year; }
                set {SetProperty(ref _Year , value); }
            }

            public string Month
            {
                get { return _Month; }
                set { SetProperty(ref _Month , value); }
            }

            public string Day
            {
                get { return _Day; }
                set { SetProperty(ref _Day , value); }
            }
            
        
        }



        public FlightDate DepartDate { get; set; }
        public FlightDate ReturnDate { get; set; }

        private DateTime _DepartDateInDateTime, _ReturnDateInDateTime;

        public DateTime ReturnDateInDateTime
        {
            get { return _ReturnDateInDateTime; }
            set { SetProperty(ref _ReturnDateInDateTime , value); }
        }

        public DateTime DepartDateInDateTime
        {
            get { return _DepartDateInDateTime; }
            set { SetProperty(ref _DepartDateInDateTime , value); }
        }
        public ObservableCollection<City> CityList { get; set; }


        private string _cityIDFrom, _cityIDTo;

        public string CityIDTo
        {
            get { return _cityIDTo; }
            set { SetProperty(ref _cityIDTo , value); }
        }

        public string CityIDFrom
        {
            get { return _cityIDFrom; }
            set {SetProperty(ref _cityIDFrom , value); }
        }
        
       
        private bool _fixedDate, _oneWayTicket, _flexibleDate, _roundTripTicket;

        public bool RoundTripTicket
        {
            get { return _roundTripTicket; }
            set { SetProperty(ref _roundTripTicket, value); }
        }
        public bool OneWayTicket
        {
            get { return _oneWayTicket; }
            set { SetProperty(ref _oneWayTicket, value); }
        }
        public bool FlexibleDate
        {
            get { return _flexibleDate; }
            set { SetProperty(ref _flexibleDate, value); }
        }

        public bool FixedDate
        {
            get { return _fixedDate; }
            set { SetProperty(ref _fixedDate, value); }
        }





        private int _noOfAdults, _noOfChildren, _noOfInfants;
        private string _cabinClass, _promoCode;
        public int NoOfAdults { get { return _noOfAdults; } set { SetProperty(ref _noOfAdults, value); } }
        public int NoOfChildren { get { return _noOfChildren; } set { SetProperty(ref _noOfChildren, value); } }
        public int NoOfInfants { get { return _noOfInfants; } set { SetProperty(ref _noOfInfants, value); } }

        public string CabinClass { get { return _cabinClass; } set { SetProperty(ref _cabinClass, value); } }
        public string PromoCode { get { return _promoCode; } set { SetProperty(ref _promoCode, value); } }

      

       
    }
}
