﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace BimanPortableClassLibrary.DataModel
{
    public class PassengerInfoDataModel:INotifyPropertyChangedCommon
    {

        #region HiddenInfo
        private string _Pnr, _Held_Seat_Ref;
        public string Action = "Make Reservation";

        public string Held_Seat_Ref
        {
            get { return _Held_Seat_Ref; }
            set { _Held_Seat_Ref = value; }
        }

        public string Pnr
        {
            get { return _Pnr; }
            set { _Pnr = value; }
        }
        
        #endregion HiddenInfo


        //Only the First Adult should fill in email and alternate phone field;
        #region PassengerInfo
        private bool _DataPopulated;

        public bool DataPopulated
        {
            get { return _DataPopulated; }
            set { _DataPopulated = value; }
        }

        private string _Email;

        public string Email
        {
            get { return _Email; }
            set {SetProperty(ref _Email , value); }
        }

        private string _AlternatePhone, _AltPhoneCountryCode;

        public string AltPhoneCountryCode
        {
            get { return _AltPhoneCountryCode; }
            set 
            {
                string temp = value;
                try
                {
                    value = value.Split('+')[1].Split(')')[0];
                }
                catch (Exception)
                {
                    value = temp;
                }
                SetProperty(ref _AltPhoneCountryCode , value);
            }
        }

        public string AlternatePhone
        {
            get { return _AlternatePhone; }
            set { SetProperty(ref _AlternatePhone , value); }
        }

        #region CommonInfo


        public Dictionary<string, string> PaxDictionary = new Dictionary<string, string>() 
            {
                {"adult","ADT"},
                {"child","CHD"},
                {"infant","INF"}

            };

        public ObservableCollection<string> CountryNameCollection { get; set; }
        public ObservableCollection<string> CountryCodeCollection { get; set; }
        public ObservableCollection<string> PhoneCountryCollection { get; set; }
        #endregion CommonInfo
        public class BasicInfo:INotifyPropertyChangedCommon
        {

            public BasicInfo(ObservableCollection<string> value)
            {
                CountryNameCollection = new ObservableCollection<string>(value);
            }

            private string _Title, _FirstName, _LastName, _PassportNumber, _IssuingCountry, _Nationality;

            public ObservableCollection<string> CountryNameCollection { get; set; }
            public string Title
            {
                get { return _Title; }
                set { _Title = value; }
            }

            public string PassportNumber
            {
                get { return _PassportNumber; }
                set { SetProperty(ref _PassportNumber , value); }
            }

            public string IssuingCountry
            {
                get { return _IssuingCountry; }
                set { SetProperty(ref _IssuingCountry, value); }
            }

            public string Nationality
            {
                get { return _Nationality; }
                set { SetProperty(ref _Nationality, value); }
            }

            public string LastName
            {
                get { return _LastName; }
                set { SetProperty(ref _LastName, value); }
            }

            public string FirstName
            {
                get { return _FirstName; }
                set { SetProperty(ref _FirstName, value); }
            }
            public class CalenderDate:INotifyPropertyChangedCommon
            {
                private string _Day, _Month, _Year;

                public string Day
                {
                    get { return _Day; }
                    set { SetProperty(ref _Day, value); }
                }

                public string Month
                {
                    get { return _Month; }
                    set { SetProperty(ref _Month, value); }
                }



                //Year list has to be limited according to age restrictions associated with the age groups. 
                //Adults: 12+
                //Chindern: 2-12

                //for children: Year of flight + 12 more years
                //for infants: year of flight + 2 more years
                //remember: year of flight "NOT CURRENT YEAR"!!!!!


                public string Year
                {
                    get { return _Year; }
                    set { SetProperty(ref _Year, value); }
                }




                private ObservableCollection<string> _MonthCollection = new ObservableCollection<string>()
                    {
            "Jan",
            "Feb",
            "Mar",
            "Apr",
            "May",
            "Jun",
            "Jul",
            "Aug",
            "Sep",
            "Oct",
            "Nov",
            "Dec",
 

                    };

                public ObservableCollection<string> MonthCollection
                {
                    get { return _MonthCollection; }
                    set { _MonthCollection = value; }
                }


                private ObservableCollection<string> _DayCollection = new ObservableCollection<string>() { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", };

                public ObservableCollection<string> DayCollection
                {
                    get { return _DayCollection; }
                    set { _DayCollection = value; }
                }

                private ObservableCollection<string> _PassportYearCollection = new ObservableCollection<string>();

                public ObservableCollection<string> PassportYearCollection
                {
                    get
                    {
                        for (int i = DateTime.Now.Year; i < DateTime.Now.Year + 100; i++)
                            this._PassportYearCollection.Add(i.ToString());

                        return _PassportYearCollection;
                    }
                    set { _PassportYearCollection = value; }
                }


                private ObservableCollection<string> _BirthYearCollection = new ObservableCollection<string>();

                public ObservableCollection<string> BirthYearCollection
                {
                    get { return _BirthYearCollection; }
                    set { _BirthYearCollection = value; }
                }


                
               

                

                

                
            }

           

            private CalenderDate _BirthDate, _PassportExpirationDate;

            public CalenderDate PassportExpirationDate
            {
                get { return _PassportExpirationDate; }
                set { SetProperty(ref _PassportExpirationDate, value); }
            }

            public CalenderDate BirthDate
            {
                get { return _BirthDate; }
                set { SetProperty(ref _BirthDate, value); }
            }


            
        }

        public class AdultInfo:BasicInfo
        {
            private string _ClubMemberNumber, _MobilePhone, _PhoneCountryCode;

            public AdultInfo(ObservableCollection<string> PhoneCountryCollection, ObservableCollection<string> CountryCollection)
                : base(CountryCollection)
            {
                this.PhoneCountryCollection = new ObservableCollection<string>(PhoneCountryCollection);
            }

            public string PhoneCountryCode
            {
                get { return _PhoneCountryCode; }
                set
                {
                    string temp = value;
                    try
                    {
                        value = value.Split('+')[1].Split(')')[0];
                    }
                    catch (Exception)
                    {
                        value = temp;
                    }
                    SetProperty(ref _PhoneCountryCode , value); 
                }
            }

           

            public string ClubMemberNumber
            {
                get { return _ClubMemberNumber; }
                set { SetProperty(ref _ClubMemberNumber, value); }
            }

            public string MobilePhone
            {
                get { return _MobilePhone; }
                set { SetProperty(ref _MobilePhone, value); }
            }

            public ObservableCollection<string> PhoneCountryCollection { get; set; }

            private ObservableCollection<string> _TitleList = new ObservableCollection<string>()
            {
                "Mr",
                "Mrs",
                "Ms"
            };

            public ObservableCollection<string> TitleList
            {
                get { return _TitleList; }
                set { _TitleList = value; }
            }

            
        }


        public class ChildInfo : BasicInfo
        {
            public ChildInfo(ObservableCollection<string> CountryCollection)
                : base(CountryCollection)
            {

            }

            private ObservableCollection<string> _TitleList = new ObservableCollection<string>()
            {
                "Mstr",
                "Miss"
            };

            public ObservableCollection<string> TitleList
            {
                get { return _TitleList; }
                set { _TitleList = value; }
            }

            private string _ClubMemberNumber;

            public string ClubMemberNumber
            {
                get { return _ClubMemberNumber; }
                set {SetProperty(ref _ClubMemberNumber , value); }
            }

        }

        public class InfantInfo : BasicInfo
        {
              public InfantInfo(ObservableCollection<string> CountryCollection)
                : base(CountryCollection)
            {

            }

            private ObservableCollection<string> _GenderList = new ObservableCollection<string>()
            {
                "Male",
                "Female"
                
            };

            public ObservableCollection<string> GenderList
            {
                get { return _GenderList; }
                set { _GenderList = value; }
            }

            private string _Gender;

            public string Gender
            {
                get { return _Gender; }
                set {SetProperty(ref _Gender , value); }
            }

        }


        public List<AdultInfo> AdultInfoCollection { get; set; }
        public List<ChildInfo> ChildInfoCollection { get; set; }
        public List<InfantInfo> InfantInfoCollection { get; set; }


        public AdultInfo SpecificAdultInfo { get; set; }
        public ChildInfo SpecificChildInfo { get; set; }
        public InfantInfo SpecificInfantInfo { get; set; }

        


       

        #endregion PassengerInfo

    }
}
