﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;
using System.Threading;

namespace BimanPortableClassLibrary.DataModel
{
    public enum FlightTypeEnum { DepartureFlight, ReturnFlight};
    [DataContract]
    public class ReservationInfoDataModel : INotifyPropertyChangedCommon
    {


        public class Passanger : INotifyPropertyChangedCommon
        {
            private string _Name, _Contact;

            [DataMember]
            public string Name
            {
                get { return _Name; }
                set { SetProperty(ref _Name, value); }
            }

            [DataMember]
            public string Contact
            {
                get { return _Contact; }
                set { SetProperty(ref _Contact, value); }
            }
        }

        private ObservableCollection<Passanger> _PassangerCollection = new ObservableCollection<Passanger>();

        [DataMember]
        public ObservableCollection<Passanger> PassangerCollection
        {
            get { return _PassangerCollection; }
            set { _PassangerCollection = value; }
        }

        public class Flight : INotifyPropertyChangedCommon
        {
            private string _Date, _Day, _DepartTime, _LandTime, _DepartPlace, _LandPlace, _FlightNumber, _Notes, _CabinFamily, _Terminal, _FlightStatus;
            private DateTime _FlightDT;
            private string _DepartDateTime, _LandDateTime, _DepartDay, _LandDay;
            public FlightTypeEnum FlightType { get; set; }
            public string FlightStatus
            {
                get { return _FlightStatus; }
                set { SetProperty(ref _FlightStatus, value); }
            }

            public string Terminal
            {
                get { return _Terminal; }
                set { SetProperty(ref _Terminal, value); }
            }
            

            public string LandDateTime
            {
                get { return _LandDateTime; }
                set { SetProperty(ref _LandDateTime, value); }
            }

            public string DepartDay
            {
                get { return _DepartDay; }
                set { SetProperty(ref _DepartDay, value); }
            }

            public string LandDay
            {
                get { return _LandDay; }
                set { SetProperty(ref  _LandDay, value); }
            }

            public string DepartDateTime
            {
                get { return _DepartDateTime; }
                set { SetProperty(ref _DepartDateTime, value); }
            }

            public DateTime FlightDT
            {
                get { return _FlightDT; }
                set { SetProperty(ref _FlightDT, value); }
            }

            public string CabinFamily
            {
                get { return _CabinFamily; }
                set { SetProperty(ref _CabinFamily, value); }
            }

            public string Date
            {
                get { return _Date; }
                set { SetProperty(ref _Date, value); }
            }

            public string Day
            {
                get { return _Day; }
                set { SetProperty(ref _Day, value); }
            }

            public string DepartTime
            {
                get { return _DepartTime; }
                set { SetProperty(ref _DepartTime, value); }
            }

            public string LandTime
            {
                get { return _LandTime; }
                set { SetProperty(ref _LandTime, value); }
            }

            public string DepartPlace
            {
                get { return _DepartPlace; }
                set { SetProperty(ref _DepartPlace, value); }
            }

            public string LandPlace
            {
                get { return _LandPlace; }
                set { SetProperty(ref _LandPlace, value); }
            }

            public string FlightNumber
            {
                get { return _FlightNumber; }
                set { SetProperty(ref _FlightNumber, value); }
            }

            public string Notes
            {
                get { return _Notes; }
                set { SetProperty(ref _Notes, value); }
            }
        }

        private ObservableCollection<Flight> _FlightCollection = new ObservableCollection<Flight>();

        [DataMember]
        public ObservableCollection<Flight> FlightCollection
        {
            get { return _FlightCollection; }
            set { _FlightCollection = value; }
        }

        private string _TotalCost, _ErrorMessage, _DueAmount, _Website, _Phone, _ExpirationString, _GmtString, _ReservationString;
        private string _BookingID, _TripType, _From, _To;
        private DateTime _ExpirationDateGMT, _ReservationDateGMT;
        private string _ClosestFlightString, _BookingStatus;

        public string BookingStatus
        {
            get { return _BookingStatus; }
            set {SetProperty(ref _BookingStatus, value); }
        }
        private TimeSpan _MinTimeToFlight;

        public TimeSpan MinTimeToFlight
        {
            get { return _MinTimeToFlight; }
            set { SetProperty(ref _MinTimeToFlight, value); }
        }
        public string ClosestFlightString
        {
            get { return _ClosestFlightString; }
            set { SetProperty(ref _ClosestFlightString, value); }
        }


        public void UpdateClosestFlightString()
        {
            TimeSpan minTimeSpan = new TimeSpan(366, 0, 0, 0, 0);
            int minIndex = 0;
            for (int i = 0; i < FlightCollection.Count; i++)
            {
                TimeSpan tempTimeSpan = FlightCollection[i].FlightDT - DateTime.UtcNow;
                if (tempTimeSpan < minTimeSpan && tempTimeSpan.TotalMinutes > 0)
                {
                    minTimeSpan = tempTimeSpan;
                    minIndex = i;
                }


            }

            MinTimeToFlight = minTimeSpan;
            if (MinTimeToFlight.TotalMinutes < 0) MinTimeToFlight = new TimeSpan(366, 0, 0, 0);


            string TimeComp;

            if (minTimeSpan.Days > 0)
            {
                TimeComp = minTimeSpan.Days.ToString() + " Day(s)";
            }
            else if (minTimeSpan.Hours > 0)
            {
                TimeComp = minTimeSpan.Hours.ToString() + " Hour(s) " + minTimeSpan.Minutes.ToString() + " Minutes";
            }
            else if (minTimeSpan.Minutes > 0)
            {
                TimeComp = minTimeSpan.Minutes.ToString() + " Minutes";
            }
            else
            {
                ClosestFlightString = "*Flight Left*";
                return;
            }

            string frm, t;


            frm = FlightCollection[minIndex].DepartPlace.Split('(')[0];
            t = FlightCollection[minIndex].LandPlace.Split('(')[0];

            ClosestFlightString = frm + " -> " + t + " in " + TimeComp;




        }

        public void updateReservationStatus()
        {
            if (this.ExpirationDateGMT < DateTime.UtcNow)
                BookingStatus = "RESERVATION EXPIRED";
                
        }

        [DataMember]
        public string To
        {
            get { return _To; }
            set { SetProperty(ref _To, value); }
        }

        [DataMember]
        public string From
        {
            get { return _From; }
            set { SetProperty(ref _From, value); }
        }

        [DataMember]
        public string TripType
        {
            get { return _TripType; }
            set { SetProperty(ref _TripType, value); }
        }

        [DataMember]
        public string BookingID
        {
            get { return _BookingID; }
            set { SetProperty(ref _BookingID, value); }
        }
        private int _GmtTest = 6;

        [DataMember]
        public string ReservationDateString
        {
            get { return _ReservationString; }
            set { SetProperty(ref _ReservationString, value); }
        }

        [DataMember]
        public DateTime ReservationDateGMT
        {
            get { return _ReservationDateGMT; }
            set { _ReservationDateGMT = value; }
        }

    
        [DataMember]
        public string GmtString
        {
            get { return _GmtString; }
            set { _GmtString = value; }
        }

        [DataMember]
        public DateTime ExpirationDateGMT
        {
            get { return _ExpirationDateGMT; }
            set { _ExpirationDateGMT = value; }
        }

        [DataMember]
        public string ExpirationDateString
        {
            get { return _ExpirationString; }
            set { SetProperty(ref _ExpirationString, value); }
        }

        [DataMember]
        public string Phone
        {
            get { return _Phone; }
            set { SetProperty(ref _Phone, value); }
        }

        [DataMember]
        public string Website
        {
            get { return _Website; }
            set { SetProperty(ref _Website, value); }
        }

        [DataMember]
        public string DueAmount
        {
            get { return _DueAmount; }
            set { _DueAmount = value; }
        }
        private bool _HasErrors, _WillReturn;

        [DataMember]
        public bool WillReturn
        {
            get { return _WillReturn; }
            set { _WillReturn = value; }
        }

        [DataMember]
        public bool HasErrors
        {
            get { return _HasErrors; }
            set { _HasErrors = value; }
        }

        [DataMember]
        public string ErrorMessage
        {
            get { return _ErrorMessage; }
            set { SetProperty(ref _ErrorMessage, value); }
        }

        [DataMember]
        public string TotalCost
        {
            get { return _TotalCost; }
            set { SetProperty(ref _TotalCost, value); }
        }


    }

    [DataContract]
    public class ReservationInfoCollector
    {

        public ReservationInfoCollector(ObservableCollection<ReservationInfoDataModel> PassedReservationInfoCollection)
        {
            this.ReservationInfoCollection = new ObservableCollection<ReservationInfoDataModel>(PassedReservationInfoCollection);
        }

        ObservableCollection<ReservationInfoDataModel> _ReservationInfoCollection = new ObservableCollection<ReservationInfoDataModel>();

        [DataMember]
        public ObservableCollection<ReservationInfoDataModel> ReservationInfoCollection
        {
            get { return _ReservationInfoCollection; }
            set { _ReservationInfoCollection = value; }
        }
    }
}
