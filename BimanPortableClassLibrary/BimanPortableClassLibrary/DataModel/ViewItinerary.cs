﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.ComponentModel;

namespace BimanPortableClassLibrary.DataModel
{
    public class ViewItineraryDataModel:INotifyPropertyChangedCommon
    {

        private string _BookingNumber, _OriginalPNRNumber, _ReservedOn, _ExpiresOn;

        public string BookingNumber
        {
            get { return _BookingNumber; }
            set {SetProperty(ref _BookingNumber,  value); }
        }

        public string OriginalPNRNumber
        {
            get { return _OriginalPNRNumber; }
            set {SetProperty(ref _OriginalPNRNumber , value); }
        }

        public string ReservedOn
        {
            get { return _ReservedOn; }
            set {SetProperty(ref _ReservedOn ,value); }
        }

        public string ExpiresOn
        {
            get { return _ExpiresOn; }
            set {SetProperty(ref _ExpiresOn , value); }
        }

        

        public class TravelItinerary : FlightSelectionDataModel.FlightDetails
        {
            private string _WeekDay, _SelectedCabinFamily, _Note;

            

            public string WeekDay
            {
                get { return _WeekDay; }
                set {SetProperty(ref _WeekDay , value); }
            }

            public string SelectedCabinFamily
            {
                get { return _SelectedCabinFamily; }
                set {SetProperty(ref _SelectedCabinFamily , value); }
            }

            public string Note
            {
                get { return _Note; }
                set {SetProperty(ref _Note , value); }
            }
        }


        public class TransactionDescription:INotifyPropertyChangedCommon
        {
            private string _BaseFare, _Subcharges, _Taxes, _Fees, _TotalAmount;

            public string BaseFare
            {
                get { return _BaseFare; }
                set {SetProperty(ref _BaseFare , value); }
            }

            public string Subcharges
            {
                get { return _Subcharges; }
                set {SetProperty(ref _Subcharges , value); }
            }

            public string Taxes
            {
                get { return _Taxes; }
                set {SetProperty(ref _Taxes , value); }
            }

            public string Fees
            {
                get { return _Fees; }
                set {SetProperty(ref _Fees , value); }
            }

            public string TotalAmount
            {
                get { return _TotalAmount; }
                set {SetProperty(ref _TotalAmount , value); }
            }
        }
        public TravelItinerary DepartureTravelItinerary { get;set; }
        public TravelItinerary ReturnTravelItinerary { get; set; }
        public TransactionDescription TransactionDescriptionForShow { get; set; }

        public class PassengerDetails:INotifyPropertyChangedCommon
        {
            private string _Name, _Contact;

            public string Name
            {
                get { return _Name; }
                set {SetProperty(ref _Name , value); }
            }

            public string Contact
            {
                get { return _Contact; }
                set {SetProperty(ref _Contact , value); }
            }
        }

        public PassengerDetails PassengerDetailsInReservation { get; set; }



    
    
    }
}
