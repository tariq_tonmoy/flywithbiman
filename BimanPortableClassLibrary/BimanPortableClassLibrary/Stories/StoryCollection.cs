﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BimanPortableClassLibrary.Stories
{
    public class StoryCollection
    {
        public IFlightBookingHandler BookFlight { get; set; }
        public IReservationInfoViewer ViewReservation { get; set; }


        public static StoryCollection StoryCollectionInstance { get; set; }
    }
}
