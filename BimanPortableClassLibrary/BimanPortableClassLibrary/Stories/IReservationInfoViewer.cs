﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BimanPortableClassLibrary.DataModel;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace BimanPortableClassLibrary.Stories
{
    public abstract class IReservationInfoViewer
    {

        protected static ReservationInfoDataModel _ReservationInfoDataModel;
        public abstract ReservationInfoDataModel GetReservationInfoDataModel();
        public abstract void SetReservationInfoDataModel(ReservationInfoDataModel LocalReservationInfoDataModel);
        public abstract ReservationInfoDataModel ExtractReservationInfoFromWebpage(string Webpage);

        public abstract Task<ReservationInfoDataModel> RetrieveReservationInfoFromBimanAsync(string PassedBookingID,string PassedName);

        
        
        
        
        
        
        
        protected static ReservationInfoCollector _ReservationInfoCollector;
        public abstract void SetReservationInfoCollectionToJson(ObservableCollection<ReservationInfoDataModel> PassedReservationInfoCollection);
        public abstract Task<ObservableCollection<ReservationInfoDataModel>> GetReservationInfoCollectionByJson();
        public abstract Task<ReservationInfoDataModel> GetBookingSearchResult(string PassedBookingId, string PassedName);
        public abstract bool IsBookingPresentInDatabase(string PassedBookingId);
        

        public abstract ReservationInfoCollector GetReservationInfoColletor();
        public abstract void SetReservationInfoCollector(ReservationInfoCollector PassedreservationInfoCollector);
    }
}
