﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BimanPortableClassLibrary.DataModel;
using System.Threading.Tasks;


namespace BimanPortableClassLibrary.Stories
{
    public abstract class IFlightBookingHandler
    {
        protected static BookFlightDataModel _BookFlightDataModel;
        public abstract BookFlightDataModel GetBookFlightDataModel();
        public abstract void SetBookFlightDataModel(BookFlightDataModel LocalBookFlightDataModel);
        public  abstract Task<BookFlightDataModel> GetBookFlightDataFromBimanAsync();




        protected static FlightSelectionDataModel _FlightSelectionDataModel;
        public abstract FlightSelectionDataModel GetFlightSelectionDataModel();
        public abstract void SetFlightSelectionDataModel(FlightSelectionDataModel LocalFlightSelectionDataModel);
        public  abstract Task<FlightSelectionDataModel> GetFlightSelectionFromBimanAsync();



        protected static TravelInitiaryDataModel _TravelInitiaryDataModel;
        public abstract TravelInitiaryDataModel GetTravelInitiaryDataModel();
        public abstract void SetTravelInitiaryDataModel(TravelInitiaryDataModel LocalTravelInitiatyDataModel);
        public  abstract Task<TravelInitiaryDataModel> SetTravelInitiaryFromBimanAsync();



        protected static PassengerInfoDataModel _PassengerInfoDataModel;
        public abstract PassengerInfoDataModel GetPassengerInfoDataModel();
        public abstract void SetPassengerInfoDataModel(PassengerInfoDataModel LocalPassengerInfoDataModel);
        public abstract  Task<PassengerInfoDataModel> GetPassengerInfoFromBimanAsync();
        public abstract Task<ReservationInfoDataModel> CreateReservationToBimanAsync();
        
        //protected static ReservationInfoDataModel _ReservationInfoDataModel;
        //public abstract ReservationInfoDataModel GetReservationInfoDataModel();
        //public abstract void SetReservationInfoDataModel(ReservationInfoDataModel LocalReservationInfoDataModel);
        //
        //public abstract Task<ReservationInfoDataModel> RetrieveReservationInfoFromBimanAsync();

    }
}
