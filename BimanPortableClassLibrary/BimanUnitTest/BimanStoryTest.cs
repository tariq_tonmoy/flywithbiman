﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using HtmlAgilityPack;
using System.Xml;
using System.Xml.XPath;
using System.Diagnostics;
using System.Net;
using System.Collections.Specialized;
using System.IO;
using System.Text;

namespace BimanUnitTest
{
    [TestClass]
    public class BimanStoryTest
    {
        
        [TestMethod]
        public void TestCookie()
        {
            var cookies = new CookieContainer();
            ServicePointManager.Expect100Continue = false;


            var request = (HttpWebRequest)WebRequest.Create("http://www.facebook.com/login.php");

            request.CookieContainer = cookies;

            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";


            using (var requestStream=request.GetRequestStream())
            using (var writer = new StreamWriter(requestStream))
            {
                writer.Write("email=tariq_tonmoy@live.com&pass=Console.Writeline(s1)&default_persistent=1");
            }


            using (var responseStream=request.GetResponse().GetResponseStream())
            using (var reader = new StreamReader(responseStream))
            {
                var result = reader.ReadToEnd();
                Console.WriteLine(result);
                
            
            }
        }


        //[TestMethod]
        public void TestBooking()
        {
            string strFsc = null, strTrip_1 = null, strTrip_2 = null;
            var WebGet = new HtmlWeb();
            var doc = WebGet.Load("http://www.biman-airlines.com/bookings/flight_selection.aspx?TT=RT&DC=DAC&AC=RGN&AM=2013-12&AD=27&RM=2013-12&RD=29&FL=on&PA=2&PC=1&PI=2&CC=C&CD=HELLO-YANGON");
            //WebGet.Load("TestSave.txt");
            //doc.Save("TestSave.txt");
            //var doc = WebGet.Load("http://www.biman-airlines.com");


            var nodes = doc.DocumentNode.SelectNodes("//*");



            foreach (var tag in nodes)
            {
                if (tag.Attributes["name"] != null && tag.Attributes["name"].Value == "fsc")
                {
                    strFsc = tag.Attributes["value"].Value;

                }
                if (tag.Name == "input" && strTrip_1 == null && tag.Attributes["name"] != null && tag.Attributes["name"].Value == "trip_1")
                {
                    strTrip_1 = tag.Attributes["value"].Value;

                }
                if (tag.Name == "input" && tag.Attributes["name"] != null && strTrip_2 == null && tag.Attributes["name"].Value == "trip_2")
                {
                    strTrip_2 = tag.Attributes["value"].Value;

                }
            }

            string urlAuth = "http://www.biman-airlines.com/bookings/flight_selection.aspx?TT=RT&DC=DAC&AC=RGN&AM=2013-12&AD=27&RM=2013-12&RD=29&FL=on&PA=2&PC=1&PI=2&CC=C&CD=HELLO-YANGON";
            WebClient webClient = new WebClient();

            NameValueCollection FormData = new NameValueCollection();

            FormData["fsc"] = strFsc;
            FormData["trip_1"] = strTrip_1;

            FormData["trip_2"] = strTrip_2;

            try
            {
                byte[] responseBytes = webClient.UploadValues(urlAuth, "POST", FormData);
                string result = Encoding.UTF8.GetString(responseBytes);
                Console.WriteLine(result);


            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            
            
        }


    
        public void TestPosting()
        {
            //string urlAuth = "http://www.biman-airlines.com/bookings/flight_selection.aspx?TT=RT&DC=DAC&AC=RGN&AM=2013-12&AD=27&RM=2013-12&RD=29&FL=on&PA=2&PC=1&PI=2&CC=C&CD=HELLO-YANGON";
            //WebClient webClient = new WebClient();

            //NameValueCollection FormData = new NameValueCollection();

            //FormData["fsc"] = strFsc;
            //FormData["trip_1"] = strTrip_1;

            //FormData["trip_2"] = strTrip_2;
            
            //try
            //{
            //    byte[] responseBytes = webClient.UploadValues(urlAuth, "POST", FormData);
            //    string result = Encoding.UTF8.GetString(responseBytes);
            //    Console.WriteLine(result);


            //}
            //catch (Exception e)
            //{
            //    Console.WriteLine(e.Message);
            //}

        }
    }
}
